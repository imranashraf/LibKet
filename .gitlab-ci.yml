before_script:
   #
   # Install ssh-agent if not already installed, it is required by Docker.
   # (change apt-get to yum if you use an RPM-based image)
   #
   - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'

   #
   # Run ssh-agent (inside the build environment)
   #
   - eval $(ssh-agent -s)

   #
   # Add the SSH key stored in SSH_PRIVATE_KEY variable to the agent store
   # We're using tr to fix line endings which makes ed25519 keys work
   # without extra base64 encoding.
   # https://gitlab.com/gitlab-examples/ssh-private-key/issues/1#note_48526556
   #
   - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null

   #
   # Create the SSH directory and give it the right permissions
   #
   - mkdir -p ~/.ssh
   - chmod 700 ~/.ssh
   
   #
   # Add the Github host to the list of known hosts
   #
   - ssh-keyscan -t rsa github.com >> ~/.ssh/known_hosts

stages:
  - build

#
# Create Doxygen documentation
#
pages:
  stage: build
  image: registry.gitlab.com/mmoelle1/docker-gcc:8
  script: 
    - mkdir -p build 
    - cd build 
    - CC=gcc-8 CXX=g++-8 cmake ..
    - make doc
    - mv doc/html ../public
  artifacts:
    paths:
    - public
  only:
  - master

#
# AQASM
#
AQASM-clang-6.0:
  stage: build
  image: registry.gitlab.com/mmoelle1/docker-clang:6.0
  script:
    - mkdir -p build
    - cd build
    - CC=clang-6.0 CXX=clang++-6.0  cmake .. -DLIBKET_BUILD_UNITTESTS=ON -DLIBKET_WITH_AQASM=ON
    - make
    - make test
    
AQASM-clang-7:
  stage: build
  image: registry.gitlab.com/mmoelle1/docker-clang:7
  script:
    - mkdir -p build
    - cd build
    - CC=clang-7 CXX=clang++-7  cmake .. -DLIBKET_BUILD_UNITTESTS=ON -DLIBKET_WITH_AQASM=ON
    - make
    - make test
    
AQASM-clang-8:
  stage: build
  image: registry.gitlab.com/mmoelle1/docker-clang:8
  script:
    - mkdir -p build
    - cd build
    - CC=clang-8 CXX=clang++-8  cmake .. -DLIBKET_BUILD_UNITTESTS=ON -DLIBKET_WITH_AQASM=ON
    - make
    - make test

AQASM-gcc-5:
  stage: build
  image: registry.gitlab.com/mmoelle1/docker-gcc:5
  script:
    - mkdir -p build
    - cd build
    - CC=gcc-5 CXX=g++-5  cmake .. -DLIBKET_BUILD_UNITTESTS=ON -DLIBKET_WITH_AQASM=ON
    - make
    - make test

AQASM-gcc-6:
  stage: build
  image: registry.gitlab.com/mmoelle1/docker-gcc:6
  script:
    - mkdir -p build
    - cd build
    - CC=gcc-6 CXX=g++-6  cmake .. -DLIBKET_BUILD_UNITTESTS=ON -DLIBKET_WITH_AQASM=ON
    - make
    - make test

AQASM-gcc-7:
  stage: build
  image: registry.gitlab.com/mmoelle1/docker-gcc:7
  script:
    - mkdir -p build
    - cd build
    - CC=gcc-7 CXX=g++-7  cmake .. -DLIBKET_BUILD_UNITTESTS=ON -DLIBKET_WITH_AQASM=ON
    - make
    - make test

AQASM-gcc-8:
  stage: build
  image: registry.gitlab.com/mmoelle1/docker-gcc:8
  script:
    - mkdir -p build
    - cd build
    - CC=gcc-8 CXX=g++-8  cmake .. -DLIBKET_BUILD_UNITTESTS=ON -DLIBKET_WITH_AQASM=ON
    - make
    - make test
    
AQASM-gcc-9:
  stage: build
  image: registry.gitlab.com/mmoelle1/docker-gcc:9
  script:
    - mkdir -p build
    - cd build
    - CC=gcc-9 CXX=g++-9  cmake .. -DLIBKET_BUILD_UNITTESTS=ON -DLIBKET_WITH_AQASM=ON
    - make
    - make test

#
# cQASMv1
#
cQASM-clang-6.0:
  stage: build
  image: registry.gitlab.com/mmoelle1/docker-clang:6.0
  script:
    - mkdir -p build
    - cd build
    - CC=clang-6.0 CXX=clang++-6.0  cmake .. -DLIBKET_BUILD_UNITTESTS=ON -DLIBKET_WITH_CQASM=ON
    - make
    - make test

cQASM-gcc-8:
  stage: build
  image: registry.gitlab.com/mmoelle1/docker-gcc:8
  script:
    - mkdir -p build
    - cd build
    - CC=gcc-8 CXX=g++-8  cmake .. -DLIBKET_BUILD_UNITTESTS=ON -DLIBKET_WITH_CQASM=ON
    - make
    - make test

#
# OpenQASMv2
#
OpenQASM-clang-6.0:
  stage: build
  image: registry.gitlab.com/mmoelle1/docker-clang:6.0
  script:
    - mkdir -p build
    - cd build
    - CC=clang-6.0 CXX=clang++-6.0  cmake .. -DLIBKET_BUILD_UNITTESTS=ON -DLIBKET_WITH_OPENQASM=ON
    - make
    - make test

OpenQASM-gcc-8:
  stage: build
  image: registry.gitlab.com/mmoelle1/docker-gcc:8
  script:
    - mkdir -p build
    - cd build
    - CC=gcc-8 CXX=g++-8  cmake .. -DLIBKET_BUILD_UNITTESTS=ON -DLIBKET_WITH_OPENQASM=ON
    - make
    - make test

#
# OpenQL
#
OpenQL-clang-6.0:
  stage: build
  image: registry.gitlab.com/mmoelle1/docker-clang:6.0
  script:
    - apt-get update -y && apt-get install swig -y
    - mkdir -p build
    - cd build
    - CC=clang-6.0 CXX=clang++-6.0  cmake .. -DLIBKET_BUILD_UNITTESTS=ON -DLIBKET_WITH_OPENQL=ON
    - make
    - make test

OpenQL-gcc-8:
  stage: build
  image: registry.gitlab.com/mmoelle1/docker-gcc:8
  script:
    - apt-get update -y && apt-get install swig -y
    - mkdir -p build
    - cd build
    - CC=gcc-8 CXX=g++-8  cmake .. -DLIBKET_BUILD_UNITTESTS=ON -DLIBKET_WITH_OPENQL=ON
    - make
    - make test

#
# QASM
#
QASM-clang-6.0:
  stage: build
  image: registry.gitlab.com/mmoelle1/docker-clang:6.0
  script:
    - mkdir -p build
    - cd build
    - CC=clang-6.0 CXX=clang++-6.0  cmake .. -DLIBKET_BUILD_UNITTESTS=ON -DLIBKET_WITH_QASM=ON
    - make
    - make test

QASM-gcc-8:
  stage: build
  image: registry.gitlab.com/mmoelle1/docker-gcc:8
  script:
    - mkdir -p build
    - cd build
    - CC=gcc-8 CXX=g++-8  cmake .. -DLIBKET_BUILD_UNITTESTS=ON -DLIBKET_WITH_QASM=ON
    - make
    - make test

#
# Quil
#
Quil-clang-6.0:
  stage: build
  image: registry.gitlab.com/mmoelle1/docker-clang:6.0
  script:
    - mkdir -p build
    - cd build
    - CC=clang-6.0 CXX=clang++-6.0  cmake .. -DLIBKET_BUILD_UNITTESTS=ON -DLIBKET_WITH_QUIL=ON
    - make
    - make test

Quil-gcc-8:
  stage: build
  image: registry.gitlab.com/mmoelle1/docker-gcc:8
  script:
    - mkdir -p build
    - cd build
    - CC=gcc-8 CXX=g++-8  cmake .. -DLIBKET_BUILD_UNITTESTS=ON -DLIBKET_WITH_QUIL=ON
    - make
    - make test

#
# QX
#
QX-clang-6.0:
  stage: build
  image: registry.gitlab.com/mmoelle1/docker-clang:6.0
  script:
    - mkdir -p build
    - cd build
    - CC=clang-6.0 CXX=clang++-6.0  cmake .. -DLIBKET_BUILD_UNITTESTS=ON -DLIBKET_WITH_QX=ON
    - make
    - make test

QX-gcc-8:
  stage: build
  image: registry.gitlab.com/mmoelle1/docker-gcc:8
  script:
    - mkdir -p build
    - cd build
    - CC=gcc-8 CXX=g++-8  cmake .. -DLIBKET_BUILD_UNITTESTS=ON -DLIBKET_WITH_QX=ON
    - make
    - make test
