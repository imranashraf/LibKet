//  Created by Michael Gazonda on 2014-12-28.
//  Copyright (c) 2014 Zed Mind Incorporated - http://zedmind.com. All rights reserved.
//  LICENSED UNDER CPOL 1.02 - http://www.codeproject.com/info/cpol10.aspx

// Compile Time Loops with C++11 - Creating a Generalized static_for Implementation
// http://www.codeproject.com/Articles/857354/Compile-Time-Loops-with-Cplusplus-Creating-a-Gener

#pragma once

// This is an implementation that uses enums instead of constexpr for use with Visual Studio 2013

// You could also make this compatible with C++98 by removing the perfect forwarding, though
// that is unsupported.
namespace zed
{
  template<size_t count, typename functor, size_t sequence_width = 70, typename... functor_types>
  inline auto static_for(functor_types&&... functor_args)
  {
    return static_for_impl<0, count - 1, functor, sequence_width, functor_types...>::loop(std::forward<functor_types>(functor_args)...);
  }

  template<size_t start, size_t end, typename functor, size_t sequence_width = 70, typename... functor_types>
  inline auto static_for(functor_types&&... functor_args)
  {
    return static_for_impl<start, end, functor, sequence_width, functor_types...>::loop(std::forward<functor_types>(functor_args)...);
  }

  template<size_t for_start, size_t for_end, typename functor, size_t sequence_width, typename... functor_types> struct static_for_impl
  {
    static inline auto loop(functor_types&&... functor_args)
    {
      using sequence = point<for_start, for_end>;
      return next<sequence>(std::integral_constant<bool, sequence::is_end_point_>(), std::forward<functor_types>(functor_args)...);
    }

  private:
    template<size_t pt_start, size_t pt_end> struct point
    {
      template<size_t index> struct child_start
      {
        enum { v = index == 0 ? pt_start : child_end<index == 0 ? sequence_count_ : index - 1>::v + 1 };
      };
      template<size_t index> struct child_end
      {
        enum { v = (index >= sequence_count_ - 1) ? pt_end : pt_start + points_in_sequence<sequence_count_>::v * (index + 1) - (index < count_ ? 1 : 0) };
      };
      template<size_t max> struct points_in_sequence
      {
        enum { v = count_ / max + ((count_ % max) > 0 ? 1 : 0) };
      };
      enum { start_      = pt_start };
      enum { end_        = pt_end };
      enum { count_      = end_ - start_ + 1 };
      enum { is_end_point_  = count_ <= sequence_width };
      enum { sequence_count_  = points_in_sequence<sequence_width>::v > sequence_width ? sequence_width : points_in_sequence<sequence_width>::v };

      template<size_t index> using child_point = point<child_start<index>::v, child_end<index>::v>;
    };

    template<size_t flat_start, size_t flat_end, class flat_functor> struct flat_for
    {
      static inline auto flat_loop(functor_types&&... functor_args)
      {
        return flat_next(std::integral_constant<size_t, flat_start>(), std::forward<functor_types>(functor_args)...);
      }

    private:
      static inline auto flat_next(std::integral_constant<size_t, flat_end + 1>, functor_types&&...)
      {
      }
      template<size_t index> static inline auto flat_next(std::integral_constant<size_t, index>, functor_types&&... functor_args)
      {
        auto a = flat_functor::template func<index>(std::forward<functor_types>(functor_args)...);
        return flat_next(std::integral_constant<size_t, index + 1>(), std::forward<functor_types>(functor_args)...);
      }

    };

    template<typename sequence> struct flat_sequence
    {
      template<size_t index> static inline auto func(functor_types&&... functor_args)
      {
        using pt = typename sequence::template child_point<index>;
        return next<pt>(std::integral_constant<bool, pt::is_end_point_>(), std::forward<functor_types>(functor_args)...);
      }
    };

    template<typename sequence> static inline auto next(std::true_type, functor_types&&... functor_args)
    {
      return flat_for<sequence::start_, sequence::end_, functor>::flat_loop(std::forward<functor_types>(functor_args)...);
    }
    template<typename sequence> static inline auto next(std::false_type, functor_types&&... functor_args)
    {
      return flat_for<0, sequence::sequence_count_ - 1, flat_sequence<sequence>>::flat_loop(std::forward<functor_types>(functor_args)...);
    }

  };
}
