//  Created by Michael Gazonda on 2014-12-24.
//  Copyright (c) 2014 Zed Mind Incorporated - http://zedmind.com. All rights reserved.
//  LICENSED UNDER CPOL 1.02 - http://www.codeproject.com/info/cpol10.aspx

// Compile Time Loops with C++11 - Creating a Generalized static_for Implementation
// http://www.codeproject.com/Articles/857354/Compile-Time-Loops-with-Cplusplus-Creating-a-Gener

#pragma once

// I prefer to separate the definition of functions when possible, even with templates.
// To do this requires including those definitions at the same time as the declarations.

// The definitions can get very large and unwieldy when this happens, and so I have created
// some pre-processor definitions that simplify things, and take the attention away from that
// which is the same in every definition.

#define t___      template<long for_start, long for_end, long for_step, class functor, long sequence_width, typename functor_return_type, typename... functor_types>
#define c___      static_for_impl<for_start, for_end, for_step, functor, sequence_width, functor_return_type, functor_types...>

#define tp___    template<long pt_start, long pt_end, long pt_step>
#define cp___    point<pt_start, pt_end, pt_step>

#define tflat___    template<long flat_start, long flat_end, long flat_step, class flat_functor>
#define cflat___    flat_for<flat_start, flat_end, flat_step, flat_functor>

namespace zed
{
  template<long start, long end, long step, typename functor, long sequence_width, typename functor_return_type, typename... functor_types>
  inline auto static_for(functor_return_type&& functor_return_arg, functor_types&&... functor_args)
  {
    static_assert(step != 0, "static_for requires a step != 0");
    return static_for_impl<start, end, step, functor, sequence_width, functor_return_type, functor_types...>::loop(std::forward<functor_return_type>(functor_return_arg), std::forward<functor_types>(functor_args)...);
  }
  template<long count, typename functor, long sequence_width, typename functor_return_type, typename... functor_types>
  inline auto static_for(functor_return_type&& functor_return_arg, functor_types&&... functor_args)
  {
    static_assert(count > 0, "static_for requires a count > 0");
    return static_for_impl<0, count - 1, 1, functor, sequence_width, functor_return_type, functor_types...>::loop(std::forward<functor_return_type>(functor_return_arg), std::forward<functor_types>(functor_args)...);
  }



  t___ auto c___::loop(functor_return_type&& functor_return_arg, functor_types&&... functor_args)
  {
    using sequence = point<for_start, for_end, for_step>;

    return next<sequence>(std::integral_constant<bool, sequence::is_end_point_>(), std::forward<functor_return_type>(functor_return_arg), std::forward<functor_types>(functor_args)...);
  }



  t___ tp___ constexpr long c___::cp___::child_start(long index)
  {
    return
        index == 0
      ?
        start_
      :
        child_end(index - 1) + 1;
  }
  t___ tp___ constexpr long c___::cp___::child_end(long index)
  {
    return
        index == sequence_count() - 1
      ?
        end_
      :
        start_ + points_in_sequence(sequence_count()) * (index + 1) -
          (index < count_
        ?
           1
        :
           0);
  }
  t___ tp___ constexpr long c___::cp___::child_step(long index)
  {
    return
      step_;
  }
  t___ tp___ constexpr long c___::cp___::points_in_sequence(long max)
  {
    return count_ / max +
        (count_ % max > 0
      ?
         1
      :
         0);
  }
  t___ tp___ constexpr long c___::cp___::sequence_count()
  {
    return
        points_in_sequence(sequence_width) > sequence_width
      ?
        sequence_width
      :
        points_in_sequence(sequence_width);
  }



  t___ tflat___ auto c___::cflat___::flat_loop(functor_return_type&& functor_return_arg, functor_types&&... functor_args)
  {
    return flat_next(std::integral_constant<long, flat_start>(), std::forward<functor_return_type>(functor_return_arg), std::forward<functor_types>(functor_args)...);
  }
  t___ tflat___ auto c___::cflat___::flat_next(std::integral_constant<long, flat_start + ( (flat_end - flat_start + flat_step) / flat_step) * flat_step>, functor_return_type&& functor_return_arg, functor_types&&... functor_args)
  {
    return functor_return_arg;
  }
  t___ tflat___ template<long index> auto c___::cflat___::flat_next(std::integral_constant<long, index>, functor_return_type&& functor_return_arg, functor_types&&... functor_args)
  {
    auto arg = flat_functor::template func<index>(std::forward<functor_return_type>(functor_return_arg), std::forward<functor_types>(functor_args)...);

    using next___ = typename static_for_impl<for_start, for_end, for_step, functor, sequence_width, decltype(arg), functor_types...>::template flat_for<flat_start, flat_end, flat_step, flat_functor>;

    return next___::flat_next(std::integral_constant<long, index + flat_step>(),
                              std::forward<decltype(arg)>(arg),
                              std::forward<functor_types>(functor_args)...);
  }



  t___ template<typename sequence> template<long index> auto c___::flat_sequence<sequence>::func(functor_return_type&& functor_return_arg, functor_types&&... functor_args)
  {
    using pt = typename sequence::template child_point<index>;
    return next<pt>(std::integral_constant<bool, pt::is_end_point_>(), std::forward<functor_return_type>(functor_return_arg), std::forward<functor_types>(functor_args)...);
  }
  t___ template<typename sequence> auto c___::next(std::true_type, functor_return_type&& functor_return_arg, functor_types&&... functor_args)
  {
    return flat_for<sequence::start_, sequence::end_, sequence::step_, functor>::flat_loop(std::forward<functor_return_type>(functor_return_arg), std::forward<functor_types>(functor_args)...);
  }
  t___ template<typename sequence> auto c___::next(std::false_type, functor_return_type&& functor_return_arg, functor_types&&... functor_args)
  {
    return flat_for<0, sequence::sequence_count() - 1, 1, flat_sequence<sequence>>::flat_loop(std::forward<functor_return_type>(functor_return_arg), std::forward<functor_types>(functor_args)...);
  }
}

#undef t___
#undef c___

#undef tp___
#undef cp___

#undef tflat___
#undef cflat___
