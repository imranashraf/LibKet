//  Created by Michael Gazonda on 2014-12-24.
//  Copyright (c) 2014 Zed Mind Incorporated - http://zedmind.com. All rights reserved.
//  LICENSED UNDER CPOL 1.02 - http://www.codeproject.com/info/cpol10.aspx

// Compile Time Loops with C++11 - Creating a Generalized static_for Implementation
// http://www.codeproject.com/Articles/857354/Compile-Time-Loops-with-Cplusplus-Creating-a-Gener

#pragma once

// This is my preferred implementation
// I like constexpr over enums, and appreciate that declaration and definitions are separable
namespace zed
{
  template<long count, typename functor, long sequence_width = 70, typename functor_return_type, typename... functor_types>
  inline auto static_for(functor_return_type&& functor_return_arg, functor_types&&... functor_args);

  template<long start, long end, long step, typename functor, long sequence_width = 70, typename functor_return_type, typename... functor_types>
  inline auto static_for(functor_return_type&& functor_return_arg, functor_types&&... functor_args);

  template<long for_start, long for_end, long for_step, typename functor, long sequence_width, typename functor_return_type, typename... functor_types> struct static_for_impl
  {
    static inline auto loop(functor_return_type&& functor_return_arg, functor_types&&... functor_args);

  private:
    template<long pt_start, long pt_end, long pt_step> struct point
    {
      static constexpr long start_    { pt_start };
      static constexpr long end_    { pt_end };
      static constexpr long step_    { pt_step };
      static constexpr long count_    { (end_ - start_)/step_ + 1 };
      static constexpr bool is_end_point_  { count_ <= sequence_width };

      static constexpr long sequence_count();

    private:
      static constexpr long child_start(long index);
      static constexpr long child_end(long index);
      static constexpr long child_step(long index);
      static constexpr long points_in_sequence(long max);

    public:
      template<long index> using child_point = point<child_start(index), child_end(index), child_step(index)>;
    };

  public:
    template<long flat_start, long flat_end, long flat_step, class flat_functor> struct flat_for
    {
      static inline auto flat_loop(functor_return_type&& functor_return_arg, functor_types&&... functor_args);

      // private:
      static inline auto flat_next(std::integral_constant<long, flat_start + ( (flat_end - flat_start + flat_step) / flat_step) * flat_step>, functor_return_type&&, functor_types&&...);
      template<long index> static inline auto flat_next(std::integral_constant<long, index>, functor_return_type&& functor_return_arg, functor_types&&... functor_args);
    };

    template<typename sequence> struct flat_sequence
    {
      template<long index> static inline auto func(functor_return_type&& functor_return_arg, functor_types&&... functor_args);
    };

    template<typename sequence> static inline auto next(std::true_type, functor_return_type&& functor_return_arg, functor_types&&... functor_args);
    template<typename sequence> static inline auto next(std::false_type, functor_return_type&& functor_return_arg, functor_types&&... functor_args);
  };
}
#include "static_for.hpp"
