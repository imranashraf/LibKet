# Convert Latex to markdown syntax
sub subst() {
    my ($string) = (@_);

    # {\bf a}     => {**a**}
    $string =~ s|\{\\bf (.*?)\}|\{**$1**\}|g;
    
    # \bf a       => **a**
    $string =~ s|\\bf (.*?)|**$1**|g;

    # {\em a}     => {a}
    $string =~ s|\{\\em (.*?)\}|\{*$1*\}|g;
    
    # \em a       => *a*
    $string =~ s|\\em (.*?)|*$1*|g;

    # {\it a}     => {*a*}
    $string =~ s|\{\\it (.*?)\}|\{*$1*\}|g;
    
    # \it a       => *a*
    $string =~ s|\\it (.*?)|*$1*|g;
    
    # {\rm a}     => {a}
    $string =~ s|\{\\rm (.*?)\}|\{$1\}|g;

    # \rm a       => a
    $string =~ s|\\rm (.*?)|$1|g;

    
    # \mathbf{a}  => **a**
    $string =~ s|\\mathbf\{(.*?)\}|**$1**|g;

    # \frac{a}{b} => a/b
    $string =~ s|\\frac\{(.*?)\}\{(.*?)\}|$1/$2|g;

    # a_{b}       => a<sub>b</sub>
    $string =~ s|([^_]+)_\{([^}]+)\}|$1<sub>$2</sub>|g;
    
    # a_b         => a<sub>b</sub>
    $string =~ s|([^_]+)_(\S)|$1<sub>$2</sub>|g;

    # a^{b}       => a<sup>b</sup>
    $string =~ s|([^\^]+)\^\{([^}]+)\}|$1<sup>$2</sup>|g;
    
    # a^b         => a<sup>b</sup>
    $string =~ s|([^\^]+)\^(\S)|$1<sup>$2</sup>|g;

    # \|a\|       => &#124;&#124;a&#124;&#124;
    $string =~ s/\\\|(.*?)\\\|/&#124;&#124;$1&#124;&#124;/g;

    # \cdot       => &#183;
    $string =~ s/\\cdot(\s+)/&#183;/g;
    
    # \,          => <space>
    $string =~ s/\\,(\s+)/ /g;

    # \           => ''
    # This must be the last rule, otherwise the special characters are not detected!!!
    $string =~ s/\\//g;
    
    return $string;
}

# Pass all matches to previously defined, custom conversion subroutine
s/\\f\$(.+?)\\f\$/&subst($1)/ge;
