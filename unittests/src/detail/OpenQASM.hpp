/** @file unittests/src/detail/OpenQASM.hpp

    @brief LibKet quantum base classes and declarations

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
*/

#pragma once
#ifndef DETAIL_OPENQASM_HPP
#define DETAIL_OPENQASM_HPP

#ifdef LIBKET_WITH_OPENQASM

template<std::size_t _qubits>
struct Fixture_OpenQASM
{
public:
  LibKet::QData<_qubits, LibKet::QBackend::OpenQASMv2> data;

  template<typename Expr>
  bool run(const Expr& expr)
  {
    try {
      // Perform forced measurement at the end of the circuit
      auto expr_ = measure(all(expr));
      LibKet::utils::json result = expr_(data).execute();

      return result["success"].get<bool>();
    } catch (...) {
      return false;
    }
  }
};

#endif // LIBKET_WITH_OPENQASM
#endif // DETAIL_OPENQASM_HPP
