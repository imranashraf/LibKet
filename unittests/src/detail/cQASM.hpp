/** @file unittests/src/detail/cQASM.hpp

    @brief LibKet quantum base classes and declarations

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
*/

#pragma once
#ifndef DETAIL_CQASM_HPP
#define DETAIL_CQASM_HPP

#ifdef LIBKET_WITH_CQASM

template<std::size_t _qubits>
struct Fixture_cQASM
{
public:
  LibKet::QData<_qubits, LibKet::QBackend::cQASMv1> data;

  template<typename Expr>
  bool run(const Expr& expr)
  {
    try {
      LibKet::utils::json result = expr(data).execute();
      return result["raw_text"].get<std::string>().empty();
    } catch (...) {
      return false;
    }
  }
};

#endif // LIBKET_WITH_CQASM
#endif // DETAIL_CQASM_HPP
