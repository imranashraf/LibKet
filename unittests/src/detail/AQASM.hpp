/** @file unittests/src/detail/AQASM.hpp

    @brief LibKet quantum base classes and declarations

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
*/

#pragma once
#ifndef DETAIL_AQASM_HPP
#define DETAIL_AQASM_HPP

#ifdef LIBKET_WITH_AQASM

template<std::size_t _qubits>
struct Fixture_AQASM
{
public:
  LibKet::QData<_qubits, LibKet::QBackend::AQASM> data;

  template<typename Expr>
  bool run(const Expr& expr)
  {
    // Testing not yet supported
    return true;
  }
};

#endif // LIBKET_WITH_AQASM
#endif // DETAIL_AQASM_HPP
