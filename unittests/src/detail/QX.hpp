/** @file unittests/src/detail/QX.hpp

    @brief LibKet quantum base classes and declarations

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
*/

#pragma once
#ifndef DETAIL_QX_HPP
#define DETAIL_QX_HPP

#ifdef LIBKET_WITH_QX

template<std::size_t _qubits>
struct Fixture_QX
{
public:
  LibKet::QData<_qubits, LibKet::QBackend::QX> data;

  template<typename Expr>
  bool run(const Expr& expr)
  {
    try {
      qx::qu_register& qreg = expr(data).execute();
      return true;
    } catch (...) {
      return false;
    }
  }
};

#endif // LIBKET_WITH_QX
#endif // DETAIL_QX_HPP
