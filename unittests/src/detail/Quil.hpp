/** @file unittests/src/detail/Quil.hpp

    @brief LibKet quantum base classes and declarations

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
*/

#pragma once
#ifndef DETAIL_QUIL_HPP
#define DETAIL_QUIL_HPP

#ifdef LIBKET_WITH_QUIL

template<std::size_t _qubits>
struct Fixture_Quil
{
public:
  LibKet::QData<_qubits, LibKet::QBackend::Quil> data;

  template<typename Expr>
  bool run(const Expr& expr)
  {
    try {
      LibKet::utils::json result = expr(data).execute();
      return true;
    } catch (...) {
      return false;
    }
  }
};

#endif // LIBKET_WITH_QUIL
#endif // DETAIL_QUIL_HPP
