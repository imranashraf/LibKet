/** @file unittests/src/detail/qasm.hpp

    @brief LibKet quantum base classes and declarations

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
*/

#pragma once
#ifndef DETAIL_QASM_HPP
#define DETAIL_QASM_HPP

template<std::size_t _qubits>
struct Fixture_QASM
{
public:
  LibKet::QData<_qubits, LibKet::QBackend::QASM> data;

  template<typename Expr>
  bool run(const Expr& expr)
  {
    try {
      expr(data).compile();
      return true;
    } catch (...) {
      return false;
    }
  }
};

#endif // DETAIL_QASM_HPP
