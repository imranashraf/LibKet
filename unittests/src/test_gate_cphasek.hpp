/** @file test_gate_cphasek.hpp
 *
 *  @brief UnitTests++ LibKet::gate::cphasek() test
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_cphasek)
{
  try {
    // cphasek()
    auto f = cphasek<1>();
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCPhaseK pi/2^1\n| filter = QFilter\n|  "
      "expr0 = QFilter\n|  expr1 = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphasek<1>(all(),all())
    auto f = cphasek<1>(all(), all());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseK pi/2^1\n| filter = "
                "QFilterSelectAll\n|  "
                "expr0 = QFilterSelectAll\n|  expr1 = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphasek<1>(sel<0,1,3,5>(), sel<2,4,6,7>())
    auto f = cphasek<1>(sel<0, 1, 3, 5>(), sel<2, 4, 6, 7>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseK pi/2^1\n| filter = "
                "QFilterSelect [ 0 1 3 5 2 4 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 3 5 ]\n|  expr1 = QFilterSelect [ 2 4 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphasek<1>(range<0,3>(), range<4,7>())
    auto f = cphasek<1>(range<0, 3>(), range<4, 7>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseK pi/2^1\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphasek<1>(qureg<0,4>(), qureg<4,4>())
    auto f = cphasek<1>(qureg<0, 4>(), qureg<4, 4>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseK pi/2^1\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphasek<1>(qubit<1>(), qubit<2>())
    auto f = cphasek<1>(qubit<1>(), qubit<2>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCPhaseK pi/2^1\n| filter = QFilterSelect [ 1 2 "
      "]\n|  expr0 = QFilterSelect [ 1 ]\n|  expr1 = QFilterSelect [ 2 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CPHASEK<1>()
    auto f = CPHASEK<1>();
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCPhaseK pi/2^1\n| filter = QFilter\n|  "
      "expr0 = QFilter\n|  expr1 = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CPHASEK<1>(all(), all())
    auto f = CPHASEK<1>(all(), all());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseK pi/2^1\n| filter = "
                "QFilterSelectAll\n|  "
                "expr0 = QFilterSelectAll\n|  expr1 = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CPHASEK<1>(sel<0,1,3,5>(), sel<2,4,6,7>())
    auto f = CPHASEK<1>(sel<0, 1, 3, 5>(), sel<2, 4, 6, 7>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseK pi/2^1\n| filter = "
                "QFilterSelect [ 0 1 3 5 2 4 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 3 5 ]\n|  expr1 = QFilterSelect [ 2 4 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CPHASEK<1>(range<0,3>(), range<4,7>())
    auto f = CPHASEK<1>(range<0, 3>(), range<4, 7>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseK pi/2^1\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CPHASEK<1>(qureg<0,4>(), qureg<4,4>())
    auto f = CPHASEK<1>(qureg<0, 4>(), qureg<4, 4>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseK pi/2^1\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CPHASEK<1>(qubit<1>(), qubit<2>())
    auto f = CPHASEK<1>(qubit<1>(), qubit<2>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCPhaseK pi/2^1\n| filter = QFilterSelect [ 1 2 "
      "]\n|  expr0 = QFilterSelect [ 1 ]\n|  expr1 = QFilterSelect [ 2 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // crk<1>()
    auto f = crk<1>();
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCPhaseK pi/2^1\n| filter = QFilter\n|  "
      "expr0 = QFilter\n|  expr1 = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // crk<1>(all(), all())
    auto f = crk<1>(all(), all());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseK pi/2^1\n| filter = "
                "QFilterSelectAll\n|  "
                "expr0 = QFilterSelectAll\n|  expr1 = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // crk<1>(sel<0,1,3,5>(), sel<2,4,6,7>())
    auto f = crk<1>(sel<0, 1, 3, 5>(), sel<2, 4, 6, 7>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseK pi/2^1\n| filter = "
                "QFilterSelect [ 0 1 3 5 2 4 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 3 5 ]\n|  expr1 = QFilterSelect [ 2 4 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // crk<1>(range<0,3>(), range<4,7>())
    auto f = crk<1>(range<0, 3>(), range<4, 7>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseK pi/2^1\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // crk<1>(qureg<0,4>(), qureg<4,4>())
    auto f = crk<1>(qureg<0, 4>(), qureg<4, 4>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseK pi/2^1\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // crk<1>(qubit<1>(), qubit<2>())
    auto f = crk<1>(qubit<1>(), qubit<2>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCPhaseK pi/2^1\n| filter = QFilterSelect [ 1 2 "
      "]\n|  expr0 = QFilterSelect [ 1 ]\n|  expr1 = QFilterSelect [ 2 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CRK<1>()
    auto f = CRK<1>();
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCPhaseK pi/2^1\n| filter = QFilter\n|  "
      "expr0 = QFilter\n|  expr1 = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CRK<1>(all(), all())
    auto f = CRK<1>(all(), all());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseK pi/2^1\n| filter = "
                "QFilterSelectAll\n|  "
                "expr0 = QFilterSelectAll\n|  expr1 = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CRK<1>(sel<0,1,3,5>(), sel<2,4,6,7>())
    auto f = CRK<1>(sel<0, 1, 3, 5>(), sel<2, 4, 6, 7>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseK pi/2^1\n| filter = "
                "QFilterSelect [ 0 1 3 5 2 4 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 3 5 ]\n|  expr1 = QFilterSelect [ 2 4 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CRK<1>(range<0,3>(), range<4,7>())
    auto f = CRK<1>(range<0, 3>(), range<4, 7>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseK pi/2^1\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CRK<1>(qureg<0,4>(), qureg<4,4>())
    auto f = CRK<1>(qureg<0, 4>(), qureg<4, 4>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseK pi/2^1\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CRK<1>(qubit<1>(), qubit<2>())
    auto f = CRK<1>(qubit<1>(), qubit<2>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCPhaseK pi/2^1\n| filter = QFilterSelect [ 1 2 "
      "]\n|  expr0 = QFilterSelect [ 1 ]\n|  expr1 = QFilterSelect [ 2 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QCPhaseK<1>()
    auto f = QCPhaseK<1>();
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(), "QCPhaseK pi/2^1\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QCPhaseK<1>()(all(), all())
    auto f = QCPhaseK<1>()(all(), all());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseK pi/2^1\n| filter = "
                "QFilterSelectAll\n|  "
                "expr0 = QFilterSelectAll\n|  expr1 = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QCPhaseK<1>()(sel<0,1,3,5>(), sel<2,4,6,7>())
    auto f = QCPhaseK<1>()(sel<0, 1, 3, 5>(), sel<2, 4, 6, 7>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseK pi/2^1\n| filter = "
                "QFilterSelect [ 0 1 3 5 2 4 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 3 5 ]\n|  expr1 = QFilterSelect [ 2 4 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QCPhaseK<1>()(range<0,3>(), range<4,7>())
    auto f = QCPhaseK<1>()(range<0, 3>(), range<4, 7>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseK pi/2^1\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QCPhaseK<1>()(qureg<0,4>(), qureg<4,4>())
    auto f = QCPhaseK<1>()(qureg<0, 4>(), qureg<4, 4>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseK pi/2^1\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QCPhaseK<1>()(qubit<1>(), qubit<2>())
    auto f = QCPhaseK<1>()(qubit<1>(), qubit<2>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCPhaseK pi/2^1\n| filter = QFilterSelect [ 1 2 "
      "]\n|  expr0 = QFilterSelect [ 1 ]\n|  expr1 = QFilterSelect [ 2 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }
}
