/** @file test_gate_cnot.hpp
 *
 *  @brief UnitTests++ LibKet::gate::cnot() test
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_cnot)
{
  try {
    // cnot()
    auto f = cnot();
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCNOT\n| filter = QFilter\n|  expr0 = "
                "QFilter\n|  expr1 = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cnot(all(),all())
    auto f = cnot(all(), all());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCNOT\n| filter = QFilterSelectAll\n| "
                " expr0 = QFilterSelectAll\n|  expr1 = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cnot(sel<0,1,3,5>(), sel<2,4,6,7>())
    auto f = cnot(sel<0, 1, 3, 5>(), sel<2, 4, 6, 7>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCNOT\n| filter = QFilterSelect [ 0 1 "
                "3 5 2 4 6 7 ]\n|  expr0 = QFilterSelect [ 0 1 3 5 ]\n|  expr1 "
                "= QFilterSelect [ 2 4 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cnot(range<0,3>(), range<4,7>())
    auto f = cnot(range<0, 3>(), range<4, 7>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCNOT\n| filter = QFilterSelect [ 0 1 "
                "2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ 0 1 2 3 ]\n|  expr1 "
                "= QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cnot(qureg<0,4>(), qureg<4,4>())
    auto f = cnot(qureg<0, 4>(), qureg<4, 4>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCNOT\n| filter = QFilterSelect [ 0 1 "
                "2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ 0 1 2 3 ]\n|  expr1 "
                "= QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cnot(qubit<1>(), qubit<2>())
    auto f = cnot(qubit<1>(), qubit<2>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCNOT\n| filter = QFilterSelect [ 1 2 ]\n|  "
      "expr0 = QFilterSelect [ 1 ]\n|  expr1 = QFilterSelect [ 2 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CNOT()
    auto f = CNOT();
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCNOT\n| filter = QFilter\n|  expr0 = "
                "QFilter\n|  expr1 = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CNOT(all(), all())
    auto f = CNOT(all(), all());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCNOT\n| filter = QFilterSelectAll\n| "
                " expr0 = QFilterSelectAll\n|  expr1 = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CNOT(sel<0,1,3,5>(), sel<2,4,6,7>())
    auto f = CNOT(sel<0, 1, 3, 5>(), sel<2, 4, 6, 7>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCNOT\n| filter = QFilterSelect [ 0 1 "
                "3 5 2 4 6 7 ]\n|  expr0 = QFilterSelect [ 0 1 3 5 ]\n|  expr1 "
                "= QFilterSelect [ 2 4 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CNOT(range<0,3>(), range<4,7>())
    auto f = CNOT(range<0, 3>(), range<4, 7>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCNOT\n| filter = QFilterSelect [ 0 1 "
                "2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ 0 1 2 3 ]\n|  expr1 "
                "= QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CNOT(qureg<0,4>(), qureg<4,4>())
    auto f = CNOT(qureg<0, 4>(), qureg<4, 4>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCNOT\n| filter = QFilterSelect [ 0 1 "
                "2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ 0 1 2 3 ]\n|  expr1 "
                "= QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CNOT(qubit<1>(), qubit<2>())
    auto f = CNOT(qubit<1>(), qubit<2>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCNOT\n| filter = QFilterSelect [ 1 2 ]\n|  "
      "expr0 = QFilterSelect [ 1 ]\n|  expr1 = QFilterSelect [ 2 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QCNOT()
    auto f = QCNOT();
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(), "QCNOT\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QCNOT()(all(), all())
    auto f = QCNOT()(all(), all());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCNOT\n| filter = QFilterSelectAll\n| "
                " expr0 = QFilterSelectAll\n|  expr1 = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QCNOT()(sel<0,1,3,5>(), sel<2,4,6,7>())
    auto f = QCNOT()(sel<0, 1, 3, 5>(), sel<2, 4, 6, 7>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCNOT\n| filter = QFilterSelect [ 0 1 "
                "3 5 2 4 6 7 ]\n|  expr0 = QFilterSelect [ 0 1 3 5 ]\n|  expr1 "
                "= QFilterSelect [ 2 4 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QCNOT()(range<0,3>(), range<4,7>())
    auto f = QCNOT()(range<0, 3>(), range<4, 7>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCNOT\n| filter = QFilterSelect [ 0 1 "
                "2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ 0 1 2 3 ]\n|  expr1 "
                "= QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QCNOT()(qureg<0,4>(), qureg<4,4>())
    auto f = QCNOT()(qureg<0, 4>(), qureg<4, 4>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCNOT\n| filter = QFilterSelect [ 0 1 "
                "2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ 0 1 2 3 ]\n|  expr1 "
                "= QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QCNOT()(qubit<1>(), qubit<2>())
    auto f = QCNOT()(qubit<1>(), qubit<2>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCNOT\n| filter = QFilterSelect [ 1 2 ]\n|  "
      "expr0 = QFilterSelect [ 1 ]\n|  expr1 = QFilterSelect [ 2 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cx()
    auto f = cx();
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCNOT\n| filter = QFilter\n|  expr0 = "
                "QFilter\n|  expr1 = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cx(all(),all())
    auto f = cx(all(), all());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCNOT\n| filter = QFilterSelectAll\n| "
                " expr0 = QFilterSelectAll\n|  expr1 = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cx(sel<0,1,3,5>(), sel<2,4,6,7>())
    auto f = cx(sel<0, 1, 3, 5>(), sel<2, 4, 6, 7>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCNOT\n| filter = QFilterSelect [ 0 1 "
                "3 5 2 4 6 7 ]\n|  expr0 = QFilterSelect [ 0 1 3 5 ]\n|  expr1 "
                "= QFilterSelect [ 2 4 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cx(range<0,3>(), range<4,7>())
    auto f = cx(range<0, 3>(), range<4, 7>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCNOT\n| filter = QFilterSelect [ 0 1 "
                "2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ 0 1 2 3 ]\n|  expr1 "
                "= QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cx(qureg<0,4>(), qureg<4,4>())
    auto f = cx(qureg<0, 4>(), qureg<4, 4>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCNOT\n| filter = QFilterSelect [ 0 1 "
                "2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ 0 1 2 3 ]\n|  expr1 "
                "= QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cx(qubit<1>(), qubit<2>())
    auto f = cx(qubit<1>(), qubit<2>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCNOT\n| filter = QFilterSelect [ 1 2 ]\n|  "
      "expr0 = QFilterSelect [ 1 ]\n|  expr1 = QFilterSelect [ 2 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CX()
    auto f = CX();
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCNOT\n| filter = QFilter\n|  expr0 = "
                "QFilter\n|  expr1 = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CX(all(),all())
    auto f = CX(all(), all());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCNOT\n| filter = QFilterSelectAll\n| "
                " expr0 = QFilterSelectAll\n|  expr1 = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CX(sel<0,1,3,5>(), sel<2,4,6,7>())
    auto f = CX(sel<0, 1, 3, 5>(), sel<2, 4, 6, 7>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCNOT\n| filter = QFilterSelect [ 0 1 "
                "3 5 2 4 6 7 ]\n|  expr0 = QFilterSelect [ 0 1 3 5 ]\n|  expr1 "
                "= QFilterSelect [ 2 4 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CX(range<0,3>(), range<4,7>())
    auto f = CX(range<0, 3>(), range<4, 7>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCNOT\n| filter = QFilterSelect [ 0 1 "
                "2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ 0 1 2 3 ]\n|  expr1 "
                "= QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CX(qureg<0,4>(), qureg<4,4>())
    auto f = CX(qureg<0, 4>(), qureg<4, 4>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCNOT\n| filter = QFilterSelect [ 0 1 "
                "2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ 0 1 2 3 ]\n|  expr1 "
                "= QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CX(qubit<1>(), qubit<2>())
    auto f = CX(qubit<1>(), qubit<2>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCNOT\n| filter = QFilterSelect [ 1 2 ]\n|  "
      "expr0 = QFilterSelect [ 1 ]\n|  expr1 = QFilterSelect [ 2 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }
}
