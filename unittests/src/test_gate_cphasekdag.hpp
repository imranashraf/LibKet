/** @file test_gate_cphasekdag.hpp
 *
 *  @brief UnitTests++ LibKet::gate::cphasekdag() test
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_cphasekdag)
{
  try {
    // cphasekdag()
    auto f = cphasekdag<1>();
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCPhaseKdag pi/2^1\n| filter = QFilter\n|  "
      "expr0 = QFilter\n|  expr1 = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphasekdag<1>(all(),all())
    auto f = cphasekdag<1>(all(), all());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseKdag pi/2^1\n| filter = "
                "QFilterSelectAll\n|  "
                "expr0 = QFilterSelectAll\n|  expr1 = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphasekdag<1>(sel<0,1,3,5>(), sel<2,4,6,7>())
    auto f = cphasekdag<1>(sel<0, 1, 3, 5>(), sel<2, 4, 6, 7>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseKdag pi/2^1\n| filter = "
                "QFilterSelect [ 0 1 3 5 2 4 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 3 5 ]\n|  expr1 = QFilterSelect [ 2 4 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphasekdag<1>(range<0,3>(), range<4,7>())
    auto f = cphasekdag<1>(range<0, 3>(), range<4, 7>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseKdag pi/2^1\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphasekdag<1>(qureg<0,4>(), qureg<4,4>())
    auto f = cphasekdag<1>(qureg<0, 4>(), qureg<4, 4>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseKdag pi/2^1\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphasekdag<1>(qubit<1>(), qubit<2>())
    auto f = cphasekdag<1>(qubit<1>(), qubit<2>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCPhaseKdag pi/2^1\n| filter = QFilterSelect [ "
      "1 2 "
      "]\n|  expr0 = QFilterSelect [ 1 ]\n|  expr1 = QFilterSelect [ 2 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CPHASEKDAG<1>()
    auto f = CPHASEKDAG<1>();
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCPhaseKdag pi/2^1\n| filter = QFilter\n|  "
      "expr0 = QFilter\n|  expr1 = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CPHASEKDAG<1>(all(), all())
    auto f = CPHASEKDAG<1>(all(), all());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseKdag pi/2^1\n| filter = "
                "QFilterSelectAll\n|  "
                "expr0 = QFilterSelectAll\n|  expr1 = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CPHASEKDAG<1>(sel<0,1,3,5>(), sel<2,4,6,7>())
    auto f = CPHASEKDAG<1>(sel<0, 1, 3, 5>(), sel<2, 4, 6, 7>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseKdag pi/2^1\n| filter = "
                "QFilterSelect [ 0 1 3 5 2 4 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 3 5 ]\n|  expr1 = QFilterSelect [ 2 4 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CPHASEKDAG<1>(range<0,3>(), range<4,7>())
    auto f = CPHASEKDAG<1>(range<0, 3>(), range<4, 7>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseKdag pi/2^1\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CPHASEKDAG<1>(qureg<0,4>(), qureg<4,4>())
    auto f = CPHASEKDAG<1>(qureg<0, 4>(), qureg<4, 4>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseKdag pi/2^1\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CPHASEKDAG<1>(qubit<1>(), qubit<2>())
    auto f = CPHASEKDAG<1>(qubit<1>(), qubit<2>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCPhaseKdag pi/2^1\n| filter = QFilterSelect [ "
      "1 2 "
      "]\n|  expr0 = QFilterSelect [ 1 ]\n|  expr1 = QFilterSelect [ 2 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // crkdag<1>()
    auto f = crkdag<1>();
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCPhaseKdag pi/2^1\n| filter = QFilter\n|  "
      "expr0 = QFilter\n|  expr1 = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // crkdag<1>(all(), all())
    auto f = crkdag<1>(all(), all());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseKdag pi/2^1\n| filter = "
                "QFilterSelectAll\n|  "
                "expr0 = QFilterSelectAll\n|  expr1 = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // crkdag<1>(sel<0,1,3,5>(), sel<2,4,6,7>())
    auto f = crkdag<1>(sel<0, 1, 3, 5>(), sel<2, 4, 6, 7>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseKdag pi/2^1\n| filter = "
                "QFilterSelect [ 0 1 3 5 2 4 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 3 5 ]\n|  expr1 = QFilterSelect [ 2 4 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // crkdag<1>(range<0,3>(), range<4,7>())
    auto f = crkdag<1>(range<0, 3>(), range<4, 7>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseKdag pi/2^1\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // crkdag<1>(qureg<0,4>(), qureg<4,4>())
    auto f = crkdag<1>(qureg<0, 4>(), qureg<4, 4>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseKdag pi/2^1\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // crkdag<1>(qubit<1>(), qubit<2>())
    auto f = crkdag<1>(qubit<1>(), qubit<2>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCPhaseKdag pi/2^1\n| filter = QFilterSelect [ "
      "1 2 "
      "]\n|  expr0 = QFilterSelect [ 1 ]\n|  expr1 = QFilterSelect [ 2 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CRKDAG<1>()
    auto f = CRKDAG<1>();
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCPhaseKdag pi/2^1\n| filter = QFilter\n|  "
      "expr0 = QFilter\n|  expr1 = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CRKDAG<1>(all(), all())
    auto f = CRKDAG<1>(all(), all());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseKdag pi/2^1\n| filter = "
                "QFilterSelectAll\n|  "
                "expr0 = QFilterSelectAll\n|  expr1 = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CRKDAG<1>(sel<0,1,3,5>(), sel<2,4,6,7>())
    auto f = CRKDAG<1>(sel<0, 1, 3, 5>(), sel<2, 4, 6, 7>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseKdag pi/2^1\n| filter = "
                "QFilterSelect [ 0 1 3 5 2 4 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 3 5 ]\n|  expr1 = QFilterSelect [ 2 4 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CRKDAG<1>(range<0,3>(), range<4,7>())
    auto f = CRKDAG<1>(range<0, 3>(), range<4, 7>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseKdag pi/2^1\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CRKDAG<1>(qureg<0,4>(), qureg<4,4>())
    auto f = CRKDAG<1>(qureg<0, 4>(), qureg<4, 4>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseKdag pi/2^1\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CRKDAG<1>(qubit<1>(), qubit<2>())
    auto f = CRKDAG<1>(qubit<1>(), qubit<2>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCPhaseKdag pi/2^1\n| filter = QFilterSelect [ "
      "1 2 "
      "]\n|  expr0 = QFilterSelect [ 1 ]\n|  expr1 = QFilterSelect [ 2 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QCPhaseKdag<1>()
    auto f = QCPhaseKdag<1>();
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(), "QCPhaseKdag pi/2^1\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QCPhaseKdag<1>()(all(), all())
    auto f = QCPhaseKdag<1>()(all(), all());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseKdag pi/2^1\n| filter = "
                "QFilterSelectAll\n|  "
                "expr0 = QFilterSelectAll\n|  expr1 = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QCPhaseKdag<1>()(sel<0,1,3,5>(), sel<2,4,6,7>())
    auto f = QCPhaseKdag<1>()(sel<0, 1, 3, 5>(), sel<2, 4, 6, 7>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseKdag pi/2^1\n| filter = "
                "QFilterSelect [ 0 1 3 5 2 4 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 3 5 ]\n|  expr1 = QFilterSelect [ 2 4 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QCPhaseKdag<1>()(range<0,3>(), range<4,7>())
    auto f = QCPhaseKdag<1>()(range<0, 3>(), range<4, 7>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseKdag pi/2^1\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QCPhaseKdag<1>()(qureg<0,4>(), qureg<4,4>())
    auto f = QCPhaseKdag<1>()(qureg<0, 4>(), qureg<4, 4>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhaseKdag pi/2^1\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QCPhaseKdag<1>()(qubit<1>(), qubit<2>())
    auto f = QCPhaseKdag<1>()(qubit<1>(), qubit<2>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCPhaseKdag pi/2^1\n| filter = QFilterSelect [ "
      "1 2 "
      "]\n|  expr0 = QFilterSelect [ 1 ]\n|  expr1 = QFilterSelect [ 2 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }
}
