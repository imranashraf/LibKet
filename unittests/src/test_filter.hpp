/** @file unittests/src/test_filter.hpp
 *
 *  @brief LibKet quantum filter tests
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;

TEST_FIXTURE(Fixture, filter)
{
  try {
    // all(all()) == all()
    auto f = all(all());
    auto g = all();
    CHECK((std::is_same<decltype(f), decltype(g)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // all(sel<0,1,2,3>()) == all()
    auto f = all(sel<0, 1, 2, 3>());
    auto g = all();
    CHECK((std::is_same<decltype(f), decltype(g)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // all(range<0,3>()) == all()
    auto f = all(range<0, 3>());
    auto g = all();
    CHECK((std::is_same<decltype(f), decltype(g)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // all(qureg<0,4>()) == all()
    auto f = all(qureg<0, 4>());
    auto g = all();
    CHECK((std::is_same<decltype(f), decltype(g)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // all(qubit<0>()) == all()
    auto f = all(qubit<0>());
    auto g = all();
    CHECK((std::is_same<decltype(f), decltype(g)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // sel<0,1,2,3>(all()) == sel<0,1,2,3>()
    auto f = sel<0, 1, 2, 3>(all());
    auto g = sel<0, 1, 2, 3>();
    CHECK((std::is_same<decltype(f), decltype(g)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // sel<1,3>(sel<0,1,2,3>()) == sel<1,3>()
    auto f = sel<1, 3>(sel<0, 1, 2, 3>());
    auto g = sel<1, 3>();
    CHECK((std::is_same<decltype(f), decltype(g)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // sel<1,3>(range<0,3>()) == sel<1,3>()
    auto f = sel<1, 3>(range<0, 3>());
    auto g = sel<1, 3>();
    CHECK((std::is_same<decltype(f), decltype(g)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // sel<1,3>(qureg<0,4>()) == sel<1,3>()
    auto f = sel<1, 3>(qureg<0, 4>());
    auto g = sel<1, 3>();
    CHECK((std::is_same<decltype(f), decltype(g)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // sel<0>(qubit<3>()) == sel<3>()
    auto f = sel<0>(qubit<3>());
    auto g = sel<3>();
    CHECK((std::is_same<decltype(f), decltype(g)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // range<0,3>(all()) == range<0,3>()
    auto f = range<0, 3>(all());
    auto g = range<0, 3>();
    CHECK((std::is_same<decltype(f), decltype(g)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // range<0,3>(sel<5,6,7,8,9,10>()) == sel<5,6,7,8>()
    auto f = range<0, 3>(sel<5, 6, 7, 8, 9, 10>());
    auto g = sel<5, 6, 7, 8>();
    CHECK((std::is_same<decltype(f), decltype(g)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // range<0,3>(range<5,10>()) == sel<5,6,7,8>()
    auto f = range<0, 3>(range<5, 10>());
    auto g = sel<5, 6, 7, 8>();
    CHECK((std::is_same<decltype(f), decltype(g)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // range<0,3>(qureg<5,6>()) == sel<5,6,7,8>()
    auto f = range<0, 3>(qureg<5, 6>());
    auto g = sel<5, 6, 7, 8>();
    CHECK((std::is_same<decltype(f), decltype(g)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // range<0,0>(qubit<5>()) == qubit<5>()
    auto f = range<0, 0>(qubit<5>());
    auto g = sel<5>();
    CHECK((std::is_same<decltype(f), decltype(g)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // qureg<3,6>(all()) == range<3,8>()
    auto f = qureg<3, 6>(all());
    auto g = range<3, 8>();
    CHECK((std::is_same<decltype(f), decltype(g)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // qureg<3,6>(sel<1,2,3,4,5,6,7,8,9,10>()) == range<4,9>()
    auto f = qureg<3, 6>(sel<1, 2, 3, 4, 5, 6, 7, 8, 9, 10>());
    auto g = range<4, 9>();
    CHECK((std::is_same<decltype(f), decltype(g)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // qureg<3,6>(range<1,10>()) == range<4,9>()
    auto f = qureg<3, 6>(range<1, 10>());
    auto g = range<4, 9>();
    CHECK((std::is_same<decltype(f), decltype(g)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // qureg<3,6>(qureg<1,10>()) == range<4,9>()
    auto f = qureg<3, 6>(qureg<1, 10>());
    auto g = range<4, 9>();
    CHECK((std::is_same<decltype(f), decltype(g)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // qureg<0,1>(qubit<1>()) == sel<1>()
    auto f = qureg<0, 1>(qubit<1>());
    auto g = sel<1>();
    CHECK((std::is_same<decltype(f), decltype(g)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // qubit<1>(all()) == sel<1>()
    auto f = qubit<1>(all());
    auto g = sel<1>();
    CHECK((std::is_same<decltype(f), decltype(g)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // qubit<1>(sel<1,2,3,4,5>()) == sel<2>()
    auto f = qubit<1>(sel<1, 2, 3, 4, 5>());
    auto g = sel<2>();
    CHECK((std::is_same<decltype(f), decltype(g)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // qubit<1>(range<1,5>()) == sel<2>()
    auto f = qubit<1>(range<1, 5>());
    auto g = sel<2>();
    CHECK((std::is_same<decltype(f), decltype(g)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // qubit<1>(qureg<1,5>()) == sel<2>()
    auto f = qubit<1>(qureg<1, 5>());
    auto g = sel<2>();
    CHECK((std::is_same<decltype(f), decltype(g)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // qubit<0>(qubit<1>()) == sel<1>()
    auto f = qubit<0>(qubit<1>());
    auto g = sel<1>();
    CHECK((std::is_same<decltype(f), decltype(g)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }
}
