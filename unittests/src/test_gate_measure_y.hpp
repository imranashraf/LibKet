/** @file test_gate_measure_y.hpp
 *
 *  @brief UnitTests++ LibKet::gate::measure_y() test
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_measure_y)
{
  try {
    // measure_y()
    auto f = LibKet::gates::measure_y();
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QMeasure_Y\n| filter = QFilter\n|   expr "
      "= QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // measure_y(all())
    auto f = LibKet::gates::measure_y(all());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QMeasure_Y\n| filter = "
                "QFilterSelectAll\n|   expr = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // measure_y(sel<0,1,3,5>())
    auto f = LibKet::gates::measure_y(sel<0, 1, 3, 5>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QMeasure_Y\n| filter = QFilterSelect [ 0 "
      "1 3 5 ]\n|   expr = QFilterSelect [ 0 1 3 5 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // measure_y(range<0,3>())
    auto f = LibKet::gates::measure_y(range<0, 3>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QMeasure_Y\n| filter = QFilterSelect [ 0 "
      "1 2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // measure_y(qureg<0,4>())
    auto f = LibKet::gates::measure_y(qureg<0, 4>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QMeasure_Y\n| filter = QFilterSelect [ 0 "
      "1 2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // measure_y(qubit<1>())
    auto f = LibKet::gates::measure_y(qubit<1>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QMeasure_Y\n| filter = QFilterSelect [ 1 "
      "]\n|   expr = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // MEASURE_Y()
    auto f = LibKet::gates::MEASURE_Y();
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QMeasure_Y\n| filter = QFilter\n|   expr "
      "= QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // MEASURE_Y(all())
    auto f = LibKet::gates::MEASURE_Y(all());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QMeasure_Y\n| filter = "
                "QFilterSelectAll\n|   expr = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // MEASURE_Y(sel<0,1,3,5>())
    auto f = LibKet::gates::MEASURE_Y(sel<0, 1, 3, 5>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QMeasure_Y\n| filter = QFilterSelect [ 0 "
      "1 3 5 ]\n|   expr = QFilterSelect [ 0 1 3 5 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // MEASURE_Y(range<0,3>())
    auto f = LibKet::gates::MEASURE_Y(range<0, 3>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QMeasure_Y\n| filter = QFilterSelect [ 0 "
      "1 2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // MEASURE_Y(qureg<0,4>())
    auto f = LibKet::gates::MEASURE_Y(qureg<0, 4>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QMeasure_Y\n| filter = QFilterSelect [ 0 "
      "1 2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // MEASURE_Y(qubit<1>())
    auto f = LibKet::gates::MEASURE_Y(qubit<1>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QMeasure_Y\n| filter = QFilterSelect [ 1 "
      "]\n|   expr = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QMeasure_Y()
    auto f = QMeasure_Y();
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(), "QMeasure_Y\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QMeasure_Y()(all())
    auto f = QMeasure_Y()(all());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QMeasure_Y\n| filter = "
                "QFilterSelectAll\n|   expr = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QMeasure_Y()(sel<0,1,3,5>())
    auto f = QMeasure_Y()(sel<0, 1, 3, 5>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QMeasure_Y\n| filter = QFilterSelect [ 0 "
      "1 3 5 ]\n|   expr = QFilterSelect [ 0 1 3 5 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QMeasure_Y()(range<0,3>())
    auto f = QMeasure_Y()(range<0, 3>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QMeasure_Y\n| filter = QFilterSelect [ 0 "
      "1 2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QMeasure_Y()(qureg<0,4>())
    auto f = QMeasure_Y()(qureg<0, 4>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QMeasure_Y\n| filter = QFilterSelect [ 0 "
      "1 2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QMeasure_Y()(qubit<1>())
    auto f = QMeasure_Y()(qubit<1>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QMeasure_Y\n| filter = QFilterSelect [ 1 "
      "]\n|   expr = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }
}
