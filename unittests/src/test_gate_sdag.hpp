/** @file test_gate_sdag.hpp
 *
 *  @brief UnitTests++ LibKet::gate::sdag() test
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_sdag)
{
  try {
    // sdag()
    auto f = sdag();
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QSdag\n| filter = QFilter\n|   expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // sdag(all())
    auto f = sdag(all());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QSdag\n| filter = QFilterSelectAll\n|  "
                " expr = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // sdag(sel<0,1,3,5>())
    auto f = sdag(sel<0, 1, 3, 5>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QSdag\n| filter = QFilterSelect [ 0 1 "
                "3 5 ]\n|   expr = QFilterSelect [ 0 1 3 5 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // sdag(range<0,3>())
    auto f = sdag(range<0, 3>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QSdag\n| filter = QFilterSelect [ 0 1 "
                "2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // sdag(qureg<0,4>())
    auto f = sdag(qureg<0, 4>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QSdag\n| filter = QFilterSelect [ 0 1 "
                "2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // sdag(qubit<1>())
    auto f = sdag(qubit<1>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QSdag\n| filter = QFilterSelect [ 1 "
                "]\n|   expr = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // Sdag()
    auto f = Sdag();
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QSdag\n| filter = QFilter\n|   expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // Sdag(all())
    auto f = Sdag(all());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QSdag\n| filter = QFilterSelectAll\n|  "
                " expr = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // Sdag(sel<0,1,3,5>())
    auto f = Sdag(sel<0, 1, 3, 5>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QSdag\n| filter = QFilterSelect [ 0 1 "
                "3 5 ]\n|   expr = QFilterSelect [ 0 1 3 5 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // Sdag(range<0,3>())
    auto f = Sdag(range<0, 3>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QSdag\n| filter = QFilterSelect [ 0 1 "
                "2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // Sdag(qureg<0,4>())
    auto f = Sdag(qureg<0, 4>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QSdag\n| filter = QFilterSelect [ 0 1 "
                "2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // Sdag(qubit<1>())
    auto f = Sdag(qubit<1>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QSdag\n| filter = QFilterSelect [ 1 "
                "]\n|   expr = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // SDAG()
    auto f = SDAG();
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QSdag\n| filter = QFilter\n|   expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // SDAG(all())
    auto f = SDAG(all());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QSdag\n| filter = QFilterSelectAll\n|  "
                " expr = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // SDAG(sel<0,1,3,5>())
    auto f = SDAG(sel<0, 1, 3, 5>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QSdag\n| filter = QFilterSelect [ 0 1 "
                "3 5 ]\n|   expr = QFilterSelect [ 0 1 3 5 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // SDAG(range<0,3>())
    auto f = SDAG(range<0, 3>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QSdag\n| filter = QFilterSelect [ 0 1 "
                "2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // SDAG(qureg<0,4>())
    auto f = SDAG(qureg<0, 4>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QSdag\n| filter = QFilterSelect [ 0 1 "
                "2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // SDAG(qubit<1>())
    auto f = SDAG(qubit<1>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QSdag\n| filter = QFilterSelect [ 1 "
                "]\n|   expr = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QSdag()
    auto f = QSdag();
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(), "QSdag\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QSdag()(all())
    auto f = QSdag()(all());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QSdag\n| filter = QFilterSelectAll\n|  "
                " expr = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QSdag()(sel<0,1,3,5>())
    auto f = QSdag()(sel<0, 1, 3, 5>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QSdag\n| filter = QFilterSelect [ 0 1 "
                "3 5 ]\n|   expr = QFilterSelect [ 0 1 3 5 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QSdag()(range<0,3>())
    auto f = QSdag()(range<0, 3>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QSdag\n| filter = QFilterSelect [ 0 1 "
                "2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QSdag()(qureg<0,4>())
    auto f = QSdag()(qureg<0, 4>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QSdag\n| filter = QFilterSelect [ 0 1 "
                "2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QSdag()(qubit<1>())
    auto f = QSdag()(qubit<1>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QSdag\n| filter = QFilterSelect [ 1 "
                "]\n|   expr = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }
}
