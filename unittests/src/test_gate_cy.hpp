/** @file test_gate_cy.hpp
 *
 *  @brief UnitTests++ LibKet::gate::cy() test
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_cy)
{
  try {
    // cy()
    auto f = cy();
    std::stringstream ss;
    show<99>(f);
    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCY\n| filter = QFilter\n|  expr0 = "
                "QFilter\n|  expr1 = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cy(all(),all())
    auto f = cy(all(), all());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCY\n| filter = QFilterSelectAll\n| "
                " expr0 = QFilterSelectAll\n|  expr1 = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cy(sel<0,1,3,5>(), sel<2,4,6,7>())
    auto f = cy(sel<0, 1, 3, 5>(), sel<2, 4, 6, 7>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCY\n| filter = QFilterSelect [ 0 1 "
                "3 5 2 4 6 7 ]\n|  expr0 = QFilterSelect [ 0 1 3 5 ]\n|  expr1 "
                "= QFilterSelect [ 2 4 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cy(range<0,3>(), range<4,7>())
    auto f = cy(range<0, 3>(), range<4, 7>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCY\n| filter = QFilterSelect [ 0 1 "
                "2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ 0 1 2 3 ]\n|  expr1 "
                "= QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cy(qureg<0,4>(), qureg<4,4>())
    auto f = cy(qureg<0, 4>(), qureg<4, 4>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCY\n| filter = QFilterSelect [ 0 1 "
                "2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ 0 1 2 3 ]\n|  expr1 "
                "= QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cy(qubit<1>(), qubit<2>())
    auto f = cy(qubit<1>(), qubit<2>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCY\n| filter = QFilterSelect [ 1 2 ]\n|  "
      "expr0 = QFilterSelect [ 1 ]\n|  expr1 = QFilterSelect [ 2 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CY()
    auto f = CY();
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCY\n| filter = QFilter\n|  expr0 = "
                "QFilter\n|  expr1 = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CY(all(), all())
    auto f = CY(all(), all());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCY\n| filter = QFilterSelectAll\n| "
                " expr0 = QFilterSelectAll\n|  expr1 = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CY(sel<0,1,3,5>(), sel<2,4,6,7>())
    auto f = CY(sel<0, 1, 3, 5>(), sel<2, 4, 6, 7>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCY\n| filter = QFilterSelect [ 0 1 "
                "3 5 2 4 6 7 ]\n|  expr0 = QFilterSelect [ 0 1 3 5 ]\n|  expr1 "
                "= QFilterSelect [ 2 4 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CY(range<0,3>(), range<4,7>())
    auto f = CY(range<0, 3>(), range<4, 7>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCY\n| filter = QFilterSelect [ 0 1 "
                "2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ 0 1 2 3 ]\n|  expr1 "
                "= QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CY(qureg<0,4>(), qureg<4,4>())
    auto f = CY(qureg<0, 4>(), qureg<4, 4>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCY\n| filter = QFilterSelect [ 0 1 "
                "2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ 0 1 2 3 ]\n|  expr1 "
                "= QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CY(qubit<1>(), qubit<2>())
    auto f = CY(qubit<1>(), qubit<2>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCY\n| filter = QFilterSelect [ 1 2 ]\n|  "
      "expr0 = QFilterSelect [ 1 ]\n|  expr1 = QFilterSelect [ 2 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QCY()
    auto f = QCY();
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(), "QCY\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QCY()(all(), all())
    auto f = QCY()(all(), all());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCY\n| filter = QFilterSelectAll\n| "
                " expr0 = QFilterSelectAll\n|  expr1 = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QCY()(sel<0,1,3,5>(), sel<2,4,6,7>())
    auto f = QCY()(sel<0, 1, 3, 5>(), sel<2, 4, 6, 7>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCY\n| filter = QFilterSelect [ 0 1 "
                "3 5 2 4 6 7 ]\n|  expr0 = QFilterSelect [ 0 1 3 5 ]\n|  expr1 "
                "= QFilterSelect [ 2 4 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QCY()(range<0,3>(), range<4,7>())
    auto f = QCY()(range<0, 3>(), range<4, 7>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCY\n| filter = QFilterSelect [ 0 1 "
                "2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ 0 1 2 3 ]\n|  expr1 "
                "= QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QCY()(qureg<0,4>(), qureg<4,4>())
    auto f = QCY()(qureg<0, 4>(), qureg<4, 4>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCY\n| filter = QFilterSelect [ 0 1 "
                "2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ 0 1 2 3 ]\n|  expr1 "
                "= QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QCY()(qubit<1>(), qubit<2>())
    auto f = QCY()(qubit<1>(), qubit<2>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCY\n| filter = QFilterSelect [ 1 2 ]\n|  "
      "expr0 = QFilterSelect [ 1 ]\n|  expr1 = QFilterSelect [ 2 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }
}
