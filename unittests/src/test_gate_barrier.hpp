/** @file test_gate_barrier.hpp
 *
 *  @brief UnitTests++ LibKet::gate::barrier() test
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_barrier)
{
  try {
    // barrier()
    auto f = LibKet::gates::barrier();
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QBarrier\n| filter = QFilter\n|   expr "
                "= QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // barrier(all())
    auto f = LibKet::gates::barrier(all());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QBarrier\n| filter = "
                "QFilterSelectAll\n|   expr = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // barrier(sel<0,1,3,5>())
    auto f = LibKet::gates::barrier(sel<0, 1, 3, 5>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QBarrier\n| filter = QFilterSelect [ 0 "
                "1 3 5 ]\n|   expr = QFilterSelect [ 0 1 3 5 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // barrier(range<0,3>())
    auto f = LibKet::gates::barrier(range<0, 3>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QBarrier\n| filter = QFilterSelect [ 0 "
                "1 2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // barrier(qureg<0,4>())
    auto f = LibKet::gates::barrier(qureg<0, 4>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QBarrier\n| filter = QFilterSelect [ 0 "
                "1 2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // barrier(qubit<1>())
    auto f = LibKet::gates::barrier(qubit<1>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QBarrier\n| filter = QFilterSelect [ 1 "
                "]\n|   expr = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // BARRIER()
    auto f = LibKet::gates::BARRIER();
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QBarrier\n| filter = QFilter\n|   expr "
                "= QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // BARRIER(all())
    auto f = LibKet::gates::BARRIER(all());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QBarrier\n| filter = "
                "QFilterSelectAll\n|   expr = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // BARRIER(sel<0,1,3,5>())
    auto f = LibKet::gates::BARRIER(sel<0, 1, 3, 5>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QBarrier\n| filter = QFilterSelect [ 0 "
                "1 3 5 ]\n|   expr = QFilterSelect [ 0 1 3 5 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // BARRIER(range<0,3>())
    auto f = LibKet::gates::BARRIER(range<0, 3>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QBarrier\n| filter = QFilterSelect [ 0 "
                "1 2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // BARRIER(qureg<0,4>())
    auto f = LibKet::gates::BARRIER(qureg<0, 4>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QBarrier\n| filter = QFilterSelect [ 0 "
                "1 2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // BARRIER(qubit<1>())
    auto f = LibKet::gates::BARRIER(qubit<1>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QBarrier\n| filter = QFilterSelect [ 1 "
                "]\n|   expr = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QBarrier()
    auto f = QBarrier();
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(), "QBarrier\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QBarrier()(all())
    auto f = QBarrier()(all());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QBarrier\n| filter = "
                "QFilterSelectAll\n|   expr = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QBarrier()(sel<0,1,3,5>())
    auto f = QBarrier()(sel<0, 1, 3, 5>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QBarrier\n| filter = QFilterSelect [ 0 "
                "1 3 5 ]\n|   expr = QFilterSelect [ 0 1 3 5 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QBarrier()(range<0,3>())
    auto f = QBarrier()(range<0, 3>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QBarrier\n| filter = QFilterSelect [ 0 "
                "1 2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QBarrier()(qureg<0,4>())
    auto f = QBarrier()(qureg<0, 4>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QBarrier\n| filter = QFilterSelect [ 0 "
                "1 2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QBarrier()(qubit<1>())
    auto f = QBarrier()(qubit<1>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QBarrier\n| filter = QFilterSelect [ 1 "
                "]\n|   expr = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }
}
