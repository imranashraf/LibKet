/** @file test_gate_cphasedag.hpp
 *
 *  @brief UnitTests++ LibKet::gate::cphasedag() test
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_cphasedag)
{
  try {
    // cphasedag(QConst(3.141))
    auto f = cphasedag(QConst(3.141));
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCPhasedag 3.141\n| filter = QFilter\n|  "
      "expr0 = QFilter\n|  expr1 = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphasedag(QConst(3.141),all(),all())
    auto f = cphasedag(QConst(3.141), all(), all());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhasedag 3.141\n| filter = "
                "QFilterSelectAll\n|  "
                "expr0 = QFilterSelectAll\n|  expr1 = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphasedag(QConst(3.141), sel<0,1,3,5>(), sel<2,4,6,7>())
    auto f = cphasedag(QConst(3.141), sel<0, 1, 3, 5>(), sel<2, 4, 6, 7>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhasedag 3.141\n| filter = "
                "QFilterSelect [ 0 1 3 5 2 4 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 3 5 ]\n|  expr1 = QFilterSelect [ 2 4 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphasedag(QConst(3.141), range<0,3>(), range<4,7>())
    auto f = cphasedag(QConst(3.141), range<0, 3>(), range<4, 7>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhasedag 3.141\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphasedag(QConst(3.141), qureg<0,4>(), qureg<4,4>())
    auto f = cphasedag(QConst(3.141), qureg<0, 4>(), qureg<4, 4>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhasedag 3.141\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphasedag(QConst(3.141), qubit<1>(), qubit<2>())
    auto f = cphasedag(QConst(3.141), qubit<1>(), qubit<2>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCPhasedag 3.141\n| filter = QFilterSelect [ 1 "
      "2 "
      "]\n|  expr0 = QFilterSelect [ 1 ]\n|  expr1 = QFilterSelect [ 2 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CPHASEDAG(QConst(3.141))
    auto f = CPHASEDAG(QConst(3.141));
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCPhasedag 3.141\n| filter = QFilter\n|  "
      "expr0 = QFilter\n|  expr1 = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CPHASEDAG(QConst(3.141), all(), all())
    auto f = CPHASEDAG(QConst(3.141), all(), all());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhasedag 3.141\n| filter = "
                "QFilterSelectAll\n|  "
                "expr0 = QFilterSelectAll\n|  expr1 = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CPHASEDAG(QConst(3.141), sel<0,1,3,5>(), sel<2,4,6,7>())
    auto f = CPHASEDAG(QConst(3.141), sel<0, 1, 3, 5>(), sel<2, 4, 6, 7>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhasedag 3.141\n| filter = "
                "QFilterSelect [ 0 1 3 5 2 4 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 3 5 ]\n|  expr1 = QFilterSelect [ 2 4 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CPHASEDAG(QConst(3.141), range<0,3>(), range<4,7>())
    auto f = CPHASEDAG(QConst(3.141), range<0, 3>(), range<4, 7>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhasedag 3.141\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CPHASEDAG(QConst(3.141), qureg<0,4>(), qureg<4,4>())
    auto f = CPHASEDAG(QConst(3.141), qureg<0, 4>(), qureg<4, 4>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhasedag 3.141\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CPHASEDAG(QConst(3.141), qubit<1>(), qubit<2>())
    auto f = CPHASEDAG(QConst(3.141), qubit<1>(), qubit<2>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCPhasedag 3.141\n| filter = QFilterSelect [ 1 "
      "2 "
      "]\n|  expr0 = QFilterSelect [ 1 ]\n|  expr1 = QFilterSelect [ 2 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // crdag(QConst(3.141))
    auto f = crdag(QConst(3.141));
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCPhasedag 3.141\n| filter = QFilter\n|  "
      "expr0 = QFilter\n|  expr1 = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // crdag(QConst(3.141), all(), all())
    auto f = crdag(QConst(3.141), all(), all());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhasedag 3.141\n| filter = "
                "QFilterSelectAll\n|  "
                "expr0 = QFilterSelectAll\n|  expr1 = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // crdag(QConst(3.141), sel<0,1,3,5>(), sel<2,4,6,7>())
    auto f = crdag(QConst(3.141), sel<0, 1, 3, 5>(), sel<2, 4, 6, 7>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhasedag 3.141\n| filter = "
                "QFilterSelect [ 0 1 3 5 2 4 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 3 5 ]\n|  expr1 = QFilterSelect [ 2 4 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // crdag(QConst(3.141), range<0,3>(), range<4,7>())
    auto f = crdag(QConst(3.141), range<0, 3>(), range<4, 7>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhasedag 3.141\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // crdag(QConst(3.141), qureg<0,4>(), qureg<4,4>())
    auto f = crdag(QConst(3.141), qureg<0, 4>(), qureg<4, 4>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhasedag 3.141\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // crdag(QConst(3.141), qubit<1>(), qubit<2>())
    auto f = crdag(QConst(3.141), qubit<1>(), qubit<2>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCPhasedag 3.141\n| filter = QFilterSelect [ 1 "
      "2 "
      "]\n|  expr0 = QFilterSelect [ 1 ]\n|  expr1 = QFilterSelect [ 2 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CRDAG(QConst(3.141))
    auto f = CRDAG(QConst(3.141));
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCPhasedag 3.141\n| filter = QFilter\n|  "
      "expr0 = QFilter\n|  expr1 = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CRDAG(QConst(3.141), all(), all())
    auto f = CRDAG(QConst(3.141), all(), all());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhasedag 3.141\n| filter = "
                "QFilterSelectAll\n|  "
                "expr0 = QFilterSelectAll\n|  expr1 = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CRDAG(QConst(3.141), sel<0,1,3,5>(), sel<2,4,6,7>())
    auto f = CRDAG(QConst(3.141), sel<0, 1, 3, 5>(), sel<2, 4, 6, 7>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhasedag 3.141\n| filter = "
                "QFilterSelect [ 0 1 3 5 2 4 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 3 5 ]\n|  expr1 = QFilterSelect [ 2 4 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CRDAG(QConst(3.141), range<0,3>(), range<4,7>())
    auto f = CRDAG(QConst(3.141), range<0, 3>(), range<4, 7>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhasedag 3.141\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CRDAG(QConst(3.141), qureg<0,4>(), qureg<4,4>())
    auto f = CRDAG(QConst(3.141), qureg<0, 4>(), qureg<4, 4>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhasedag 3.141\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CRDAG(QConst(3.141), qubit<1>(), qubit<2>())
    auto f = CRDAG(QConst(3.141), qubit<1>(), qubit<2>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCPhasedag 3.141\n| filter = QFilterSelect [ 1 "
      "2 "
      "]\n|  expr0 = QFilterSelect [ 1 ]\n|  expr1 = QFilterSelect [ 2 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QCPhasedag<QConst_t(3.141)>()
    auto f = QCPhasedag<QConst_t(3.141)>();
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(), "QCPhasedag 3.141\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QCPhasedag<QConst_t(3.141)>()(all(), all())
    auto f = QCPhasedag<QConst_t(3.141)>()(all(), all());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhasedag 3.141\n| filter = "
                "QFilterSelectAll\n|  "
                "expr0 = QFilterSelectAll\n|  expr1 = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QCPhasedag<QConst_t(3.141)>()(sel<0,1,3,5>(), sel<2,4,6,7>())
    auto f =
      QCPhasedag<QConst_t(3.141)>()(sel<0, 1, 3, 5>(), sel<2, 4, 6, 7>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhasedag 3.141\n| filter = "
                "QFilterSelect [ 0 1 3 5 2 4 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 3 5 ]\n|  expr1 = QFilterSelect [ 2 4 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QCPhasedag<QConst_t(3.141)>()(range<0,3>(), range<4,7>())
    auto f = QCPhasedag<QConst_t(3.141)>()(range<0, 3>(), range<4, 7>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhasedag 3.141\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QCPhasedag<QConst_t(3.141)>()(qureg<0,4>(), qureg<4,4>())
    auto f = QCPhasedag<QConst_t(3.141)>()(qureg<0, 4>(), qureg<4, 4>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhasedag 3.141\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QCPhasedag<QConst_t(3.141)>()(qubit<1>(), qubit<2>())
    auto f = QCPhasedag<QConst_t(3.141)>()(qubit<1>(), qubit<2>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCPhasedag 3.141\n| filter = QFilterSelect [ 1 "
      "2 "
      "]\n|  expr0 = QFilterSelect [ 1 ]\n|  expr1 = QFilterSelect [ 2 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }
}
