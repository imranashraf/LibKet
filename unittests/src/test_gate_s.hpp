/** @file test_gate_s.hpp
 *
 *  @brief UnitTests++ LibKet::gate::s() test
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_s)
{
  try {
    // s()
    auto f = s();
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QS\n| filter = QFilter\n|   expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // s(all())
    auto f = s(all());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QS\n| filter = QFilterSelectAll\n|   "
                "expr = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // s(sel<0,1,3,5>())
    auto f = s(sel<0, 1, 3, 5>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QS\n| filter = QFilterSelect [ 0 1 3 5 "
                "]\n|   expr = QFilterSelect [ 0 1 3 5 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // s(range<0,3>())
    auto f = s(range<0, 3>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QS\n| filter = QFilterSelect [ 0 1 2 3 "
                "]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // s(qureg<0,4>())
    auto f = s(qureg<0, 4>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QS\n| filter = QFilterSelect [ 0 1 2 3 "
                "]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // s(qubit<1>())
    auto f = s(qubit<1>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QS\n| filter = QFilterSelect [ 1 ]\n|  "
                " expr = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // S()
    auto f = S();
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QS\n| filter = QFilter\n|   expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // S(all())
    auto f = S(all());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QS\n| filter = QFilterSelectAll\n|   "
                "expr = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // S(sel<0,1,3,5>())
    auto f = S(sel<0, 1, 3, 5>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QS\n| filter = QFilterSelect [ 0 1 3 5 "
                "]\n|   expr = QFilterSelect [ 0 1 3 5 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // S(range<0,3>())
    auto f = S(range<0, 3>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QS\n| filter = QFilterSelect [ 0 1 2 3 "
                "]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // S(qureg<0,4>())
    auto f = S(qureg<0, 4>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QS\n| filter = QFilterSelect [ 0 1 2 3 "
                "]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // S(qubit<1>())
    auto f = S(qubit<1>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QS\n| filter = QFilterSelect [ 1 ]\n|  "
                " expr = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QS()
    auto f = QS();
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(), "QS\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QS()(all())
    auto f = QS()(all());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QS\n| filter = QFilterSelectAll\n|   "
                "expr = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QS()(sel<0,1,3,5>())
    auto f = QS()(sel<0, 1, 3, 5>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QS\n| filter = QFilterSelect [ 0 1 3 5 "
                "]\n|   expr = QFilterSelect [ 0 1 3 5 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QS()(range<0,3>())
    auto f = QS()(range<0, 3>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QS\n| filter = QFilterSelect [ 0 1 2 3 "
                "]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QS()(qureg<0,4>())
    auto f = QS()(qureg<0, 4>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QS\n| filter = QFilterSelect [ 0 1 2 3 "
                "]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QS()(qubit<1>())
    auto f = QS()(qubit<1>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QS\n| filter = QFilterSelect [ 1 ]\n|  "
                " expr = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }
}
