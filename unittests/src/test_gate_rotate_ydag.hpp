/** @file test_gate_rotate_ydag.hpp
 *
 *  @brief UnitTests++ LibKet::gate::rotate_ydag() test
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_rotate_ydag)
{
  try {
    // rotate_ydag(QConst(3.141))
    auto f = rotate_ydag(QConst(3.141));
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_Ydag 3.141\n| filter = QFilter\n| "
      "  expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rotate_ydag(QConst(3.141),all())
    auto f = rotate_ydag(QConst(3.141), all());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_Ydag 3.141\n| filter = "
                "QFilterSelectAll\n|   expr = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rotate_ydag(QConst(3.141),sel<0,1,3,5>())
    auto f = rotate_ydag(QConst(3.141), sel<0, 1, 3, 5>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_Ydag 3.141\n| filter = "
                "QFilterSelect [ 0 1 "
                "3 5 ]\n|   expr = QFilterSelect [ 0 1 3 5 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rotate_ydag(QConst(3.141),range<0,3>())
    auto f = rotate_ydag(QConst(3.141), range<0, 3>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_Ydag 3.141\n| filter = "
                "QFilterSelect [ 0 1 "
                "2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rotate_ydag(QConst(3.141),qureg<0,4>())
    auto f = rotate_ydag(QConst(3.141), qureg<0, 4>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_Ydag 3.141\n| filter = "
                "QFilterSelect [ 0 1 "
                "2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rotate_ydag(QConst(3.141),qubit<1>())
    auto f = rotate_ydag(QConst(3.141), qubit<1>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_Ydag 3.141\n| filter = "
                "QFilterSelect [ 1 ]\n|   expr = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // ROTATE_Ydag(QConst(3.141))
    auto f = ROTATE_Ydag(QConst(3.141));
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_Ydag 3.141\n| filter = QFilter\n| "
      "  expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // ROTATE_Ydag(QConst(3.141),all())
    auto f = ROTATE_Ydag(QConst(3.141), all());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_Ydag 3.141\n| filter = "
                "QFilterSelectAll\n|   expr = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // ROTATE_Ydag(QConst(3.141),sel<0,1,3,5>())
    auto f = ROTATE_Ydag(QConst(3.141), sel<0, 1, 3, 5>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_Ydag 3.141\n| filter = "
                "QFilterSelect [ 0 1 "
                "3 5 ]\n|   expr = QFilterSelect [ 0 1 3 5 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // ROTATE_Ydag(QConst(3.141),range<0,3>())
    auto f = ROTATE_Ydag(QConst(3.141), range<0, 3>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_Ydag 3.141\n| filter = "
                "QFilterSelect [ 0 1 "
                "2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // ROTATE_Ydag(QConst(3.141),qureg<0,4>())
    auto f = ROTATE_Ydag(QConst(3.141), qureg<0, 4>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_Ydag 3.141\n| filter = "
                "QFilterSelect [ 0 1 "
                "2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // ROTATE_Ydag(QConst(3.141),qubit<1>())
    auto f = ROTATE_Ydag(QConst(3.141), qubit<1>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_Ydag 3.141\n| filter = "
                "QFilterSelect [ 1 ]\n|   expr = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rydag(QConst(3.141))
    auto f = rydag(QConst(3.141));
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_Ydag 3.141\n| filter = QFilter\n| "
      "  expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rydag(QConst(3.141),all())
    auto f = rydag(QConst(3.141), all());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_Ydag 3.141\n| filter = "
                "QFilterSelectAll\n|   expr = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rydag(QConst(3.141),sel<0,1,3,5>())
    auto f = rydag(QConst(3.141), sel<0, 1, 3, 5>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_Ydag 3.141\n| filter = "
                "QFilterSelect [ 0 1 "
                "3 5 ]\n|   expr = QFilterSelect [ 0 1 3 5 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rydag(QConst(3.141),range<0,3>())
    auto f = rydag(QConst(3.141), range<0, 3>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_Ydag 3.141\n| filter = "
                "QFilterSelect [ 0 1 "
                "2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rydag(QConst(3.141),qureg<0,4>())
    auto f = rydag(QConst(3.141), qureg<0, 4>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_Ydag 3.141\n| filter = "
                "QFilterSelect [ 0 1 "
                "2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rydag(QConst(3.141),qubit<1>())
    auto f = rydag(QConst(3.141), qubit<1>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_Ydag 3.141\n| filter = "
                "QFilterSelect [ 1 ]\n|   expr = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // RYdag(QConst(3.141))
    auto f = RYdag(QConst(3.141));
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_Ydag 3.141\n| filter = QFilter\n| "
      "  expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // RYdag(QConst(3.141),all())
    auto f = RYdag(QConst(3.141), all());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_Ydag 3.141\n| filter = "
                "QFilterSelectAll\n|   expr = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // RYdag(QConst(3.141),sel<0,1,3,5>())
    auto f = RYdag(QConst(3.141), sel<0, 1, 3, 5>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_Ydag 3.141\n| filter = "
                "QFilterSelect [ 0 1 "
                "3 5 ]\n|   expr = QFilterSelect [ 0 1 3 5 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // RYdag(QConst(3.141),range<0,3>())
    auto f = RYdag(QConst(3.141), range<0, 3>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_Ydag 3.141\n| filter = "
                "QFilterSelect [ 0 1 "
                "2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // RYdag(QConst(3.141),qureg<0,4>())
    auto f = RYdag(QConst(3.141), qureg<0, 4>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_Ydag 3.141\n| filter = "
                "QFilterSelect [ 0 1 "
                "2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // RYdag(QConst(3.141),qubit<1>())
    auto f = RYdag(QConst(3.141), qubit<1>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_Ydag 3.141\n| filter = "
                "QFilterSelect [ 1 ]\n|   expr = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // Rydag(QConst(3.141))
    auto f = Rydag(QConst(3.141));
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_Ydag 3.141\n| filter = QFilter\n| "
      "  expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // Rydag(QConst(3.141),all())
    auto f = Rydag(QConst(3.141), all());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_Ydag 3.141\n| filter = "
                "QFilterSelectAll\n|   expr = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // Rydag(QConst(3.141),sel<0,1,3,5>())
    auto f = Rydag(QConst(3.141), sel<0, 1, 3, 5>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_Ydag 3.141\n| filter = "
                "QFilterSelect [ 0 1 "
                "3 5 ]\n|   expr = QFilterSelect [ 0 1 3 5 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // Rydag(QConst(3.141),range<0,3>())
    auto f = Rydag(QConst(3.141), range<0, 3>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_Ydag 3.141\n| filter = "
                "QFilterSelect [ 0 1 "
                "2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // Rydag(QConst(3.141),qureg<0,4>())
    auto f = Rydag(QConst(3.141), qureg<0, 4>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_Ydag 3.141\n| filter = "
                "QFilterSelect [ 0 1 "
                "2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // Rydag(QConst(3.141),qubit<1>())
    auto f = Rydag(QConst(3.141), qubit<1>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_Ydag 3.141\n| filter = "
                "QFilterSelect [ 1 ]\n|   expr = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QRotate_Ydag<QConst(3.141)>()
    auto f = QRotate_Ydag<QConst_t(3.141)>();
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(), "QRotate_Ydag 3.141\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QRotate_Ydag<QConst(3.141)>()(all())
    auto f = QRotate_Ydag<QConst_t(3.141)>()(all());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_Ydag 3.141\n| filter = "
                "QFilterSelectAll\n|   expr = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QRotate_Ydag<QConst(3.141)>()(sel<0,1,3,5>())
    auto f = QRotate_Ydag<QConst_t(3.141)>()(sel<0, 1, 3, 5>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_Ydag 3.141\n| filter = "
                "QFilterSelect [ 0 1 "
                "3 5 ]\n|   expr = QFilterSelect [ 0 1 3 5 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QRotate_Ydag<QConst(3.141)>()(range<0,3>())
    auto f = QRotate_Ydag<QConst_t(3.141)>()(range<0, 3>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_Ydag 3.141\n| filter = "
                "QFilterSelect [ 0 1 "
                "2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QRotate_Ydag<QConst(3.141)>()(qureg<0,4>())
    auto f = QRotate_Ydag<QConst_t(3.141)>()(qureg<0, 4>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_Ydag 3.141\n| filter = "
                "QFilterSelect [ 0 1 "
                "2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QRotate_Ydag<QConst(3.141)>()(qubit<1>())
    auto f = QRotate_Ydag<QConst_t(3.141)>()(qubit<1>());
    std::stringstream ss;

    show<99>(f, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_Ydag 3.141\n| filter = "
                "QFilterSelect [ 1 ]\n|   expr = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }
}
