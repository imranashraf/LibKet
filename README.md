# LibKet - The Quantum Expression Template Library

[![License: MPL 2.0](https://img.shields.io/badge/License-MPL%202.0-brightgreen.svg)](https://opensource.org/licenses/MPL-2.0)
[![Build status](https://gitlab.com/mmoelle1/LibKet/badges/master/pipeline.svg)](https://gitlab.com/mmoelle1/LibKet/commits/master)
[![Coverage report](https://gitlab.com/mmoelle1/LibKet/badges/master/coverage.svg)](https://gitlab.com/mmoelle1/LibKet/commits/master)

[<img align="left" src="doc/LibKet.png" title="LibKet logo" height="75px">](doc/LibKet.png)

**LibKet** is an [expression
template](https://en.wikipedia.org/wiki/Expression_templates) library
written in C++14 that provides building blocks for the rapid
prototyping of quantum algorithms and their testing on different
quantum simulator platforms. These building blocks are quantum bits,
so-called [qbits](https://en.wikipedia.org/wiki/Qubit), [quantum
registers](https://en.wikipedia.org/wiki/Quantum_register), [quantum
logic gates](https://en.wikipedia.org/wiki/Quantum_logic_gate), and
some basic [quantum
algorithms](https://en.wikipedia.org/wiki/https://en.wikipedia.org/wiki/Quantum_algorithm)
like the [Quantum Fourier
Transform](https://en.wikipedia.org/wiki/Quantum_Fourier_transform)
and its inverse operation widely used in advanced quantum algorithms.

**LibKet** makes it possible to formulate quantum algorithms in a more
abstract way than it is typically done in most quantum simulators,
which require you to express quantum algorithms using unary and binary
quantum gates only and are restricted to a particular quantum assembly
language or quantum instruction language. In contrast, **LibKet**
allows you to implement quantum algorithms as generic quantum
expressions that can be synthesized to different quantum simulator
frameworks and quantum assembly languages.

## Compile dependencies
In order to compile LibKet, the [Universal Number Arithmetic](https://github.com/stillwater-sc/universal) package must be downloaded.

One can do so by running a git submodule command before building LibKet via cmake. See command below:

`git submodule update --init --recursive`

## Tutorial: Quantum Hello World

The following code snipped illustrates the use of **LibKet** for
creating an 8-qbit quantum register and applying Hadamard gates to
each of them. This is, for instance, the first step of the well-known
[Shor algorithm](https://en.wikipedia.org/wiki/Shor%27s_algorithm).

1. The first step is to *include the header-only* **LibKet**
   *library* and to make the **LibKet** namespace known
   ```cpp
      #include <LibKet.hpp>
      using namespace std;
      using namespace LibKet;
   ```

2. Next, *create a quantum expression* that selects the first 8 qbits
   (`q[0..7]`) and applies the Hadamard gate to each of them. This
   can be done in two different ways:
    * Compose the quantum expression from `QObject`s:
      ```cpp
         auto qalgo = QHadamard()(QRegister<0,8>());
      ```
      Technically, the above code snipped creates a `QRegister<0,8>`
      object and a `QHadamard` object and combines the two using the
      `operator()` of the `QHadamard` class. This approach works for
      all `QObject`s since all of them provide an overloaded
      `operator()`.
    * Compose the quantum expression using wrapper functions:
      ```cpp
         auto qalgo = gate::hadamard(filter::qreg<0,8>());
      ```
      An alternative way of creating quantum expressions is to use
      the wapper functions `filter::qreg<0,8>()` and
      `gate::hadamard()`. Wrapper functions exist for all `QObject`s
      grouped into different namespaces.
   
3. The so-created `qalgo` object is a lightweight quantum expression
   that can be synthesized for different backends. To begin with,
   *create a `QData` object* consisting of 8 quantum bits and
   specialized for the common Quantum Assembly Language (cQASM v1.0)
   backend.
   ```cpp
      QData<8, QBackend::cQASMv1> qdata;
   ```

4. The last step is to *apply the quantum expression* to the `QData`
   object to produce, in this case, the cQASM compute kernel as a string:
   ```cpp
      cout << qalgo(qdata) << endl;
   ```


## Copyright

Copyright (c) 2018-2019 Matthias Möller (m.moller@tudelft.nl).

In Dutch, 'Quantum' is spelled 'Kwantum', which explains the spelling
**LibKet**. The name is an allusion to the famous [bra-ket
notation](https://en.wikipedia.org/wiki/Bra%E2%80%93ket_notation) that
is widely used for expressing quantum algorithms.