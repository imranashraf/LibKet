/** @file examples/tutorial10.cpp

    @brief LibKet tutorial10: Quantum expressions and quantum filters

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Merel Schalkers
*/

#include <iostream>

#include "LibKet.hpp"

// Import LibKet namespaces
using namespace LibKet;
using namespace LibKet::circuits;
using namespace LibKet::filters;
using namespace LibKet::gates;

int main(int argc, char *argv[])
{
  auto expr = measure(
                      sqrt_not(
                               sel<0>(
                                      init()
                                      )
                               )
                      );

  // Create empty JSON object to receive results
  utils::json result;

#ifdef LIBKET_WITH_AQASM
  std::cout << "\n\n\n===== AQASM =====\n\n\n";
  QData<2, QBackend::AQASM> data_AQASM;
  std::cout << expr(data_AQASM) << std::endl;

  try {
    result = data_AQASM.execute();
    std::cout << result << std::endl;
  } catch(...) {}
#endif

#ifdef LIBKET_WITH_CQASM
  std::cout << "\n\n\n===== cQASM =====\n\n\n";
  QData<2, QBackend::cQASMv1> data_cQASMv1;
  std::cout << expr(data_cQASMv1) << std::endl;
  
  try {
    result = data_cQASMv1.execute();
    std::cout << result["histogram"] << std::endl;
  } catch(...) {}
#endif

#ifdef LIBKET_WITH_OPENQASM
  std::cout << "\n\n\n===== OpenQASM =====\n\n\n";
  QData<2, QBackend::OpenQASMv2> data_OpenQASMv2;
  std::cout << expr(data_OpenQASMv2) << std::endl;

  try {
    result = data_OpenQASMv2.execute();
    std::cout << result["results"] << std::endl;
  } catch(...) {}
#endif

#ifdef LIBKET_WITH_OPENQL
  std::cout << "\n\n\n===== OpenQL =====\n\n\n";
  QData<2, QBackend::OpenQL> data_OpenQL;
  std::cout << expr(data_OpenQL) << std::endl;

  try {
    data_OpenQL.compile("program");
  } catch(...) {}
#endif

#ifdef LIBKET_WITH_QASM
  std::cout << "\n\n\n===== QASM =====\n\n\n";
  QData<2, QBackend::QASM> data_QASM;
  std::cout << expr(data_QASM) << std::endl;
  data_QASM.compile("tutorial10");
#endif
  
#ifdef LIBKET_WITH_QUIL
  std::cout << "\n\n\n===== Quil =====\n\n\n";
  QData<6, QBackend::Quil> data_Quil;
  std::cout << expr(data_Quil) << std::endl;

  try {
    result = data_Quil.execute();
    std::cout << result << std::endl;
  } catch(...) {}
#endif

#ifdef LIBKET_WITH_QX
  std::cout << "\n\n\n===== QX =====\n\n\n";
  QData<2, QBackend::QX> data_QX;
  std::cout << expr(data_QX) << std::endl;

  try {
    data_QX.execute().dump();
    data_QX.reset();
    
    data_QX.execute(0.5).dump();
    data_QX.reset();
  } catch(...) {}
#endif

  QDebug << "Total number of jobs run: "
         << _qstream_python.size() << " Python jobs, "
         << _qstream_cxx.size() << " C++ jobs\n";
  
  return 0;
}
