########################################################################
# OpenQL.cmake
#
# Author: Matthias Moller
# Copyright (C) 2018 by the LibKet authors
#
# This file is part of the LibKet library
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# 
########################################################################

########################################################################
# OpenQL
########################################################################

if (LIBKET_BUILTIN_OPENQL)
  
  include(DownloadProject)
  download_project(
    PROJ              OpenQL
    GIT_REPOSITORY    https://github.com/QE-Lab/OpenQL.git
    GIT_TAG           develop
    TIMEOUT           180
    PREFIX            ${CMAKE_BINARY_DIR}/external/OpenQL
    ${UPDATE_DISCONNECTED_IF_AVAILABLE}
    )
    
  # Add include directory
  include_directories("${OpenQL_SOURCE_DIR}" "${OpenQL_SOURCE_DIR}/deps/CLI11/include" "${OpenQL_SOURCE_DIR}/deps/lemon")
  
else()

  # Add include directory
  if(OpenQL_INCLUDE_PATH)
    include_directories(${OpenQL_INCLUDE_PATH})
  else()
    message(WARNING "Variable OpenQL_INCLUDE_PATH is not defined. LibKet might be unable to find OpenQL include files.")
  endif()
    
endif()

# Process OpenQL project
if (NOT TARGET openql)
  add_subdirectory(${OpenQL_SOURCE_DIR} ${OpenQL_BINARY_DIR})
  add_custom_target(openql)
endif()
  
# Add include directory
include_directories("${OpenQL_SOURCE_DIR}/include")
include_directories("${OpenQL_BINARY_DIR}/deps/lemon")
include_directories("${OpenQL_SOURCE_DIR}/deps/lemon")
include_directories("${OpenQL_BINARY_DIR}/deps/CLI11/include")
include_directories("${OpenQL_SOURCE_DIR}/deps/CLI11/include")

# Make output directory
file(MAKE_DIRECTORY ${PROJECT_BINARY_DIR}/test_output)

# Copy JSON configuration files 
file(GLOB_RECURSE OpenQL_JSONS "${OpenQL_SOURCE_DIR}/test_config_default.json")
file(COPY ${OpenQL_JSONS} DESTINATION ${PROJECT_BINARY_DIR})

