########################################################################
# QX.cmake
#
# Author: Matthias Moller
# Copyright (C) 2018 by the LibKet authors
#
# This file is part of the LibKet library
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# 
########################################################################

########################################################################
# QX
########################################################################

if (LIBKET_BUILTIN_QX)
  
  include(DownloadProject)
  download_project(
    PROJ              QX
    GIT_REPOSITORY    https://github.com/QE-Lab/qx-simulator.git
    GIT_TAG           develop
    TIMEOUT           180
    PREFIX            ${CMAKE_BINARY_DIR}/external/QX
    ${UPDATE_DISCONNECTED_IF_AVAILABLE}
    )
    
  # Add include directory
  include_directories("${QX_SOURCE_DIR}/src" "${QX_SOURCE_DIR}/src/xpu-0.1.5")
  
else()

  # Add include directory
  if(QX_INCLUDE_PATH)
    include_directories(${QX_INCLUDE_PATH})
  else()
    message(WARNING "Variable QX_INCLUDE_PATH is not defined. LibKet might be unable to find QX include files.")
  endif()
    
endif()

# Enable QX simulator support
add_definitions(-DCG_BC -DQX_SPARSE_MV_MUL -DXPU_TIMER -D__BUILTIN_LINALG__)
