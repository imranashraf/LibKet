/** @file libket/intrinsics/QIntrinsics.hpp

    @brief LibKet intrinsics header file

    @copyright This file is part of the LibKet project

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */
#pragma once
#ifndef INTRINSICS_HPP
#define INTRINSICS_HPP

#include "QIntrinsic.hpp"
#include "QIntrinsic_Int.hpp"
#include "QIntrinsic_Posit.hpp"

#endif // INTRINSICS_HPP
