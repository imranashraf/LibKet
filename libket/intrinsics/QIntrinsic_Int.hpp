/** @file libket/intrinsics/QIntrinsic_Int.hpp

    @brief LibKet quantum intrinsic integer data type class

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QINTRINSIC_INT_HPP
#define QINTRINSIC_INT_HPP

#include "QIntrinsic.hpp"

namespace LibKet {

namespace intrinsics {

/**
@brief LibKet quantum intrinsic integer data type class

The LibKet quantum intrinsic integer data type class implements integer data
types
*/
template<typename bits>
class QInt : public QIntrinsic
{};

} // namespace intrinsics

} // namespace LibKet

#endif // QINTRINSIC_INT_HPP
