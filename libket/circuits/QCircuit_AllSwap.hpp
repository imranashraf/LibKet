/** @file libket/circuits/QCircuit_AllSwap.hpp

    @brief LibKet quantum all swap circuit class

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QCIRCUIT_ALLSWAP_HPP
#define QCIRCUIT_ALLSWAP_HPP

#include <QFilter.hpp>
#include <circuits/QCircuit.hpp>
#include <gates/QGates.hpp>

namespace LibKet {

using namespace filters;
using namespace gates;

namespace circuits {

/**
@brief LibKet quantum all swap circuit class

The LibKet quantum all swap circuit class implements an algorithm that
swaps all qubits in a given selection as it is done, for instance, as
last part of the quantum Fourier transform
*/
class QCircuit_AllSwap : public QCircuit
{
private:
  // Realizes the all swap loop
  template<size_t start, size_t end>
  struct allswap_loop
  {
    template<size_t index, typename Expr0, typename Expr1>
    inline static constexpr auto func(Expr0&& expr0, Expr1&& expr1) noexcept
    {
      return swap(sel<start + index>(gototag<0>(expr0)),
                  sel<end - index>(gototag<0>(expr1)));
    }
  };

public:
  /// Operator() - by constant reference
  template<typename T>
  inline constexpr auto operator()(const T& t) const noexcept;

  /// Operator() - by universal reference
  template<typename T>
  inline constexpr auto operator()(T&& t) const noexcept;

  /// Apply function for multiple qubits - used for all backends
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static QData<_qubits, _qbackend>& apply(
    typename std::enable_if<_filter::template size<_qubits> >= 2,
                            QData<_qubits, _qbackend>>::type& data) noexcept
  {
    auto expr = gototag<0>(
      utils::static_for<0,
                        ((_filter::template size<_qubits>) >> 1) - 1,
                        1,
                        allswap_loop<0, _filter::template size<_qubits> - 1>>(
        tag<0>(_filter{}), tag<0>(_filter{})));

    return expr(data);
  }

  /// Apply function for less than two qubits - used for all backends
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static QData<_qubits, _qbackend>& apply(
    typename std::enable_if<_filter::template size<_qubits> <= 1,
                            QData<_qubits, _qbackend>>::type& data) noexcept
  {
    return data;
  }
};

/**
@brief LibKet AllSwap circuit creator

This overload of the LibKet::circuits::allswap() function can be
used as terminal, i.e. the inner-most gate in a quantum
expression

\code
auto qcirc = circuits::allswap();
\endcode
*/
inline constexpr auto
allswap() noexcept
{
  return UnaryQGate<filters::QFilter, QCircuit_AllSwap>(filters::QFilter{});
}

/**
@brief LibKet AllSwap circuit creator

This overload of the LibKet::circuits::allswap() function can be
used as terminal, i.e. the inner-most gate in a quantum
expression

\code
auto qcirc = circuits::allswap(expr);
\endcode
*/
template<typename _expr>
inline constexpr auto
allswap(const _expr& expr) noexcept
{
  return UnaryQGate<_expr,
                    QCircuit_AllSwap,
                    typename filters::getFilter<_expr>::type>(expr);
}

/**
@brief LibKet AllSwap circuit creator

This overload of the LibKet::circuits::allswap() function accepts
an expression as universal reference
*/
template<typename _expr>
inline constexpr auto
allswap(_expr&& expr) noexcept
{
  return UnaryQGate<_expr,
                    QCircuit_AllSwap,
                    typename filters::getFilter<_expr>::type>(expr);
}

/**
@brief LibKet AllSwap circuit creator

Function alias for LibKet::circuits::allswap
*/
template<typename... Args>
inline constexpr auto
ALLSWAP(Args&&... args)
{
  return allswap(std::forward<Args>(args)...);
}

/// Operator() - by constant reference
template<typename T>
inline constexpr auto
QCircuit_AllSwap::operator()(const T& t) const noexcept
{
  return allswap(std::forward<T>(t));
}

/// Operator() - by universal reference
template<typename T>
inline constexpr auto
QCircuit_AllSwap::operator()(T&& t) const noexcept
{
  return allswap(std::forward<T>(t));
}

/**
   @brief LibKet show gate type - specialization for AllSwap objects
*/
template<std::size_t level = 1>
inline static auto
show(const QCircuit_AllSwap& circuit,
     std::ostream& os,
     const std::string& prefix = "")
{
  os << "AllSwap\n";

  return circuit;
}

} // namespace circuits

} // namespace LibKet

#endif // QCIRCUIT_ALLSWAP_HPP
