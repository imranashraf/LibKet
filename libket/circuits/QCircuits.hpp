/** @file libket/circuits/QCircuits.hpp

    @brief LibKet circuits header file

    @copyright This file is part of the LibKet project

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */
#pragma once
#ifndef CIRCUITS_HPP
#define CIRCUITS_HPP

#include "QCircuit.hpp"
#include "QCircuit_AllSwap.hpp"
#include "QCircuit_Arb_Ctrl.hpp"
#include "QCircuit_QFT.hpp"
#include "QCircuit_QFTdag.hpp"
#include "QCircuit_Sqrt_Not.hpp"
#include "QCircuit_Sqrt_Swap.hpp"

#endif // CIRCUITS_HPP
