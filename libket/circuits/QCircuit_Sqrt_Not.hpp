/** @file libket/circuits/QCircuit_Sqrt_Not.hpp

    @brief LibKet quantum square root of not class

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QCIRCUIT_SQRT_NOT_HPP
#define QCIRCUIT_SQRT_NOT_HPP

#include <QFilter.hpp>
#include <QUtils.hpp>
#include <circuits/QCircuit.hpp>
#include <gates/QGates.hpp>

namespace LibKet {

using namespace filters;
using namespace gates;

namespace circuits {

/**
@brief LibKet square root of not circuit class

The LibKet quantum square root of not circuit class implements
the square root of not gate for an arbitrary number of qubits
*/
template<typename _tol = QConst_t(0.0)>
class QGate_Sqrt_Not : public QCircuit
{
public:
  /// Operator() - by constant reference
  template<typename T>
  inline constexpr auto operator()(const T& t) const noexcept;

  /// Operator() - by universal reference
  template<typename T>
  inline constexpr auto operator()(T&& t) const noexcept;

  /// Apply function - used for all backends
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static QData<_qubits, _qbackend>& apply(
    QData<_qubits, _qbackend>& data) noexcept
  {
    auto expr = Rz(QConst(-1.5708),
                   Ry(QConst(1.5708), Rz(QConst(1.5708), tag<0>(_filter{}))));
    return expr(data);
  }
};

/**
@brief LibKet square root of not circuit creator

This overload of the LibKet::circuits::sqrt_not() function can be
used as terminal, i.e. the inner-most gate in a quantum
expression

\code
auto qcirc = circuits::sqrt_not();
\endcode
*/
template<typename _tol = QConst_t(0.0)>
inline constexpr auto
sqrt_not() noexcept
{
  return UnaryQGate<filters::QFilter, QGate_Sqrt_Not<_tol>>(filters::QFilter{});
}

/**
@brief LibKet square root of not circuit creator

This overload of the LibKet::circuits::sqrt_not() function can be
used as terminal, i.e. the inner-most gate in a quantum
expression

\code
auto qcirc = circuits::sqrt_not(expr);
\endcode
*/
template<typename _tol = QConst_t(0.0), typename _expr>
inline constexpr auto
sqrt_not(const _expr& expr) noexcept
{
  return UnaryQGate<_expr,
                    QGate_Sqrt_Not<_tol>,
                    typename filters::getFilter<_expr>::type>(expr);
}

/**
@brief LibKet square root of not circuit creator

This overload of the LibKet::circuits::sqrt_not() function accepts
an expression as universal reference
*/
template<typename _tol = QConst_t(0.0), typename _expr>
inline constexpr auto
sqrt_not(_expr&& expr) noexcept
{
  return UnaryQGate<_expr,
                    QGate_Sqrt_Not<_tol>,
                    typename filters::getFilter<_expr>::type>(expr);
}

/**
@brief LibKet square root of not circuit creator

Function alias for LibKet::circuits::sqrt_not()
*/
template<typename _tol = QConst_t(0.0), typename... Args>
inline constexpr auto
sqrt_not(Args&&... args)
{
  return sqrt_not<_tol>(std::forward<Args>(args)...);
}

/// Operator() - by constant reference
template<typename _tol>
template<typename T>
inline constexpr auto
QGate_Sqrt_Not<_tol>::operator()(const T& t) const noexcept
{
  return sqrt_not<_tol>(std::forward<T>(t));
}

/// Operator() - by universal reference
template<typename _tol>
template<typename T>
inline constexpr auto
QGate_Sqrt_Not<_tol>::operator()(T&& t) const noexcept
{
  return sqrt_not<_tol>(std::forward<T>(t));
}

/**
   @brief LibKet show gate type - specialization for square root of not objects
*/
template<std::size_t level = 1, typename _tol = QConst_t(0.0)>
inline static auto
show(const QGate_Sqrt_Not<_tol>& circuit,
     std::ostream& os,
     const std::string& prefix = "")
{
  os << "Sqrt_Not\n";

  return circuit;
}

} // namespace circuits

} // namespace LibKet

#endif // QCIRCUIT_SQRT_NOT_HPP
