/** @file libket/gates/QGate_Measure_Z.hpp

    @brief LibKet quantum Measure_Z class

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QGATE_MEASURE_Z_HPP
#define QGATE_MEASURE_Z_HPP

#include "QGate.hpp"
#include <QData.hpp>
#include <QFilter.hpp>

namespace LibKet {

namespace gates {

/**
@brief LibKet Measure_Z class

The LibKet Measure_Z class implements the measurement of an arbitrary
number of quantum bits in the Z-basis. Striktly speaking, measure_z is
not a quantum gate.
*/

class QMeasure_Z : public QGate
{
public:
  /// Operator() - by constant reference
  template<typename T>
  inline constexpr auto operator()(const T& t) const noexcept;

  /// Operator() - by universal reference
  template<typename T>
  inline constexpr auto operator()(T&& t) const noexcept;

#ifdef LIBKET_WITH_AQASM
  /// Apply function - specialization for AQASM backend
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static typename std::enable_if<_qbackend == QBackend::AQASM,
                                        QData<_qubits, QBackend::AQASM>>::type&
  apply(QData<_qubits, QBackend::AQASM>& data) noexcept
  {
    for (auto i : _filter::range(data)) {
      data.append_kernel("MEAS q[" + utils::to_string(i) + "]\n");
    }

    return data;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// Apply function - specialization for cQASMv1 backend
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static
    typename std::enable_if<_qbackend == QBackend::cQASMv1,
                            QData<_qubits, QBackend::cQASMv1>>::type&
    apply(QData<_qubits, QBackend::cQASMv1>& data) noexcept
  {
    std::string _expr = "measure_z q[";
    for (auto i : _filter::range(data))
      _expr += utils::to_string(i) +
               (i != *(_filter::range(data).end() - 1) ? "," : "]\n");
    data.append_kernel(_expr);

    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// Apply function - specialization for OpenQASMv2 backend
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static
    typename std::enable_if<_qbackend == QBackend::OpenQASMv2,
                            QData<_qubits, QBackend::OpenQASMv2>>::type&
    apply(QData<_qubits, QBackend::OpenQASMv2>& data) noexcept
  {
    for (auto i : _filter::range(data)) {
      data.append_kernel("measure q[" + utils::to_string(i) + "] -> c[" +
                         utils::to_string(i) + "];\n");
    }

    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// Apply function - specialization for OpenQL backend
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static typename std::enable_if<_qbackend == QBackend::OpenQL,
                                        QData<_qubits, QBackend::OpenQL>>::type&
  apply(QData<_qubits, QBackend::OpenQL>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel([&]() { data.kernel().measurez(i); });

    return data;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// Apply function - specialization for QASM backend
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static typename std::enable_if<_qbackend == QBackend::QASM,
                                        QData<_qubits, QBackend::QASM>>::type&
  apply(QData<_qubits, QBackend::QASM>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("\tmeasure_z q" + utils::to_string(i) + "\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// Apply function - specialization for Quil backend
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static typename std::enable_if<_qbackend == QBackend::Quil,
                                        QData<_qubits, QBackend::Quil>>::type&
  apply(QData<_qubits, QBackend::Quil>& data) noexcept
  {
    for (auto i : _filter::range(data)) {
      data.append_kernel("MEASURE " + utils::to_string(i) + " c[" +
                         utils::to_string(i) + "]\n");
    }

    return data;
  }
#endif

#ifdef LIBKET_WITH_QX
  /// Apply function - specialization for QX-simulator backend
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static typename std::enable_if<_qbackend == QBackend::QX,
                                        QData<_qubits, QBackend::QX>>::type&
  apply(QData<_qubits, QBackend::QX>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel(new qx::measure(i));

    return data;
  }
#endif
};

/**
@brief LibKet Measure_Z gate creator

This overload of the LibKet::gates::measure_z() function can be used as
terminal, i.e. the inner-most gate in a quantum expression

\code
auto expr = gates::measure_z();
\endcode
*/
inline constexpr auto
measure_z() noexcept
{
  return UnaryQGate<filters::QFilter, QMeasure_Z>(filters::QFilter{});
}

#ifdef LIBKET_OPTIMIZE_GATES

/**
@brief LibKet Measure_Z gate creator

This overload of the LibKet::gates::measure_z() function eliminates the
double-application of the Measure_Z gate
*/
template<typename _expr, typename _filter>
inline constexpr auto
measure_z(
  const UnaryQGate<_expr, QMeasure_Z, typename filters::getFilter<_expr>::type>&
    expr) noexcept
{
  return expr;
}

/**
@brief LibKet Measure_Z gate creator

This overload of the LibKet::gates::measure_z() function eliminates the
double-application of the Measure_Z gate
*/
template<typename _expr>
inline constexpr auto
measure_z(
  UnaryQGate<_expr, QMeasure_Z, typename filters::getFilter<_expr>::type>&&
    expr) noexcept
{
  return expr;
}

#endif // LIBKET_OPTIMIZE_GATES

/**
@brief LibKet Measure_Z gate creator

This overload of the LibKet::gates::measure_z() function accepts an
expression as constant reference
*/
template<typename _expr>
inline constexpr auto
measure_z(const _expr& expr) noexcept
{
  return UnaryQGate<_expr,
                    QMeasure_Z,
                    typename filters::getFilter<_expr>::type>(expr);
}

/**
@brief LibKet Measure_Z gate creator

This overload of the LibKet::gates::measure_z() function accepts an
expression as universal reference
*/
template<typename _expr>
inline constexpr auto
measure_z(_expr&& expr) noexcept
{
  return UnaryQGate<_expr,
                    QMeasure_Z,
                    typename filters::getFilter<_expr>::type>(expr);
}

/**
@brief LibKet Measure_Z gate creator

Function alias for LibKet::gates::measure_z
*/
template<typename... Args>
inline constexpr auto
MEASURE_Z(Args&&... args)
{
  return measure_z(std::forward<Args>(args)...);
}

/// Operator() - by constant reference
template<typename T>
inline constexpr auto
QMeasure_Z::operator()(const T& t) const noexcept
{
  return measure_z(std::forward<T>(t));
}

/// Operator() - by universal reference
template<typename T>
inline constexpr auto
QMeasure_Z::operator()(T&& t) const noexcept
{
  return measure_z(std::forward<T>(t));
}

/**
   @brief LibKet show gate type - specialization for QMeasure_Z objects
*/
template<std::size_t level = 1>
inline static auto
show(const QMeasure_Z& gate, std::ostream& os, const std::string& prefix = "")
{
  os << "QMeasure_Z\n";

  return gate;
}

} // namespace gates

} // namespace LibKet

#endif // QGATE_MEASURE_Z_HPP
