/** @file libket/gates/QGate_CZ.hpp

    @brief LibKet quantum CZ (controlled-Z gate) class

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QGATE_CZ_HPP
#define QGATE_CZ_HPP

#include "QGate.hpp"
#include <QData.hpp>
#include <QFilter.hpp>

namespace LibKet {

namespace gates {

/**
@brief LibKet CZ (controlled-Z) gate class

The LibKet CZ (controlled-Z) gate class implements the quantum
controlled-Z gate for an arbitrary number of quantum bits
*/

class QCZ : public QGate
{
public:
  /// Operator() - by constant reference
  template<typename T0, typename T1>
  inline constexpr auto operator()(const T0& t0, const T1& t1) const noexcept;

  /// Operator() - by constant and universal reference
  template<typename T0, typename T1>
  inline constexpr auto operator()(const T0& t0, T1&& t1) const noexcept;

  /// Operator() - by universal and constant reference
  template<typename T0, typename T1>
  inline constexpr auto operator()(T0&& t0, const T1& t1) const noexcept;

  /// Operator() - by universal reference
  template<typename T0, typename T1>
  inline constexpr auto operator()(T0&& t0, T1&& t1) const noexcept;

#ifdef LIBKET_WITH_AQASM
  /// Apply function - specialization for AQASM backend
  template<std::size_t _qubits,
           QBackend _qbackend,
           typename _filter0,
           typename _filter1>
  inline static typename std::enable_if<_qbackend == QBackend::AQASM,
                                        QData<_qubits, QBackend::AQASM>>::type&
  apply(QData<_qubits, QBackend::AQASM>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits> == _filter1::template size<_qubits>,
      "CZ gate can only be applied to quantum objects of the same size");
    auto it = _filter1::range(data).begin();
    for (auto i : _filter0::range(data))
      data.append_kernel("CTRL(Z) q[" + utils::to_string(i) + "],q[" +
                         utils::to_string(*(it++)) + "]\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// Apply function - specialization for cQASMv1 backend
  template<std::size_t _qubits,
           QBackend _qbackend,
           typename _filter0,
           typename _filter1>
  inline static
    typename std::enable_if<_qbackend == QBackend::cQASMv1,
                            QData<_qubits, QBackend::cQASMv1>>::type&
    apply(QData<_qubits, QBackend::cQASMv1>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits> == _filter1::template size<_qubits>,
      "CZ gate can only be applied to quantum objects of the same size");
    std::string _expr = "cz q[";
    for (auto i : _filter0::range(data))
      _expr += utils::to_string(i) +
               (i != *(_filter0::range(data).end() - 1) ? "," : "], q[");
    for (auto i : _filter1::range(data))
      _expr += utils::to_string(i) +
               (i != *(_filter1::range(data).end() - 1) ? "," : "]\n");
    data.append_kernel(_expr);

    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// Apply function - specialization for OpenQASMv2 backend
  template<std::size_t _qubits,
           QBackend _qbackend,
           typename _filter0,
           typename _filter1>
  inline static
    typename std::enable_if<_qbackend == QBackend::OpenQASMv2,
                            QData<_qubits, QBackend::OpenQASMv2>>::type&
    apply(QData<_qubits, QBackend::OpenQASMv2>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits> == _filter1::template size<_qubits>,
      "CZ gate can only be applied to quantum objects of the same size");
    auto it = _filter1::range(data).begin();
    for (auto i : _filter0::range(data))
      data.append_kernel("cz q[" + utils::to_string(i) + "], q[" +
                         utils::to_string(*(it++)) + "];\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// Apply function - specialization for OpenQL backend
  template<std::size_t _qubits,
           QBackend _qbackend,
           typename _filter0,
           typename _filter1>
  inline static typename std::enable_if<_qbackend == QBackend::OpenQL,
                                        QData<_qubits, QBackend::OpenQL>>::type&
  apply(QData<_qubits, QBackend::OpenQL>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits> == _filter1::template size<_qubits>,
      "CZ gate can only be applied to quantum objects of the same size");
    auto it = _filter1::range(data).begin();
    for (auto i : _filter0::range(data))
      data.append_kernel([&]() { data.kernel().cz(i, *(it++)); });

    return data;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// Apply function - specialization for QASM backend
  template<std::size_t _qubits,
           QBackend _qbackend,
           typename _filter0,
           typename _filter1>
  inline static typename std::enable_if<_qbackend == QBackend::QASM,
                                        QData<_qubits, QBackend::QASM>>::type&
  apply(QData<_qubits, QBackend::QASM>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits> == _filter1::template size<_qubits>,
      "CZ gate can only be applied to quantum objects of the same size");
    auto it = _filter1::range(data).begin();
    for (auto i : _filter0::range(data))
      data.append_kernel("\tcz q" + utils::to_string(i) + ",q" +
                         utils::to_string(*(it++)) + "\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// Apply function - specialization for Quil backend
  template<std::size_t _qubits,
           QBackend _qbackend,
           typename _filter0,
           typename _filter1>
  inline static typename std::enable_if<_qbackend == QBackend::Quil,
                                        QData<_qubits, QBackend::Quil>>::type&
  apply(QData<_qubits, QBackend::Quil>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits> == _filter1::template size<_qubits>,
      "CZ gate can only be applied to quantum objects of the same size");
    auto it = _filter1::range(data).begin();
    for (auto i : _filter0::range(data))
      data.append_kernel("CZ " + utils::to_string(i) + " " +
                         utils::to_string(*(it++)) + "\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_QX
  /// Apply function - specialization for QX-simulator backend
  template<std::size_t _qubits,
           QBackend _qbackend,
           typename _filter0,
           typename _filter1>
  inline static typename std::enable_if<_qbackend == QBackend::QX,
                                        QData<_qubits, QBackend::QX>>::type&
  apply(QData<_qubits, QBackend::QX>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits> == _filter1::template size<_qubits>,
      "CZ gate can only be applied to quantum objects of the same size");
    auto it = _filter1::range(data).begin();
    for (auto i : _filter0::range(data))
      data.append_kernel(
        new qx::cnot(i, *(it++))); // TODO: Change to CZ once available

    return data;
  }
#endif
};

/**
@brief LibKet CZ (controlled-Z) gate creator

This overload of the LibKet::gates::cz() function can be used as
terminal, i.e. the inner-most gate in a quantum expression

\code
auto expr = gates::cz();
\endcode
*/
inline constexpr auto
cz() noexcept
{
  return BinaryQGate<filters::QFilter, filters::QFilter, QCZ>(
    filters::QFilter{}, filters::QFilter{});
}

/**
@brief LibKet CZ (controlled-Z) gate creator

This overload of the LibKet::gates::cz() function accepts
two expressions as constant reference
*/
template<typename _expr0, typename _expr1>
inline constexpr auto
cz(const _expr0& expr0, const _expr1& expr1) noexcept
{
  return BinaryQGate<_expr0,
                     _expr1,
                     QCZ,
                     decltype(typename filters::getFilter<_expr0>::type{}
                              << typename filters::getFilter<_expr1>::type{})>(
    expr0, expr1);
}

/**
@brief LibKet CZ (controlled-Z) gate creator

This overload of the LibKet::gates::cz() function accepts the first
expression as constant reference and the second expression as
universal reference
*/
template<typename _expr0, typename _expr1>
inline constexpr auto
cz(const _expr0& expr0, _expr1&& expr1) noexcept
{
  return BinaryQGate<_expr0,
                     _expr1,
                     QCZ,
                     decltype(typename filters::getFilter<_expr0>::type{}
                              << typename filters::getFilter<_expr1>::type{})>(
    expr0, expr1);
}

/**
@brief LibKet CZ (controlled-Z) gate creator

This overload of the LibKet::gates::cz() function accepts the first
expression as universal reference and the second expression as
constant reference
*/
template<typename _expr0, typename _expr1>
inline constexpr auto
cz(_expr0&& expr0, const _expr1& expr1) noexcept
{
  return BinaryQGate<_expr0,
                     _expr1,
                     QCZ,
                     decltype(typename filters::getFilter<_expr0>::type{}
                              << typename filters::getFilter<_expr1>::type{})>(
    expr0, expr1);
}

/**
@brief LibKet CZ (controlled-Z) gate creator

This overload of the LibKet::gates::cz() function accepts
two expression as universal reference
*/
template<typename _expr0, typename _expr1>
inline constexpr auto
cz(_expr0&& expr0, _expr1&& expr1) noexcept
{
  return BinaryQGate<_expr0,
                     _expr1,
                     QCZ,
                     decltype(typename filters::getFilter<_expr0>::type{}
                              << typename filters::getFilter<_expr1>::type{})>(
    expr0, expr1);
}

/**
@brief LibKet CZ (controlled-Z) gate creator

Function alias for LibKet::gates::cz
*/
template<typename... Args>
inline constexpr auto
CZ(Args&&... args)
{
  return cz(std::forward<Args>(args)...);
}

/// Operator() - by constant reference
template<typename T0, typename T1>
inline constexpr auto
QCZ::operator()(const T0& t0, const T1& t1) const noexcept
{
  return cz(std::forward<T0>(t0), std::forward<T1>(t1));
}

/// Operator() - by constant and universal reference
template<typename T0, typename T1>
inline constexpr auto
QCZ::operator()(const T0& t0, T1&& t1) const noexcept
{
  return cz(std::forward<T0>(t0), std::forward<T1>(t1));
}

/// Operator() - by universal and constant reference
template<typename T0, typename T1>
inline constexpr auto
QCZ::operator()(T0&& t0, const T1& t1) const noexcept
{
  return cz(std::forward<T0>(t0), std::forward<T1>(t1));
}

/// Operator() - by universal reference
template<typename T0, typename T1>
inline constexpr auto
QCZ::operator()(T0&& t0, T1&& t1) const noexcept
{
  return cz(std::forward<T0>(t0), std::forward<T1>(t1));
}

/**
   @brief LibKet show gate type - specialization for QCZ objects
*/
template<std::size_t level = 1>
inline static auto
show(const QCZ& gate, std::ostream& os, const std::string& prefix = "")
{
  os << "QCZ"
     << "\n";

  return gate;
}

} // namespace gates

} // namespace LibKet

#endif // QGATE_CZ_HPP
