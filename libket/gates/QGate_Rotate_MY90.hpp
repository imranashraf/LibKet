/** @file libket/gates/QGate_Rotate_MY90.hpp

    @brief LibKet quantum Rotate_MY90 class

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QGATE_ROTATE_MY90_HPP
#define QGATE_ROTATE_MY90_HPP

#include "QGate.hpp"
#include <QData.hpp>
#include <QFilter.hpp>

namespace LibKet {

namespace gates {

/**
@brief LibKet Rotate_MY90 gate class

The LibKet Rotate_MY90 gate class implements the quantum
Rotate_MY90 gate for an arbitrary number of quantum bits
*/

class QRotate_MY90 : public QGate
{
public:
  /// Operator() - by constant reference
  template<typename T>
  inline constexpr auto operator()(const T& t) const noexcept;

  /// Operator() - by universal reference
  template<typename T>
  inline constexpr auto operator()(T&& t) const noexcept;

#ifdef LIBKET_WITH_AQASM
  /// Apply function - specialization for AQASM backend
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static typename std::enable_if<_qbackend == QBackend::AQASM,
                                        QData<_qubits, QBackend::AQASM>>::type&
  apply(QData<_qubits, QBackend::AQASM>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("RY[-PI/2.0] q[" + utils::to_string(i) + "]\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// Apply function - specialization for cQASMv1 backend
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static
    typename std::enable_if<_qbackend == QBackend::cQASMv1,
                            QData<_qubits, QBackend::cQASMv1>>::type&
    apply(QData<_qubits, QBackend::cQASMv1>& data) noexcept
  {
    std::string _expr = "my90 q[";
    for (auto i : _filter::range(data))
      _expr += utils::to_string(i) +
               (i != *(_filter::range(data).end() - 1) ? "," : "]\n");
    data.append_kernel(_expr);

    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// Apply function - specialization for OpenQASMv2 backend
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static
    typename std::enable_if<_qbackend == QBackend::OpenQASMv2,
                            QData<_qubits, QBackend::OpenQASMv2>>::type&
    apply(QData<_qubits, QBackend::OpenQASMv2>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("ry(" + utils::to_string(M_PI_2 + M_PI) + ") q[" +
                         utils::to_string(i) + "];\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// Apply function - specialization for OpenQL backend
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static typename std::enable_if<_qbackend == QBackend::OpenQL,
                                        QData<_qubits, QBackend::OpenQL>>::type&
  apply(QData<_qubits, QBackend::OpenQL>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel([&]() { data.kernel().my90(i); });

    return data;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// Apply function - specialization for QASM backend
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static typename std::enable_if<_qbackend == QBackend::QASM,
                                        QData<_qubits, QBackend::QASM>>::type&
  apply(QData<_qubits, QBackend::QASM>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("\tmy90 q" + utils::to_string(i) + "\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// Apply function - specialization for Quil backend
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static typename std::enable_if<_qbackend == QBackend::Quil,
                                        QData<_qubits, QBackend::Quil>>::type&
  apply(QData<_qubits, QBackend::Quil>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("RY(" + utils::to_string(M_PI_2 + M_PI) + ") " +
                         utils::to_string(i) + "\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_QX
  /// Apply function - specialization for QX-simulator backend
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static typename std::enable_if<_qbackend == QBackend::QX,
                                        QData<_qubits, QBackend::QX>>::type&
  apply(QData<_qubits, QBackend::QX>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel(new qx::ry(i, M_PI_2 + M_PI));

    return data;
  }
#endif
};

/**
@brief LibKet Rotate_MY90 gate creator

This overload of the LibKet::gate:rotate_my90() function can be
used as terminal, i.e. the inner-most gate in a quantum
expression

\code
auto expr = gates::rotate_my90();
\endcode
*/
inline constexpr auto
rotate_my90() noexcept
{
  return UnaryQGate<filters::QFilter, QRotate_MY90>(filters::QFilter{});
}

#ifdef LIBKET_OPTIMIZE_GATES

// Forward declaration
class QRotate_Y90;

/**
@brief LibKet Rotate_MY90 gate creator

This overload of the LibKet::gate:rotate_my90() function eliminates
the application of the Rotate_MY90 gate to a Rotate_Y90 gate
*/
template<typename _expr, typename _filter>
inline constexpr auto
rotate_my90(
  const UnaryQGate<_expr,
                   QRotate_Y90,
                   typename filters::getFilter<_expr>::type>& expr) noexcept
{
  return expr.expr;
}

/**
@brief LibKet Rotate_MY90 gate creator

This overload of the LibKet::gate:rotate_my90() function eliminates
the application of the Rotate_MY90 gate to a Rotate_Y90 gate
*/
template<typename _expr>
inline constexpr auto
rotate_my90(
  UnaryQGate<_expr, QRotate_MY90, typename filters::getFilter<_expr>::type>&&
    expr) noexcept
{
  return expr.expr;
}

#endif // LIBKET_OPTIMIZE_GATES

/**
@brief LibKet Rotate_MY90 gate creator

This overload of the LibKet::gates::rotate_my90() function accepts
an expression as constant reference
*/
template<typename _expr>
inline constexpr auto
rotate_my90(const _expr& expr) noexcept
{
  return UnaryQGate<_expr,
                    QRotate_MY90,
                    typename filters::getFilter<_expr>::type>(expr);
}

/**
@brief LibKet Rotate_MY90 gate creator

This overload of the LibKet::gates::rotate_my90() function accepts
an expression as universal reference
*/
template<typename _expr>
inline constexpr auto
rotate_my90(_expr&& expr) noexcept
{
  return UnaryQGate<_expr,
                    QRotate_MY90,
                    typename filters::getFilter<_expr>::type>(expr);
}

/**
@brief LibKet Rotate_MY90 gate creator

Function alias for LibKet::gates::my90
*/
template<typename... Args>
inline constexpr auto
ROTATE_MY90(Args&&... args)
{
  return rotate_my90(std::forward<Args>(args)...);
}

/**
@brief LibKet Rotate_MY90 gate creator

Function alias for LibKet::gates::rotate_y90
*/
template<typename... Args>
inline constexpr auto
rmy90(Args&&... args)
{
  return rotate_my90(std::forward<Args>(args)...);
}

/**
@brief LibKet Rotate_MY90 gate creator

Function alias for LibKet::gates::rotate_my90
*/
template<typename... Args>
inline constexpr auto
RMY90(Args&&... args)
{
  return rotate_my90(std::forward<Args>(args)...);
}

/**
@brief LibKet Rotate_MY90 gate creator

Function alias for LibKet::gates::rotate_my90
*/
template<typename... Args>
inline constexpr auto
Rmy90(Args&&... args)
{
  return rotate_my90(std::forward<Args>(args)...);
}

/// Operator() - by constant reference
template<typename T>
inline constexpr auto
QRotate_MY90::operator()(const T& t) const noexcept
{
  return rotate_my90(std::forward<T>(t));
}

/// Operator() - by universal reference
template<typename T>
inline constexpr auto
QRotate_MY90::operator()(T&& t) const noexcept
{
  return rotate_my90(std::forward<T>(t));
}

/**
   @brief LibKet show gate type - specialization for QRotate_MY90 objects
*/
template<std::size_t level = 1>
inline static auto
show(const QRotate_MY90& gate, std::ostream& os, const std::string& prefix = "")
{
  os << "QRotate_MY90\n";

  return gate;
}

} // namespace gates

} // namespace LibKet

#endif // QGATE_ROTATE_MY90_HPP
