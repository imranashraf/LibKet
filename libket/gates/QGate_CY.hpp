/** @file libket/gates/QGate_CY.hpp

    @brief LibKet quantum CY (controlled-Y gate) class

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QGATE_CY_HPP
#define QGATE_CY_HPP

#include "QGate.hpp"
#include <QData.hpp>
#include <QFilter.hpp>

namespace LibKet {

namespace gates {

/**
@brief LibKet CY (controlled-Y) gate class

The LibKet CY (controlled-Y) gate class implements the quantum
controlled-Y gate for an arbitrary number of quantum bits
*/

class QCY : public QGate
{
public:
  /// Operator() - by constant reference
  template<typename T0, typename T1>
  inline constexpr auto operator()(const T0& t0, const T1& t1) const noexcept;

  /// Operator() - by constant and universal reference
  template<typename T0, typename T1>
  inline constexpr auto operator()(const T0& t0, T1&& t1) const noexcept;

  /// Operator() - by universal and constant reference
  template<typename T0, typename T1>
  inline constexpr auto operator()(T0&& t0, const T1& t1) const noexcept;

  /// Operator() - by universal reference
  template<typename T0, typename T1>
  inline constexpr auto operator()(T0&& t0, T1&& t1) const noexcept;

#ifdef LIBKET_WITH_AQASM
  /// Apply function - specialization for AQASM backend
  template<std::size_t _qubits,
           QBackend _qbackend,
           typename _filter0,
           typename _filter1>
  inline static typename std::enable_if<_qbackend == QBackend::AQASM,
                                        QData<_qubits, QBackend::AQASM>>::type&
  apply(QData<_qubits, QBackend::AQASM>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits> == _filter1::template size<_qubits>,
      "CY gate can only be applied to quantum objects of the same size");
    auto it = _filter1::range(data).begin();
    for (auto i : _filter0::range(data))
      data.append_kernel("CTRL(Y) q[" + utils::to_string(i) + "],q[" +
                         utils::to_string(*(it++)) + "]\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// Apply function - specialization for cQASMv1 backend
  template<std::size_t _qubits,
           QBackend _qbackend,
           typename _filter0,
           typename _filter1>
  inline static
    typename std::enable_if<_qbackend == QBackend::cQASMv1,
                            QData<_qubits, QBackend::cQASMv1>>::type&
    apply(QData<_qubits, QBackend::cQASMv1>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits> == _filter1::template size<_qubits>,
      "CY gate can only be applied to quantum objects of the same size");
    std::string _expr = "cy q[";
    for (auto i : _filter0::range(data))
      _expr += utils::to_string(i) +
               (i != *(_filter0::range(data).end() - 1) ? "," : "], q[");
    for (auto i : _filter1::range(data))
      _expr += utils::to_string(i) +
               (i != *(_filter1::range(data).end() - 1) ? "," : "]\n");
    data.append_kernel(_expr);

    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// Apply function - specialization for OpenQASMv2 backend
  template<std::size_t _qubits,
           QBackend _qbackend,
           typename _filter0,
           typename _filter1>
  inline static
    typename std::enable_if<_qbackend == QBackend::OpenQASMv2,
                            QData<_qubits, QBackend::OpenQASMv2>>::type&
    apply(QData<_qubits, QBackend::OpenQASMv2>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits> == _filter1::template size<_qubits>,
      "CY gate can only be applied to quantum objects of the same size");
    auto it = _filter1::range(data).begin();
    for (auto i : _filter0::range(data))
      data.append_kernel("cy q[" + utils::to_string(i) + "], q[" +
                         utils::to_string(*(it++)) + "];\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// Apply function - specialization for OpenQL backend
  template<std::size_t _qubits,
           QBackend _qbackend,
           typename _filter0,
           typename _filter1>
  inline static typename std::enable_if<_qbackend == QBackend::OpenQL,
                                        QData<_qubits, QBackend::OpenQL>>::type&
  apply(QData<_qubits, QBackend::OpenQL>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits> == _filter1::template size<_qubits>,
      "CY gate can only be applied to quantum objects of the same size");
    auto it = _filter1::range(data).begin();
    for (auto i : _filter0::range(data))
      data.append_kernel([&]() { data.kernel().cy(i, *(it++)); });

    return data;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// Apply function - specialization for QASM backend
  template<std::size_t _qubits,
           QBackend _qbackend,
           typename _filter0,
           typename _filter1>
  inline static typename std::enable_if<_qbackend == QBackend::QASM,
                                        QData<_qubits, QBackend::QASM>>::type&
  apply(QData<_qubits, QBackend::QASM>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits> == _filter1::template size<_qubits>,
      "CY gate can only be applied to quantum objects of the same size");
    auto it = _filter1::range(data).begin();
    for (auto i : _filter0::range(data))
      data.append_kernel("\tcy q" + utils::to_string(i) + ",q" +
                         utils::to_string(*(it++)) + "\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// Apply function - specialization for Quil backend
  template<std::size_t _qubits,
           QBackend _qbackend,
           typename _filter0,
           typename _filter1>
  inline static typename std::enable_if<_qbackend == QBackend::Quil,
                                        QData<_qubits, QBackend::Quil>>::type&
  apply(QData<_qubits, QBackend::Quil>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits> == _filter1::template size<_qubits>,
      "CY gate can only be applied to quantum objects of the same size");
    auto it = _filter1::range(data).begin();
    for (auto i : _filter0::range(data))
      data.append_kernel("CY " + utils::to_string(i) + " " +
                         utils::to_string(*(it++)) + "\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_QX
  /// Apply function - specialization for QX-simulator backend
  template<std::size_t _qubits,
           QBackend _qbackend,
           typename _filter0,
           typename _filter1>
  inline static typename std::enable_if<_qbackend == QBackend::QX,
                                        QData<_qubits, QBackend::QX>>::type&
  apply(QData<_qubits, QBackend::QX>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits> == _filter1::template size<_qubits>,
      "CY gate can only be applied to quantum objects of the same size");
    auto it = _filter1::range(data).begin();
    for (auto i : _filter0::range(data))
      data.append_kernel(
        new qx::cnot(i, *(it++))); // TODO: Change to CY once available

    return data;
  }
#endif
};

/**
@brief LibKet CY (controlled-Y) gate creator

This overload of the LibKet::gates::cy() function can be used as
terminal, i.e. the inner-most gate in a quantum expression

\code
auto expr = gates::cy();
\endcode
*/
inline constexpr auto
cy() noexcept
{
  return BinaryQGate<filters::QFilter, filters::QFilter, QCY>(
    filters::QFilter{}, filters::QFilter{});
}

/**
@brief LibKet CY (controlled-Y) gate creator

This overload of the LibKet::gates::cy() function accepts
two expressions as constant reference
*/
template<typename _expr0, typename _expr1>
inline constexpr auto
cy(const _expr0& expr0, const _expr1& expr1) noexcept
{
  return BinaryQGate<_expr0,
                     _expr1,
                     QCY,
                     decltype(typename filters::getFilter<_expr0>::type{}
                              << typename filters::getFilter<_expr1>::type{})>(
    expr0, expr1);
}

/**
@brief LibKet CY (controlled-Y) gate creator

This overload of the LibKet::gates::cy() function accepts the first
expression as constant reference and the second expression as
universal reference
*/
template<typename _expr0, typename _expr1>
inline constexpr auto
cy(const _expr0& expr0, _expr1&& expr1) noexcept
{
  return BinaryQGate<_expr0,
                     _expr1,
                     QCY,
                     decltype(typename filters::getFilter<_expr0>::type{}
                              << typename filters::getFilter<_expr1>::type{})>(
    expr0, expr1);
}

/**
@brief LibKet CY (controlled-Y) gate creator

This overload of the LibKet::gates::cy() function accepts the first
expression as universal reference and the second expression as
constant reference
*/
template<typename _expr0, typename _expr1>
inline constexpr auto
cy(_expr0&& expr0, const _expr1& expr1) noexcept
{
  return BinaryQGate<_expr0,
                     _expr1,
                     QCY,
                     decltype(typename filters::getFilter<_expr0>::type{}
                              << typename filters::getFilter<_expr1>::type{})>(
    expr0, expr1);
}

/**
@brief LibKet CY (controlled-Y) gate creator

This overload of the LibKet::gates::cy() function accepts
two expression as universal reference
*/
template<typename _expr0, typename _expr1>
inline constexpr auto
cy(_expr0&& expr0, _expr1&& expr1) noexcept
{
  return BinaryQGate<_expr0,
                     _expr1,
                     QCY,
                     decltype(typename filters::getFilter<_expr0>::type{}
                              << typename filters::getFilter<_expr1>::type{})>(
    expr0, expr1);
}

/**
@brief LibKet CY (controlled-Y) gate creator

Function alias for LibKet::gates::cy
*/
template<typename... Args>
inline constexpr auto
CY(Args&&... args)
{
  return cy(std::forward<Args>(args)...);
}

/// Operator() - by constant reference
template<typename T0, typename T1>
inline constexpr auto
QCY::operator()(const T0& t0, const T1& t1) const noexcept
{
  return cy(std::forward<T0>(t0), std::forward<T1>(t1));
}

/// Operator() - by constant and universal reference
template<typename T0, typename T1>
inline constexpr auto
QCY::operator()(const T0& t0, T1&& t1) const noexcept
{
  return cy(std::forward<T0>(t0), std::forward<T1>(t1));
}

/// Operator() - by universal and constant reference
template<typename T0, typename T1>
inline constexpr auto
QCY::operator()(T0&& t0, const T1& t1) const noexcept
{
  return cy(std::forward<T0>(t0), std::forward<T1>(t1));
}

/// Operator() - by universal reference
template<typename T0, typename T1>
inline constexpr auto
QCY::operator()(T0&& t0, T1&& t1) const noexcept
{
  return cy(std::forward<T0>(t0), std::forward<T1>(t1));
}

/**
   @brief LibKet show gate type - specialization for QCY objects
*/
template<std::size_t level = 1>
inline static auto
show(const QCY& gate, std::ostream& os, const std::string& prefix = "")
{
  os << "QCY"
     << "\n";

  return gate;
}

} // namespace gates

} // namespace LibKet

#endif // QGATE_CY_HPP
