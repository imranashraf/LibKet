/** @file libket/gates/QGate_CPhaseK.hpp

    @brief LibKet quantum CPHASEK (controlled phase shift with pi/2^k angle)
   class

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QGATE_CPHASEK_HPP
#define QGATE_CPHASEK_HPP

#include "QGate.hpp"
#include <QData.hpp>
#include <QFilter.hpp>

namespace LibKet {

namespace gates {

/**
@brief LibKet CPHASEK (controlled phase shift with pi/2^k angle) gate class

The LibKet CPHASEK (controlled phase shift with pi/2^k angle) gate
class implements the quantum controlled phase shift with pi/2^k angle
gate for an arbitrary number of quantum bits
*/

template<std::size_t k, typename _tol = QConst_t(0.0)>
class QCPhaseK : public QGate
{
public:
  /// Rotation angle 2*PI/2^k
  real_t static constexpr angle = 2 * M_PI / (1 << k);

  /// Operator() - by constant reference
  template<typename T0, typename T1>
  inline constexpr auto operator()(const T0& t0, const T1& t1) const noexcept;

  /// Operator() - by constant and universal reference
  template<typename T0, typename T1>
  inline constexpr auto operator()(const T0& t0, T1&& t1) const noexcept;

  /// Operator() - by universal and constant reference
  template<typename T0, typename T1>
  inline constexpr auto operator()(T0&& t0, const T1& t1) const noexcept;

  /// Operator() - by universal reference
  template<typename T0, typename T1>
  inline constexpr auto operator()(T0&& t0, T1&& t1) const noexcept;

#ifdef LIBKET_WITH_AQASM
  /// Apply function - specialization for AQASM backend
  template<std::size_t _qubits,
           QBackend _qbackend,
           typename _filter0,
           typename _filter1>
  inline static typename std::enable_if<_qbackend == QBackend::AQASM,
                                        QData<_qubits, QBackend::AQASM>>::type&
  apply(QData<_qubits, QBackend::AQASM>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits> == _filter1::template size<_qubits>,
      "CPHASEK gate can only be applied to quantum objects of the same size");
    if (tolerance(angle, _tol{})) {
      auto it = _filter1::range(data).begin();
      for (auto i : _filter0::range(data))
        data.append_kernel("CTRL(PH[" + utils::to_string(angle) + "]) q[" +
                           utils::to_string(i) + "],q[" +
                           utils::to_string(*(it++)) + "]\n");
    }
    return data;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// Apply function - specialization for cQASMv1 backend
  template<std::size_t _qubits,
           QBackend _qbackend,
           typename _filter0,
           typename _filter1>
  inline static
    typename std::enable_if<_qbackend == QBackend::cQASMv1,
                            QData<_qubits, QBackend::cQASMv1>>::type&
    apply(QData<_qubits, QBackend::cQASMv1>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits> == _filter1::template size<_qubits>,
      "CPHASEK gate can only be applied to quantum objects of the same size");
    if (tolerance(angle, _tol{})) {
      std::string _expr = "cr q[";
      for (auto i : _filter0::range(data))
        _expr += utils::to_string(i) +
                 (i != *(_filter0::range(data).end() - 1) ? "," : "], q[");
      for (auto i : _filter1::range(data))
        _expr += utils::to_string(i) +
                 (i != *(_filter1::range(data).end() - 1) ? "," : "], ");
      _expr += utils::to_string(angle) + "\n";
      data.append_kernel(_expr);
    }
    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// Apply function - specialization for OpenQASMv2 backend
  template<std::size_t _qubits,
           QBackend _qbackend,
           typename _filter0,
           typename _filter1>
  inline static
    typename std::enable_if<_qbackend == QBackend::OpenQASMv2,
                            QData<_qubits, QBackend::OpenQASMv2>>::type&
    apply(QData<_qubits, QBackend::OpenQASMv2>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits> == _filter1::template size<_qubits>,
      "CPHASEK gate can only be applied to quantum objects of the same size");
    if (tolerance(angle, _tol{})) {
      auto it = _filter1::range(data).begin();
      for (auto i : _filter0::range(data))
        data.append_kernel("cu1(" + utils::to_string(angle) + ") q[" +
                           utils::to_string(i) + "], q[" +
                           utils::to_string(*(it++)) + "];\n");
    }
    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// Apply function - specialization for OpenQL backend
  template<std::size_t _qubits,
           QBackend _qbackend,
           typename _filter0,
           typename _filter1>
  inline static typename std::enable_if<_qbackend == QBackend::OpenQL,
                                        QData<_qubits, QBackend::OpenQL>>::type&
  apply(QData<_qubits, QBackend::OpenQL>& data) noexcept
  {
    std::cerr << "The CR/CPHASEK gate is not implemented for OpenQL!!!\n";
    static_assert(
      _filter0::template size<_qubits> == _filter1::template size<_qubits>,
      "CPHASEK gate can only be applied to quantum objects of the same size");
    if (tolerance(angle, _tol{})) {
      auto it = _filter1::range(data).begin();
      for (auto i : _filter0::range(data))
        data.append_kernel(
          [&]() { data.kernel().controlled_rz(i, *(it++), angle); });
    }
    return data;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// Apply function - specialization for QASM backend
  template<std::size_t _qubits,
           QBackend _qbackend,
           typename _filter0,
           typename _filter1>
  inline static typename std::enable_if<_qbackend == QBackend::QASM,
                                        QData<_qubits, QBackend::QASM>>::type&
  apply(QData<_qubits, QBackend::QASM>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits> == _filter1::template size<_qubits>,
      "CPHASEK gate can only be applied to quantum objects of the same size");
    if (tolerance(angle, _tol{})) {
      auto it = _filter1::range(data).begin();
      for (auto i : _filter0::range(data))
        data.append_kernel(
          "\tcphase" + utils::to_string(k) + " q" + utils::to_string(i) + ",q" +
          utils::to_string(*(it++)) + " # 2pi/2^" + utils::to_string(k) + "\n");
    }
    return data;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// Apply function - specialization for Quil backend
  template<std::size_t _qubits,
           QBackend _qbackend,
           typename _filter0,
           typename _filter1>
  inline static typename std::enable_if<_qbackend == QBackend::Quil,
                                        QData<_qubits, QBackend::Quil>>::type&
  apply(QData<_qubits, QBackend::Quil>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits> == _filter1::template size<_qubits>,
      "CPHASEK gate can only be applied to quantum objects of the same size");
    if (tolerance(angle, _tol{})) {
      auto it = _filter1::range(data).begin();
      for (auto i : _filter0::range(data))
        data.append_kernel("CPHASE (" + utils::to_string(angle) + ") " +
                           utils::to_string(i) + " " +
                           utils::to_string(*(it++)) + "\n");
    }
    return data;
  }
#endif

#ifdef LIBKET_WITH_QX
  /// Apply function - specialization for QX-simulator backend
  template<std::size_t _qubits,
           QBackend _qbackend,
           typename _filter0,
           typename _filter1>
  inline static typename std::enable_if<_qbackend == QBackend::QX,
                                        QData<_qubits, QBackend::QX>>::type&
  apply(QData<_qubits, QBackend::QX>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits> == _filter1::template size<_qubits>,
      "CPHASEK gate can only be applied to quantum objects of the same size");
    if (tolerance(angle, _tol{})) {
      auto it = _filter1::range(data).begin();
      for (auto i : _filter0::range(data))
        data.append_kernel(new qx::ctrl_phase_shift(i, *(it++), (double)angle));
    }
    return data;
  }
#endif
};

// Complete type definition
template<std::size_t k, typename _tol>
real_t constexpr QCPhaseK<k, _tol>::angle;

/**
@brief LibKet CPHASEK (controlled phase shift with pi/2^k angle) gate creator

This overload of the LibKet::gates::cphasek() function can be used as
terminal, i.e. the inner-most gate in a quantum expression

\code
auto expr = gates::cphasek();
\endcode
*/
template<std::size_t k, typename _tol = QConst_t(0.0)>
inline constexpr auto
cphasek() noexcept
{
  return BinaryQGate<filters::QFilter, filters::QFilter, QCPhaseK<k, _tol>>(
    filters::QFilter{}, filters::QFilter{});
}

/**
@brief LibKet CPHASEK (controlled phase shift with pi/2^k angle) gate creator

This overload of the LibKet::gates::cphasek() function accepts two
expressions as constant reference
*/
template<std::size_t k,
         typename _tol = QConst_t(0.0),
         typename _expr0,
         typename _expr1>
inline constexpr auto
cphasek(const _expr0& expr0, const _expr1& expr1) noexcept
{
  return BinaryQGate<_expr0,
                     _expr1,
                     QCPhaseK<k, _tol>,
                     decltype(typename filters::getFilter<_expr0>::type{}
                              << typename filters::getFilter<_expr1>::type{})>(
    expr0, expr1);
}

/**
@brief LibKet CPHASEK (controlled phase shift with pi/2^k angle) gate creator

This overload of the LibKet::gates::cphasek() function accepts the
first expression as constant reference and the second expression as
universal reference
*/
template<std::size_t k,
         typename _tol = QConst_t(0.0),
         typename _expr0,
         typename _expr1>
inline constexpr auto
cphasek(const _expr0& expr0, _expr1&& expr1) noexcept
{
  return BinaryQGate<_expr0,
                     _expr1,
                     QCPhaseK<k, _tol>,
                     decltype(typename filters::getFilter<_expr0>::type{}
                              << typename filters::getFilter<_expr1>::type{})>(
    expr0, expr1);
}

/**
@brief LibKet CPHASEK (controlled phase shift with pi/2^k angle) gate creator

This overload of the LibKet::gates::cphasek() function accepts the
first expression as universal reference and the second expression as
constant reference
*/
template<std::size_t k,
         typename _tol = QConst_t(0.0),
         typename _expr0,
         typename _expr1>
inline constexpr auto
cphasek(_expr0&& expr0, const _expr1& expr1) noexcept
{
  return BinaryQGate<_expr0,
                     _expr1,
                     QCPhaseK<k, _tol>,
                     decltype(typename filters::getFilter<_expr0>::type{}
                              << typename filters::getFilter<_expr1>::type{})>(
    expr0, expr1);
}

/**
@brief LibKet CPHASEK (controlled phase shift with pi/2^k angle) gate creator

This overload of the LibKet::gates::cphasek() function accepts two
expression as universal reference
*/
template<std::size_t k,
         typename _tol = QConst_t(0.0),
         typename _expr0,
         typename _expr1>
inline constexpr auto
cphasek(_expr0&& expr0, _expr1&& expr1) noexcept
{
  return BinaryQGate<_expr0,
                     _expr1,
                     QCPhaseK<k, _tol>,
                     decltype(typename filters::getFilter<_expr0>::type{}
                              << typename filters::getFilter<_expr1>::type{})>(
    expr0, expr1);
}

/**
@brief LibKet CPHASEK (controlled phase shift with pi/2^k angle) gate creator

Function alias for LibKet::gates::cphasek
*/
template<std::size_t k, typename _tol = QConst_t(0.0), typename... Args>
inline constexpr auto
CPHASEK(Args&&... args)
{
  return cphasek<k, _tol>(std::forward<Args>(args)...);
}

/**
@brief LibKet CPHASEK (controlled phase shift with pi/2^k angle) gate creator

Function alias for LibKet::gates::cphasek
*/
template<std::size_t k, typename _tol = QConst_t(0.0), typename... Args>
inline constexpr auto
crk(Args&&... args)
{
  return cphasek<k, _tol>(std::forward<Args>(args)...);
}

/**
@brief LibKet CPHASEK (controlled phase shift with pi/2^k angle) gate creator

Function alias for LibKet::gates::cphasek
*/
template<std::size_t k, typename _tol = QConst_t(0.0), typename... Args>
inline constexpr auto
CRK(Args&&... args)
{
  return cphasek<k, _tol>(std::forward<Args>(args)...);
}

/// Operator() - by constant reference
template<std::size_t k, typename _tol>
template<typename T0, typename T1>
inline constexpr auto
QCPhaseK<k, _tol>::operator()(const T0& t0, const T1& t1) const noexcept
{
  return cphasek<k, _tol>(std::forward<T0>(t0), std::forward<T1>(t1));
}

/// Operator() - by constant and universal reference
template<std::size_t k, typename _tol>
template<typename T0, typename T1>
inline constexpr auto
QCPhaseK<k, _tol>::operator()(const T0& t0, T1&& t1) const noexcept
{
  return cphasek<k, _tol>(std::forward<T0>(t0), std::forward<T1>(t1));
}

/// Operator() - by universal and constant reference
template<std::size_t k, typename _tol>
template<typename T0, typename T1>
inline constexpr auto
QCPhaseK<k, _tol>::operator()(T0&& t0, const T1& t1) const noexcept
{
  return cphasek<k, _tol>(std::forward<T0>(t0), std::forward<T1>(t1));
}

/// Operator() - by universal reference
template<std::size_t k, typename _tol>
template<typename T0, typename T1>
inline constexpr auto
QCPhaseK<k, _tol>::operator()(T0&& t0, T1&& t1) const noexcept
{
  return cphasek<k, _tol>(std::forward<T0>(t0), std::forward<T1>(t1));
}

/**
   @brief LibKet show gate type - specialization for QCPhaseK objects
*/
template<std::size_t level = 1, std::size_t k, typename _tol>
inline static auto
show(const QCPhaseK<k, _tol>& gate,
     std::ostream& os,
     const std::string& prefix = "")
{
  os << "QCPhaseK pi/2^" << utils::to_string(k) << "\n";

  return gate;
}

} // namespace gates

} // namespace LibKet

#endif // QGATE_CPHASEK_HPP
