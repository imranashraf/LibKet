/** @file libket/gates/QGate_Identity.hpp

    @brief LibKet quantum identity class

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QGATE_IDENTITY_HPP
#define QGATE_IDENTITY_HPP

#include "QGate.hpp"
#include <QData.hpp>
#include <QFilter.hpp>

namespace LibKet {

namespace gates {

/**
@brief LibKet identity gate class

The LibKet identity gate class implements the quantum identity
gate for an arbitrary number of quantum bits
*/

class QIdentity : public QGate
{
public:
  /// Operator() - by constant reference
  template<typename T>
  inline constexpr auto operator()(const T& t) const noexcept;

  /// Operator() - by universal reference
  template<typename T>
  inline constexpr auto operator()(T&& t) const noexcept;

#ifdef LIBKET_WITH_AQASM
  /// Apply function - specialization for AQASM backend
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static typename std::enable_if<_qbackend == QBackend::AQASM,
                                        QData<_qubits, QBackend::AQASM>>::type&
  apply(QData<_qubits, QBackend::AQASM>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("I q[" + utils::to_string(i) + "]\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// Apply function - specialization for cQASMv1 backend
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static
    typename std::enable_if<_qbackend == QBackend::cQASMv1,
                            QData<_qubits, QBackend::cQASMv1>>::type&
    apply(QData<_qubits, QBackend::cQASMv1>& data) noexcept
  {
    std::string _expr = "i q[";
    for (auto i : _filter::range(data))
      _expr += utils::to_string(i) +
               (i != *(_filter::range(data).end() - 1) ? "," : "]\n");
    data.append_kernel(_expr);

    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// Apply function - specialization for OpenQASMv2 backend
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static
    typename std::enable_if<_qbackend == QBackend::OpenQASMv2,
                            QData<_qubits, QBackend::OpenQASMv2>>::type&
    apply(QData<_qubits, QBackend::OpenQASMv2>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("id q[" + utils::to_string(i) + "];\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// Apply function - specialization for OpenQL backend
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static typename std::enable_if<_qbackend == QBackend::OpenQL,
                                        QData<_qubits, QBackend::OpenQL>>::type&
  apply(QData<_qubits, QBackend::OpenQL>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel([&]() { data.kernel().identity(i); });

    return data;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// Apply function - specialization for QASM backend
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static typename std::enable_if<_qbackend == QBackend::QASM,
                                        QData<_qubits, QBackend::QASM>>::type&
  apply(QData<_qubits, QBackend::QASM>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("\ti q" + utils::to_string(i) + "\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// Apply function - specialization for Quil backend
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static typename std::enable_if<_qbackend == QBackend::Quil,
                                        QData<_qubits, QBackend::Quil>>::type&
  apply(QData<_qubits, QBackend::Quil>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("I " + utils::to_string(i) + "\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_QX
  /// Apply function - specialization for QX-simulator backend
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static typename std::enable_if<_qbackend == QBackend::QX,
                                        QData<_qubits, QBackend::QX>>::type&
  apply(QData<_qubits, QBackend::QX>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel(new qx::identity(i));

    return data;
  }
#endif
};

/**
@brief LibKet identity gate creator

This overload of the LibKet::gates::identity() function can be
used as terminal, i.e. the inner-most gate in a quantum
expression

\code
auto expr = gates::identity( );
\endcode
*/
inline constexpr auto
identity() noexcept
{
  return UnaryQGate<filters::QFilter, QIdentity>(filters::QFilter{});
}

#ifdef LIBKET_OPTIMIZE_GATES

/**
@brief LibKet Identity gate creator

This overload of the LibKet::gates::identity() function
eliminates the double-application of the Identity gate
*/
template<typename _expr, typename _filter>
inline constexpr auto
identity(
  const UnaryQGate<_expr, QIdentity, typename filters::getFilter<_expr>::type>&
    expr) noexcept
{
  return expr;
}

/**
@brief LibKet Identity gate creator

This overload of the LibKet::gates::identity() function
eliminates the double-application of the Identity gate
*/
template<typename _expr>
inline constexpr auto
identity(
  UnaryQGate<_expr, QIdentity, typename filters::getFilter<_expr>::type>&&
    expr) noexcept
{
  return expr;
}

#endif // LIBKET_OPTIMIZE_GATES

/**
@brief LibKet identity gate creator

This overload of the LibKet::gates::identity() function accepts
an expression as constant reference
*/
template<typename _expr>
inline constexpr auto
identity(const _expr& expr) noexcept
{
  return UnaryQGate<_expr, QIdentity, typename filters::getFilter<_expr>::type>(
    expr);
}

/**
@brief LibKet identity gate creator

This overload of the LibKet::gates::identity() function accepts
an expression as universal reference
*/
template<typename _expr>
inline constexpr auto
identity(_expr&& expr) noexcept
{
  return UnaryQGate<_expr, QIdentity, typename filters::getFilter<_expr>::type>(
    expr);
}

/**
@brief LibKet identity gate creator

Function alias for LibKet::gates::identity
*/
template<typename... Args>
inline constexpr auto
IDENTITY(Args&&... args)
{
  return identity(std::forward<Args>(args)...);
}

/**
@brief LibKet identity gate creator

Function alias for LibKet::gates::identity
*/
template<typename... Args>
inline constexpr auto
i(Args&&... args)
{
  return identity(std::forward<Args>(args)...);
}

/**
@brief LibKet identity gate creator

Function alias for LibKet::gates::identity
*/
template<typename... Args>
inline constexpr auto
I(Args&&... args)
{
  return identity(std::forward<Args>(args)...);
}

/// Operator() - by constant reference
template<typename T>
inline constexpr auto
QIdentity::operator()(const T& t) const noexcept
{
  return identity(std::forward<T>(t));
}

/// Operator() - by universal reference
template<typename T>
inline constexpr auto
QIdentity::operator()(T&& t) const noexcept
{
  return identity(std::forward<T>(t));
}

/**
   @brief LibKet show gate type - specialization for QIdentity objects
*/
template<std::size_t level = 1>
inline static auto
show(const QIdentity& gate, std::ostream& os, const std::string& prefix = "")
{
  os << "QIdentity\n";

  return gate;
}

} // namespace gates

} // namespace LibKet

#endif // QGATE_IDENTITY_HPP
