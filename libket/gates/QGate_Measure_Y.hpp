/** @file libket/gates/QGate_Measure_Y.hpp

    @brief LibKet quantum Measure_Y class

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QGATE_MEASURE_Y_HPP
#define QGATE_MEASURE_Y_HPP

#include "QGate.hpp"
#include <QData.hpp>
#include <QFilter.hpp>

namespace LibKet {

namespace gates {

/**
@brief LibKet Measure_Y class

The LibKet Measure_Y class implements the measurement of an arbitrary
number of quantum bits in the Z-basis. Striktly speaking, measure_y is
not a quantum gate.
*/

class QMeasure_Y : public QGate
{
public:
  /// Operator() - by constant reference
  template<typename T>
  inline constexpr auto operator()(const T& t) const noexcept;

  /// Operator() - by universal reference
  template<typename T>
  inline constexpr auto operator()(T&& t) const noexcept;

#ifdef LIBKET_WITH_AQASM
  /// Apply function - specialization for AQASM backend
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static typename std::enable_if<_qbackend == QBackend::AQASM,
                                        QData<_qubits, QBackend::AQASM>>::type&
  apply(QData<_qubits, QBackend::AQASM>& data) noexcept
  {
    for (auto i : _filter::range(data)) {
      data.append_kernel("S q[" + utils::to_string(i) + "]\n");
      data.append_kernel("MEAS q[" + utils::to_string(i) + "]\n");
    }

    return data;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// Apply function - specialization for cQASMv1 backend
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static
    typename std::enable_if<_qbackend == QBackend::cQASMv1,
                            QData<_qubits, QBackend::cQASMv1>>::type&
    apply(QData<_qubits, QBackend::cQASMv1>& data) noexcept
  {
    std::string _expr = "measure_y q[";
    for (auto i : _filter::range(data))
      _expr += utils::to_string(i) +
               (i != *(_filter::range(data).end() - 1) ? "," : "]\n");
    data.append_kernel(_expr);

    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// Apply function - specialization for OpenQASMv2 backend
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static
    typename std::enable_if<_qbackend == QBackend::OpenQASMv2,
                            QData<_qubits, QBackend::OpenQASMv2>>::type&
    apply(QData<_qubits, QBackend::OpenQASMv2>& data) noexcept
  {
    for (auto i : _filter::range(data)) {
      data.append_kernel("s q[" + utils::to_string(i) + "];\n");
      data.append_kernel("measure q[" + utils::to_string(i) + "] -> c[" +
                         utils::to_string(i) + "];\n");
    }

    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// Apply function - specialization for OpenQL backend
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static typename std::enable_if<_qbackend == QBackend::OpenQL,
                                        QData<_qubits, QBackend::OpenQL>>::type&
  apply(QData<_qubits, QBackend::OpenQL>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel([&]() { data.kernel().measurey(i); });

    return data;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// Apply function - specialization for QASM backend
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static typename std::enable_if<_qbackend == QBackend::QASM,
                                        QData<_qubits, QBackend::QASM>>::type&
  apply(QData<_qubits, QBackend::QASM>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("\tmeasure_y q" + utils::to_string(i) + "\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// Apply function - specialization for Quil backend
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static typename std::enable_if<_qbackend == QBackend::Quil,
                                        QData<_qubits, QBackend::Quil>>::type&
  apply(QData<_qubits, QBackend::Quil>& data) noexcept
  {
    for (auto i : _filter::range(data)) {
      data.append_kernel("S " + utils::to_string(i) + "]\n");
      data.append_kernel("MEASURE " + utils::to_string(i) + " c[" +
                         utils::to_string(i) + "]\n");
    }

    return data;
  }
#endif

#ifdef LIBKET_WITH_QX
  /// Apply function - specialization for QX-simulator backend
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static typename std::enable_if<_qbackend == QBackend::QX,
                                        QData<_qubits, QBackend::QX>>::type&
  apply(QData<_qubits, QBackend::QX>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel(new qx::measure_y(i));

    return data;
  }
#endif
};

/**
@brief LibKet Measure_Y gate creator

This overload of the LibKet::gates::measure_y() function can be used as
terminal, i.e. the inner-most gate in a quantum expression

\code
auto expr = gates::measure_y();
\endcode
*/
inline constexpr auto
measure_y() noexcept
{
  return UnaryQGate<filters::QFilter, QMeasure_Y>(filters::QFilter{});
}

#ifdef LIBKET_OPTIMIZE_GATES

/**
@brief LibKet Measure_Y gate creator

This overload of the LibKet::gates::measure_y() function eliminates the
double-application of the Measure_Y gate
*/
template<typename _expr, typename _filter>
inline constexpr auto
measure_y(
  const UnaryQGate<_expr, QMeasure_Y, typename filters::getFilter<_expr>::type>&
    expr) noexcept
{
  return expr;
}

/**
@brief LibKet Measure_Y gate creator

This overload of the LibKet::gates::measure_y() function eliminates the
double-application of the Measure_Y gate
*/
template<typename _expr>
inline constexpr auto
measure_y(
  UnaryQGate<_expr, QMeasure_Y, typename filters::getFilter<_expr>::type>&&
    expr) noexcept
{
  return expr;
}

#endif // LIBKET_OPTIMIZE_GATES

/**
@brief LibKet Measure_Y gate creator

This overload of the LibKet::gates::measure_y() function accepts an
expression as constant reference
*/
template<typename _expr>
inline constexpr auto
measure_y(const _expr& expr) noexcept
{
  return UnaryQGate<_expr,
                    QMeasure_Y,
                    typename filters::getFilter<_expr>::type>(expr);
}

/**
@brief LibKet Measure_Y gate creator

This overload of the LibKet::gates::measure_y() function accepts an
expression as universal reference
*/
template<typename _expr>
inline constexpr auto
measure_y(_expr&& expr) noexcept
{
  return UnaryQGate<_expr,
                    QMeasure_Y,
                    typename filters::getFilter<_expr>::type>(expr);
}

/**
@brief LibKet Measure_Y gate creator

Function alias for LibKet::gates::measure_y
*/
template<typename... Args>
inline constexpr auto
MEASURE_Y(Args&&... args)
{
  return measure_y(std::forward<Args>(args)...);
}

/// Operator() - by constant reference
template<typename T>
inline constexpr auto
QMeasure_Y::operator()(const T& t) const noexcept
{
  return measure_y(std::forward<T>(t));
}

/// Operator() - by universal reference
template<typename T>
inline constexpr auto
QMeasure_Y::operator()(T&& t) const noexcept
{
  return measure_y(std::forward<T>(t));
}

/**
   @brief LibKet show gate type - specialization for QMeasure_Y objects
*/
template<std::size_t level = 1>
inline static auto
show(const QMeasure_Y& gate, std::ostream& os, const std::string& prefix = "")
{
  os << "QMeasure_Y\n";

  return gate;
}

} // namespace gates

} // namespace LibKet

#endif // QGATE_MEASURE_Y_HPP
