/** @file libket/gates/QGate_T.hpp

    @brief LibKet quantum T gate class

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QGATE_T_HPP
#define QGATE_T_HPP

#include "QGate.hpp"
#include <QData.hpp>
#include <QFilter.hpp>

namespace LibKet {

namespace gates {

// Forward declaration
class QTdag;

/**
@brief LibKet T gate class

The LibKet T gate class implements the quantum T gate
gate for an arbitrary number of quantum bits
*/

class QT : public QGate
{
public:
  /// Operator() - by constant reference
  template<typename T>
  inline constexpr auto operator()(const T& t) const noexcept;

  /// Operator() - by universal reference
  template<typename T>
  inline constexpr auto operator()(T&& t) const noexcept;

#ifdef LIBKET_WITH_AQASM
  /// Apply function - specialization for AQASM backend
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static typename std::enable_if<_qbackend == QBackend::AQASM,
                                        QData<_qubits, QBackend::AQASM>>::type&
  apply(QData<_qubits, QBackend::AQASM>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("T q[" + utils::to_string(i) + "]\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// Apply function - specialization for cQASMv1 backend
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static
    typename std::enable_if<_qbackend == QBackend::cQASMv1,
                            QData<_qubits, QBackend::cQASMv1>>::type&
    apply(QData<_qubits, QBackend::cQASMv1>& data) noexcept
  {
    std::string _expr = "t q[";
    for (auto i : _filter::range(data))
      _expr += utils::to_string(i) +
               (i != *(_filter::range(data).end() - 1) ? "," : "]\n");
    data.append_kernel(_expr);

    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// Apply function - specialization for OpenQASMv2 backend
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static
    typename std::enable_if<_qbackend == QBackend::OpenQASMv2,
                            QData<_qubits, QBackend::OpenQASMv2>>::type&
    apply(QData<_qubits, QBackend::OpenQASMv2>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("t q[" + utils::to_string(i) + "];\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// Apply function - specialization for OpenQL backend
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static typename std::enable_if<_qbackend == QBackend::OpenQL,
                                        QData<_qubits, QBackend::OpenQL>>::type&
  apply(QData<_qubits, QBackend::OpenQL>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel([&]() { data.kernel().t(i); });

    return data;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// Apply function - specialization for QASM backend
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static typename std::enable_if<_qbackend == QBackend::QASM,
                                        QData<_qubits, QBackend::QASM>>::type&
  apply(QData<_qubits, QBackend::QASM>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("\tt q" + utils::to_string(i) + "\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// Apply function - specialization for Quil backend
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static typename std::enable_if<_qbackend == QBackend::Quil,
                                        QData<_qubits, QBackend::Quil>>::type&
  apply(QData<_qubits, QBackend::Quil>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("T " + utils::to_string(i) + "\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_QX
  /// Apply function - specialization for QX-simulator backend
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static typename std::enable_if<_qbackend == QBackend::QX,
                                        QData<_qubits, QBackend::QX>>::type&
  apply(QData<_qubits, QBackend::QX>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel(new qx::phase_shift(i));

    return data;
  }
#endif
};

/**
@brief LibKet T gate creator

This overload of the LibKet::gate:gate_t() function can be
used as terminal, i.e. the inner-most gate in a quantum
expression

\code
auto expr = gates::t();
\endcode
*/
inline constexpr auto
t() noexcept
{
  return UnaryQGate<filters::QFilter, QT>(filters::QFilter{});
}

#ifdef LIBKET_OPTIMIZE_GATES

/**
@brief LibKet T gate creator

This overload of the LibKet::gate:gate_t() function eliminates
the application of the T gate to its adjoint, the T dagger gate
*/
template<typename _expr, typename _filter>
inline constexpr auto
t(const UnaryQGate<_expr, QTdag, typename filters::getFilter<_expr>::type>&
    expr) noexcept
{
  return expr.expr;
}

/**
@brief LibKet T gate creator

This overload of the LibKet::gate:gate_t() function eliminates
the application of the T gate to its adjoint, the T dagger gate
*/
template<typename _expr>
inline constexpr auto
t(UnaryQGate<_expr, QTdag, typename filters::getFilter<_expr>::type>&&
    expr) noexcept
{
  return expr.expr;
}

#endif // LIBKET_OPTIMIZE_GATES

/**
@brief LibKet T gate creator

This overload of the LibKet::gates::t() function accepts
an expression as constant reference
*/
template<typename _expr>
inline constexpr auto
t(const _expr& expr) noexcept
{
  return UnaryQGate<_expr, QT, typename filters::getFilter<_expr>::type>(expr);
}

/**
@brief LibKet T gate creator

This overload of the LibKet::gates::t() function accepts
an expression as universal reference
*/
template<typename _expr>
inline constexpr auto
t(_expr&& expr) noexcept
{
  return UnaryQGate<_expr, QT, typename filters::getFilter<_expr>::type>(expr);
}

/**
@brief LibKet T gate creator

Function alias for LibKet::gates::t()
*/
template<typename... Args>
inline constexpr auto
T(Args&&... args)
{
  return LibKet::gates::t(std::forward<Args>(args)...);
}

/// Operator() - by constant reference
template<typename T>
inline constexpr auto
QT::operator()(const T& t) const noexcept
{
  return LibKet::gates::t(std::forward<T>(t));
}

/// Operator() - by universal reference
template<typename T>
inline constexpr auto
QT::operator()(T&& t) const noexcept
{
  return LibKet::gates::t(std::forward<T>(t));
}

/**
   @brief LibKet show gate type - specialization for QT objects
*/
template<std::size_t level = 1>
inline static auto
show(const QT& gate, std::ostream& os, const std::string& prefix = "")
{
  os << "QT\n";

  return gate;
}

} // namespace gates

} // namespace LibKet

#endif // QGATE_T_HPP
