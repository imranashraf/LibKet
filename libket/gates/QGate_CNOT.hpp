/** @file libket/gates/QGate_CNOT.hpp

    @brief LibKet quantum CNOT (controlled-X) class

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QGATE_CNOT_HPP
#define QGATE_CNOT_HPP

#include "QGate.hpp"
#include <QData.hpp>
#include <QFilter.hpp>

namespace LibKet {

namespace gates {

/**
@brief LibKet CNOT (controlled-X) gate class

The LibKet CNOT (controlled-X) gate class implements the quantum CNOT
gate for an arbitrary number of quantum bits
*/

class QCNOT : public QGate
{
public:
  /// Operator() - by constant reference
  template<typename T0, typename T1>
  inline constexpr auto operator()(const T0& t0, const T1& t1) const noexcept;

  /// Operator() - by constant and universal reference
  template<typename T0, typename T1>
  inline constexpr auto operator()(const T0& t0, T1&& t1) const noexcept;

  /// Operator() - by universal and constant reference
  template<typename T0, typename T1>
  inline constexpr auto operator()(T0&& t0, const T1& t1) const noexcept;

  /// Operator() - by universal reference
  template<typename T0, typename T1>
  inline constexpr auto operator()(T0&& t0, T1&& t1) const noexcept;

#ifdef LIBKET_WITH_AQASM
  /// Apply function - specialization for AQASM backend
  template<std::size_t _qubits,
           QBackend _qbackend,
           typename _filter0,
           typename _filter1>
  inline static typename std::enable_if<_qbackend == QBackend::AQASM,
                                        QData<_qubits, QBackend::AQASM>>::type&
  apply(QData<_qubits, QBackend::AQASM>& data) noexcept
  {
    static_assert(_filter0::template size<_qubits> ==
                    _filter1::template size<_qubits>,
                  "CNOT (controlled-X) gate can only be applied to quantum "
                  "objects of the same size");
    auto it = _filter1::range(data).begin();
    for (auto i : _filter0::range(data))
      data.append_kernel("CNOT q[" + utils::to_string(i) + "],q[" +
                         utils::to_string(*(it++)) + "]\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// Apply function - specialization for cQASMv1 backend
  template<std::size_t _qubits,
           QBackend _qbackend,
           typename _filter0,
           typename _filter1>
  inline static
    typename std::enable_if<_qbackend == QBackend::cQASMv1,
                            QData<_qubits, QBackend::cQASMv1>>::type&
    apply(QData<_qubits, QBackend::cQASMv1>& data) noexcept
  {
    static_assert(_filter0::template size<_qubits> ==
                    _filter1::template size<_qubits>,
                  "CNOT (controlled-X) gate can only be applied to quantum "
                  "objects of the same size");
    std::string _expr = "cnot q[";
    for (auto i : _filter0::range(data))
      _expr += utils::to_string(i) +
               (i != *(_filter0::range(data).end() - 1) ? "," : "], q[");
    for (auto i : _filter1::range(data))
      _expr += utils::to_string(i) +
               (i != *(_filter1::range(data).end() - 1) ? "," : "]\n");
    data.append_kernel(_expr);

    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// Apply function - specialization for OpenQASMv2 backend
  template<std::size_t _qubits,
           QBackend _qbackend,
           typename _filter0,
           typename _filter1>
  inline static
    typename std::enable_if<_qbackend == QBackend::OpenQASMv2,
                            QData<_qubits, QBackend::OpenQASMv2>>::type&
    apply(QData<_qubits, QBackend::OpenQASMv2>& data) noexcept
  {
    static_assert(_filter0::template size<_qubits> ==
                    _filter1::template size<_qubits>,
                  "CNOT (controlled-X) gate can only be applied to quantum "
                  "objects of the same size");
    auto it = _filter1::range(data).begin();
    for (auto i : _filter0::range(data))
      data.append_kernel("cx q[" + utils::to_string(i) + "], q[" +
                         utils::to_string(*(it++)) + "];\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// Apply function - specialization for OpenQL backend
  template<std::size_t _qubits,
           QBackend _qbackend,
           typename _filter0,
           typename _filter1>
  inline static typename std::enable_if<_qbackend == QBackend::OpenQL,
                                        QData<_qubits, QBackend::OpenQL>>::type&
  apply(QData<_qubits, QBackend::OpenQL>& data) noexcept
  {
    static_assert(_filter0::template size<_qubits> ==
                    _filter1::template size<_qubits>,
                  "CNOT (controlled-X) gate can only be applied to quantum "
                  "objects of the same size");
    auto it = _filter1::range(data).begin();
    for (auto i : _filter0::range(data))
      data.append_kernel([&]() { data.kernel().cnot(i, *(it++)); });

    return data;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// Apply function - specialization for QASM backend
  template<std::size_t _qubits,
           QBackend _qbackend,
           typename _filter0,
           typename _filter1>
  inline static typename std::enable_if<_qbackend == QBackend::QASM,
                                        QData<_qubits, QBackend::QASM>>::type&
  apply(QData<_qubits, QBackend::QASM>& data) noexcept
  {
    static_assert(_filter0::template size<_qubits> ==
                    _filter1::template size<_qubits>,
                  "CNOT (controlled-X) gate can only be applied to quantum "
                  "objects of the same size");
    auto it = _filter1::range(data).begin();
    for (auto i : _filter0::range(data))
      data.append_kernel("\tcnot q" + utils::to_string(i) + ",q" +
                         utils::to_string(*(it++)) + "\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// Apply function - specialization for Quil backend
  template<std::size_t _qubits,
           QBackend _qbackend,
           typename _filter0,
           typename _filter1>
  inline static typename std::enable_if<_qbackend == QBackend::Quil,
                                        QData<_qubits, QBackend::Quil>>::type&
  apply(QData<_qubits, QBackend::Quil>& data) noexcept
  {
    static_assert(_filter0::template size<_qubits> ==
                    _filter1::template size<_qubits>,
                  "CNOT (controlled-X) gate can only be applied to quantum "
                  "objects of the same size");
    auto it = _filter1::range(data).begin();
    for (auto i : _filter0::range(data))
      data.append_kernel("CNOT " + utils::to_string(i) + " " +
                         utils::to_string(*(it++)) + "\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_QX
  /// Apply function - specialization for QX-simulator backend
  template<std::size_t _qubits,
           QBackend _qbackend,
           typename _filter0,
           typename _filter1>
  inline static typename std::enable_if<_qbackend == QBackend::QX,
                                        QData<_qubits, QBackend::QX>>::type&
  apply(QData<_qubits, QBackend::QX>& data) noexcept
  {
    static_assert(_filter0::template size<_qubits> ==
                    _filter1::template size<_qubits>,
                  "CNOT (controlled-X) gate can only be applied to quantum "
                  "objects of the same size");
    auto it = _filter1::range(data).begin();
    for (auto i : _filter0::range(data))
      data.append_kernel(new qx::cnot(i, *(it++)));

    return data;
  }
#endif
};

/**
@brief LibKet CNOT (controlled-X) gate creator

This overload of the LibKet::gates::CNOT() function can be used as
terminal, i.e. the inner-most gate in a quantum expression

\code
auto expr = gates::cnot();
\endcode
*/
inline constexpr auto
cnot() noexcept
{
  return BinaryQGate<filters::QFilter, filters::QFilter, QCNOT>(
    filters::QFilter{}, filters::QFilter{});
}

#ifdef LIBKET_OPTIMIZE_GATES

/**
@brief LibKet CNOT (controlled-X) gate creator

This overload of the LibKet::gates::cnot() function eliminates the
double-application of the CNOT (controlled-X) gate
*/
template<typename _expr, typename _filter>
inline constexpr auto
cnot(const UnaryQGate<_expr, QCNOT, typename filters::getFilter<_expr>::type>&
       expr) noexcept
{
  return expr.expr;
}

/**
@brief LibKet CNOT (controlled-X) gate creator

This overload of the LibKet::gates::cnot() function eliminates the
double-application of the CNOT (controlled-X) gate
*/
template<typename _expr>
inline constexpr auto
cnot(UnaryQGate<_expr, QCNOT, typename filters::getFilter<_expr>::type>&&
       expr) noexcept
{
  return expr.expr;
}

#endif // LIBKET_OPTIMIZE_GATES

/**
@brief LibKet CNOT (controlled-X) gate creator

This overload of the LibKet::gates::cnot() function accepts two
expressions as constant reference
*/
template<typename _expr0, typename _expr1>
inline constexpr auto
cnot(const _expr0& expr0, const _expr1& expr1) noexcept
{
  return BinaryQGate<_expr0,
                     _expr1,
                     QCNOT,
                     decltype(typename filters::getFilter<_expr0>::type{}
                              << typename filters::getFilter<_expr1>::type{})>(
    expr0, expr1);
}

/**
@brief LibKet CNOT (controlled-X) gate creator

This overload of the LibKet::gates::cnot() function accepts the first
expression as constant reference and the second expression as
universal reference
*/
template<typename _expr0, typename _expr1>
inline constexpr auto
cnot(const _expr0& expr0, _expr1&& expr1) noexcept
{
  return BinaryQGate<_expr0,
                     _expr1,
                     QCNOT,
                     decltype(typename filters::getFilter<_expr0>::type{}
                              << typename filters::getFilter<_expr1>::type{})>(
    expr0, expr1);
}

/**
@brief LibKet CNOT (controlled-X) gate creator

This overload of the LibKet::gates::cnot() function accepts the first
expression as universal reference and the second expression as
constant reference
*/
template<typename _expr0, typename _expr1>
inline constexpr auto
cnot(_expr0&& expr0, const _expr1& expr1) noexcept
{
  return BinaryQGate<_expr0,
                     _expr1,
                     QCNOT,
                     decltype(typename filters::getFilter<_expr0>::type{}
                              << typename filters::getFilter<_expr1>::type{})>(
    expr0, expr1);
}

/**
@brief LibKet CNOT (controlled-X) gate creator

This overload of the LibKet::gates::cnot() function accepts two
expression as universal reference
*/
template<typename _expr0, typename _expr1>
inline constexpr auto
cnot(_expr0&& expr0, _expr1&& expr1) noexcept
{
  return BinaryQGate<_expr0,
                     _expr1,
                     QCNOT,
                     decltype(typename filters::getFilter<_expr0>::type{}
                              << typename filters::getFilter<_expr1>::type{})>(
    expr0, expr1);
}

/**
@brief LibKet CNOT (controlled-X) gate creator

Function alias for LibKet::gates::cnot
*/
template<typename... Args>
inline constexpr auto
CNOT(Args&&... args)
{
  return cnot(std::forward<Args>(args)...);
}

/**
@brief LibKet CNOT (controlled-X) gate creator

Function alias for LibKet::gates::cnot
*/
template<typename... Args>
inline constexpr auto
CX(Args&&... args)
{
  return cnot(std::forward<Args>(args)...);
}

/**
@brief LibKet CNOT (controlled-X) gate creator

Function alias for LibKet::gates::cnot
*/
template<typename... Args>
inline constexpr auto
cx(Args&&... args)
{
  return cnot(std::forward<Args>(args)...);
}

/**
@brief LibKet CNOT (controlled-X) gate creator

Function alias for LibKet::gates::cnot
*/
template<typename... Args>
inline constexpr auto
cnotdag(Args&&... args)
{
  return cnot(std::forward<Args>(args)...);
}

/**
@brief LibKet CNOT (controlled-X) gate creator

Function alias for LibKet::gates::cnot
*/
template<typename... Args>
inline constexpr auto
CNOTdag(Args&&... args)
{
  return cnot(std::forward<Args>(args)...);
}

/**
@brief LibKet CNOT (controlled-X) gate creator

Function alias for LibKet::gates::cnot
*/
template<typename... Args>
inline constexpr auto
CXdag(Args&&... args)
{
  return cnot(std::forward<Args>(args)...);
}

/**
@brief LibKet CNOT (controlled-X) gate creator

Function alias for LibKet::gates::cnot
*/
template<typename... Args>
inline constexpr auto
cxdag(Args&&... args)
{
  return cnot(std::forward<Args>(args)...);
}

/// Operator() - by constant reference?
template<typename T0, typename T1>
inline constexpr auto
QCNOT::operator()(const T0& t0, const T1& t1) const noexcept
{
  return cnot(std::forward<T0>(t0), std::forward<T1>(t1));
}

/// Operator() - by constant and universal reference
template<typename T0, typename T1>
inline constexpr auto
QCNOT::operator()(const T0& t0, T1&& t1) const noexcept
{
  return cnot(std::forward<T0>(t0), std::forward<T1>(t1));
}

/// Operator() - by universal and constant reference
template<typename T0, typename T1>
inline constexpr auto
QCNOT::operator()(T0&& t0, const T1& t1) const noexcept
{
  return cnot(std::forward<T0>(t0), std::forward<T1>(t1));
}

/// Operator() - by universal reference
template<typename T0, typename T1>
inline constexpr auto
QCNOT::operator()(T0&& t0, T1&& t1) const noexcept
{
  return cnot(std::forward<T0>(t0), std::forward<T1>(t1));
}

/**
   @brief LibKet show gate type - specialization for QCNOT objects
*/
template<std::size_t level = 1>
inline static auto
show(const QCNOT& gate, std::ostream& os, const std::string& prefix = "")
{
  os << "QCNOT\n";

  return gate;
}

} // namespace gates

} // namespace LibKet

#endif // QGATE_CNOT_HPP
