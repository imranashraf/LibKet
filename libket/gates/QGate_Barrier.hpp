/** @file libket/gates/QGate_Barrier.hpp

    @brief LibKet quantum synchronization gate class

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QGATE_BARRIER_HPP
#define QGATE_BARRIER_HPP

#include "QGate.hpp"
#include <QData.hpp>
#include <QFilter.hpp>

namespace LibKet {

namespace gates {

/**
@brief LibKet quantum synchronization gate class

The quantum synchronization gate ensures that quantum expression
optimization does not proceed beyond the synchronization barrier.

The following code creates a quantum expression consisting of
three gadamard gates applied consecutively, which by the built-in
optimization of quantum expressions yields a single Hadamard gate

\code
auto expr = gates::hadamard( gates::hadamard( gates::hadamard() ) ); // same as
autoexpr = gates::hadamard(); \endcode

To prevent the built-in optimization from taking place, the
quantum synchronization gate can be inserted between the gates

\code
auto expr = gates::hadamard( gates::barrier ( gates::hadamard( gates::barrier (
gates::hadamard() ) ) ) ); \endcode
*/
class QBarrier : public QGate
{
public:
  /// Operator() - by constant reference
  template<typename T>
  inline constexpr auto operator()(const T& t) const noexcept;

  /// Operator() - by universal reference
  template<typename T>
  inline constexpr auto operator()(T&& t) const noexcept;

#ifdef LIBKET_WITH_AQASM
  /// Apply function - specialization for AQASM backend
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static typename std::enable_if<_qbackend == QBackend::AQASM,
                                        QData<_qubits, QBackend::AQASM>>::type&
  apply(QData<_qubits, QBackend::AQASM>& data) noexcept
  {
    return data;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// Apply function - specialization for cQASMv1 backend
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static
    typename std::enable_if<_qbackend == QBackend::cQASMv1,
                            QData<_qubits, QBackend::cQASMv1>>::type&
    apply(QData<_qubits, QBackend::cQASMv1>& data) noexcept
  {
    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// Apply function - specialization for OpenQASMv2 backend
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static
    typename std::enable_if<_qbackend == QBackend::OpenQASMv2,
                            QData<_qubits, QBackend::OpenQASMv2>>::type&
    apply(QData<_qubits, QBackend::OpenQASMv2>& data) noexcept
  {
    data.append_kernel("barrier q;");

    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// Apply function - specialization for OpenQL backend
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static typename std::enable_if<_qbackend == QBackend::OpenQL,
                                        QData<_qubits, QBackend::OpenQL>>::type&
  apply(QData<_qubits, QBackend::OpenQL>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel(new ql::wait(i, 0, 0));

    return data;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// Apply function - specialization for QASM backend
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static typename std::enable_if<_qbackend == QBackend::QASM,
                                        QData<_qubits, QBackend::QASM>>::type&
  apply(QData<_qubits, QBackend::QASM>& data) noexcept
  {
    return data;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// Apply function - specialization for Quil backend
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static typename std::enable_if<_qbackend == QBackend::Quil,
                                        QData<_qubits, QBackend::Quil>>::type&
  apply(QData<_qubits, QBackend::Quil>& data) noexcept
  {
    return data;
  }
#endif

#ifdef LIBKET_WITH_QX
  /// Apply function - specialization for QX-simulator backend
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static typename std::enable_if<_qbackend == QBackend::QX,
                                        QData<_qubits, QBackend::QX>>::type&
  apply(QData<_qubits, QBackend::QX>& data) noexcept
  {
    // for (auto i : _filter::range(data))
    // data.append_kernel(new qx::wait(i));

    return data;
  }
#endif
};

/**
@brief LibKet quantum synchronization gate creator

This overload of the LibKet::gates::barrier() function can be used
as terminal, i.e. the inner-most gate in a quantum expression

\code
auto expr1 = gates::hadamard( gates::barrier() );
\endcode

If the so-defined expression is used as sub-expression then the
synchronization gate ensures that no optimization takes place

\code
auto expr2 = expr1( gates::hadamard() );
\endcode
*/
inline constexpr auto
barrier() noexcept
{
  return UnaryQGate<filters::QFilter, QBarrier, filters::QFilter>(
    filters::QFilter{});
}

/**
@brief LibKet quantum synchronization gate creator

This overload of the LibKet::gates::barrier() function accepts an
expression as constant reference
*/
template<typename _expr>
inline constexpr auto
barrier(const _expr& expr) noexcept
{
  return UnaryQGate<_expr, QBarrier, typename filters::getFilter<_expr>::type>(
    expr);
}

/**
@brief LibKet quantum synchronization gate creator

This overload of the LibKet::gates::barrier() function accepts an
expression as universal reference
*/
template<typename _expr>
inline constexpr auto
barrier(_expr&& expr) noexcept
{
  return UnaryQGate<_expr, QBarrier, typename filters::getFilter<_expr>::type>(
    expr);
}

/**
@brief LibKet quantum synchronization gate creator

Function alias for LibKet::gates::barrier
*/
template<typename... Args>
inline constexpr auto
BARRIER(Args&&... args)
{
  return barrier(std::forward<Args>(args)...);
}

/// Operator() - by constant reference
template<typename T>
inline constexpr auto
QBarrier::operator()(const T& t) const noexcept
{
  return barrier(std::forward<T>(t));
}

/// Operator() - by universal reference
template<typename T>
inline constexpr auto
QBarrier::operator()(T&& t) const noexcept
{
  return barrier(std::forward<T>(t));
}

/**
   @brief LibKet show gate type - specialization for QBarrier objects
*/
template<std::size_t level = 1>
inline static auto
show(const QBarrier& gate, std::ostream& os, const std::string& prefix = "")
{
  os << "QBarrier\n";

  return gate;
}

} // namespace gates

} // namespace LibKet

#endif // QGATE_BARRIER_HPP
