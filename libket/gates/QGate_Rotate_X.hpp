/** @file libket/gates/QGate_Rotate_X.hpp

    @brief LibKet quantum Rotate_X class

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QGATE_ROTATE_X_HPP
#define QGATE_ROTATE_X_HPP

#include "QGate.hpp"
#include <QData.hpp>
#include <QFilter.hpp>

namespace LibKet {

namespace gates {

/**
@brief LibKet Rotate_X gate class

The LibKet Rotate_X gate class implements the quantum Rotate_X
gate for an arbitrary number of quantum bits
*/

template<typename _angle, typename _tol = QConst_t(0.0)>
class QRotate_X : public QGate
{
public:
  /// Rotation angle
  using angle = typename std::decay<_angle>::type;

  /// Operator() - by constant reference
  template<typename T>
  inline constexpr auto operator()(const T& t) const noexcept;

  /// Operator() - by universal reference
  template<typename T>
  inline constexpr auto operator()(T&& t) const noexcept;

#ifdef LIBKET_WITH_AQASM
  /// Apply function - specialization for AQASM backend
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static typename std::enable_if<_qbackend == QBackend::AQASM,
                                        QData<_qubits, QBackend::AQASM>>::type&
  apply(QData<_qubits, QBackend::AQASM>& data) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(data))
        data.append_kernel("RX[" + _angle::str() + "] q[" +
                           utils::to_string(i) + "]\n");
    }
    return data;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// Apply function - specialization for cQASMv1 backend
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static
    typename std::enable_if<_qbackend == QBackend::cQASMv1,
                            QData<_qubits, QBackend::cQASMv1>>::type&
    apply(QData<_qubits, QBackend::cQASMv1>& data) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      std::string _expr = "rx q[";
      for (auto i : _filter::range(data))
        _expr += utils::to_string(i) +
                 (i != *(_filter::range(data).end() - 1) ? "," : "], ");
      _expr += _angle::str() + "\n";
      data.append_kernel(_expr);
    }
    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// Apply function - specialization for OpenQASMv2 backend
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static
    typename std::enable_if<_qbackend == QBackend::OpenQASMv2,
                            QData<_qubits, QBackend::OpenQASMv2>>::type&
    apply(QData<_qubits, QBackend::OpenQASMv2>& data) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(data))
        data.append_kernel("rx(" + _angle::str() + ") q[" +
                           utils::to_string(i) + "];\n");
    }
    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// Apply function - specialization for OpenQL backend
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static typename std::enable_if<_qbackend == QBackend::OpenQL,
                                        QData<_qubits, QBackend::OpenQL>>::type&
  apply(QData<_qubits, QBackend::OpenQL>& data) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(data))
        data.append_kernel([&]() { data.kernel().rx(i, _angle::value()); });
    }
    return data;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// Apply function - specialization for QASM backend
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static typename std::enable_if<_qbackend == QBackend::QASM,
                                        QData<_qubits, QBackend::QASM>>::type&
  apply(QData<_qubits, QBackend::QASM>& data) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(data))
        data.append_kernel("\trx q" + utils::to_string(i) + "\n");
    }
    return data;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// Apply function - specialization for Quil backend
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static typename std::enable_if<_qbackend == QBackend::Quil,
                                        QData<_qubits, QBackend::Quil>>::type&
  apply(QData<_qubits, QBackend::Quil>& data) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(data))
        data.append_kernel("RX (" + _angle::str() + ") " + utils::to_string(i) +
                           "\n");
    }
    return data;
  }
#endif

#ifdef LIBKET_WITH_QX
  /// Apply function - specialization for QX-simulator backend
  template<std::size_t _qubits, QBackend _qbackend, typename _filter>
  inline static typename std::enable_if<_qbackend == QBackend::QX,
                                        QData<_qubits, QBackend::QX>>::type&
  apply(QData<_qubits, QBackend::QX>& data) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(data))
        data.append_kernel(new qx::rx(i, _angle::value()));
    }
    return data;
  }
#endif
};

/**
@brief LibKet Rotate_X gate creator

This overload of the LibKet::gates::rotate_x() function can be
used as terminal, i.e. the inner-most gate in a quantum
expression

\code
auto expr = gates::rotate_x();
\endcode
*/
template<typename _tol = QConst_t(0.0), typename _angle>
inline constexpr auto rotate_x(_angle) noexcept
{
  return UnaryQGate<filters::QFilter, QRotate_X<_angle, _tol>>(
    filters::QFilter{});
}

#ifdef LIBKET_OPTIMIZE_GATES

/**
@brief LibKet Rotate_X gate creator
*/
template<typename _tol = QConst_t(0.0),
         typename _angle,
         typename _expr,
         typename _filter>
inline constexpr auto
rotate_x(
  _angle,
  const UnaryQGate<_expr,
                   QRotate_X<_angle, _tol>,
                   typename filters::getFilter<_expr>::type>& expr) noexcept
{
  return expr.expr;
}

/**
@brief LibKet Rotate_X gate creator
*/
template<typename _tol = QConst_t(0.0), typename _angle, typename _expr>
inline constexpr auto
rotate_x(_angle,
         UnaryQGate<_expr,
                    QRotate_X<_angle, _tol>,
                    typename filters::getFilter<_expr>::type>&& expr) noexcept
{
  return expr.expr;
}

#endif // LIBKET_OPTIMIZE_GATES

/**
@brief LibKet Rotate_X gate creator

This overload of the LibKet::gates::rotate_x() function accepts
an expression as constant reference
*/
template<typename _tol = QConst_t(0.0), typename _angle, typename _expr>
inline constexpr auto
rotate_x(_angle, const _expr& expr) noexcept
{
  return UnaryQGate<_expr,
                    QRotate_X<_angle, _tol>,
                    typename filters::getFilter<_expr>::type>(expr);
}

/**
@brief LibKet Rotate_X gate creator

This overload of the LibKet::gates::rotate_x() function accepts
an expression as universal reference
*/
template<typename _tol = QConst_t(0.0), typename _angle, typename _expr>
inline constexpr auto
rotate_x(_angle, _expr&& expr) noexcept
{
  return UnaryQGate<_expr,
                    QRotate_X<_angle, _tol>,
                    typename filters::getFilter<_expr>::type>(expr);
}

/**
@brief LibKet Rotate_X gate creator

Function alias for LibKet::gates::rotate_x
*/
template<typename _tol = QConst_t(0.0), typename... Args>
inline constexpr auto
ROTATE_X(Args&&... args)
{
  return rotate_x<_tol>(std::forward<Args>(args)...);
}

/**
@brief LibKet Rotate_X gate creator

Function alias for LibKet::gates::rotate_x
*/
template<typename _tol = QConst_t(0.0), typename... Args>
inline constexpr auto
rx(Args&&... args)
{
  return rotate_x<_tol>(std::forward<Args>(args)...);
}

/**
@brief LibKet Rotate_X gate creator

Function alias for LibKet::gates::rotate_x
*/
template<typename _tol = QConst_t(0.0), typename... Args>
inline constexpr auto
RX(Args&&... args)
{
  return rotate_x<_tol>(std::forward<Args>(args)...);
}

/**
@brief LibKet Rotate_X gate creator

Function alias for LibKet::gates::rotate_x
*/
template<typename _tol = QConst_t(0.0), typename... Args>
inline constexpr auto
Rx(Args&&... args)
{
  return rotate_x<_tol>(std::forward<Args>(args)...);
}

/// Operator() - by constant reference
template<typename _angle, typename _tol>
template<typename T>
inline constexpr auto
QRotate_X<_angle, _tol>::operator()(const T& t) const noexcept
{
  return rotate_x<_tol>(_angle{}, std::forward<T>(t));
}

/// Operator() - by universal reference
template<typename _angle, typename _tol>
template<typename T>
inline constexpr auto
QRotate_X<_angle, _tol>::operator()(T&& t) const noexcept
{
  return rotate_x<_tol>(_angle{}, std::forward<T>(t));
}

/**
   @brief LibKet show gate type - specialization for QRotate_X objects
*/
template<std::size_t level = 1, typename _angle, typename _tol>
inline static auto
show(const QRotate_X<_angle, _tol>& gate,
     std::ostream& os,
     const std::string& prefix = "")
{
  os << "QRotate_X " << _angle::str() << "\n";

  return gate;
}

} // namespace gates

} // namespace LibKet

#endif // QGATE_ROTATE_X_HPP
