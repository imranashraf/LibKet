/** @file libket/gates/QGates.hpp

    @brief LibKet gates header file

    @copyright This file is part of the LibKet project

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */
#pragma once
#ifndef GATES_HPP
#define GATES_HPP

#include "QGate.hpp"
#include "QGate_Barrier.hpp"
#include "QGate_CCNOT.hpp"
#include "QGate_CNOT.hpp"
#include "QGate_CPhase.hpp"
#include "QGate_CPhaseK.hpp"
#include "QGate_CPhaseKdag.hpp"
#include "QGate_CPhasedag.hpp"
#include "QGate_CY.hpp"
#include "QGate_CZ.hpp"
#include "QGate_Hadamard.hpp"
#include "QGate_Identity.hpp"
#include "QGate_Init.hpp"
#include "QGate_Measure.hpp"
#include "QGate_Measure_X.hpp"
#include "QGate_Measure_Y.hpp"
#include "QGate_Measure_Z.hpp"
#include "QGate_Pauli_X.hpp"
#include "QGate_Pauli_Y.hpp"
#include "QGate_Pauli_Z.hpp"
#include "QGate_Prep_X.hpp"
#include "QGate_Prep_Y.hpp"
#include "QGate_Prep_Z.hpp"
#include "QGate_Reset.hpp"
#include "QGate_Rotate_MX90.hpp"
#include "QGate_Rotate_MY90.hpp"
#include "QGate_Rotate_X.hpp"
#include "QGate_Rotate_X90.hpp"
#include "QGate_Rotate_Xdag.hpp"
#include "QGate_Rotate_Y.hpp"
#include "QGate_Rotate_Y90.hpp"
#include "QGate_Rotate_Ydag.hpp"
#include "QGate_Rotate_Z.hpp"
#include "QGate_Rotate_Zdag.hpp"
#include "QGate_S.hpp"
#include "QGate_Sdag.hpp"
#include "QGate_Swap.hpp"
#include "QGate_T.hpp"
#include "QGate_Tdag.hpp"

#endif // GATES_HPP
