/** @file libket/QBase.hpp

@brief LibKet quantum base classes and declarations

@copyright This file is part of the LibKet library

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

@author Matthias Moller
*/

#pragma once
#ifndef QBASE_HPP
#define QBASE_HPP

#include <iostream>

#define _USE_MATH_DEFINES
#include <math.h>

#include <QConfig.h>

#ifdef LIBKET_WITH_OPENQL
#include <ql/openql.h>
#endif

#ifdef LIBKET_WITH_QX
#include <core/circuit.h>
#include <core/error_model.h>
#include <xpu.h>
#include <xpu/runtime>
#endif

#include "static_for/static_for.h"
#include "json/json.hpp"

#include "QStream.hpp"

/**
   @namespace LibKet

   @brief
   The LibKet namespace, containing all functionality of the LibKet project

   The LibKet namespace contains all functionality of the LibKet
   project that is exposed to the end-user. All functionality in this
   namespace has a stable API over future LibKet releases.
 */
namespace LibKet {

/**
   @brief LibKet quantum backends

   The LibKet quantum backend enumerator defines the supported quantum backends
*/
enum class QBackend
{
#ifdef LIBKET_WITH_AQASM
  /**
 @brief Atos Quantum Assembly Language

*/
  AQASM,
#endif

#ifdef LIBKET_WITH_CQASM
  /**
 @brief Common Quantum Assembly Language v1.0

 The common Quantum Assembly Language (cQASM) v1.0 is defined
 in the document https://arxiv.org/abs/1805.09607 by
 N. Khammassi, G.G. Guerreschi, I. Ashraf, J. W. Hogaboam,
 C. G. Almudever, K. Bertels
*/
  cQASMv1,
#endif

#ifdef LIBKET_WITH_OPENQASM
  /**
 @brief Open Quantum Assembly Language v2.0

 The Open Quantum Assembly Language (openQASM) v2.0 is defined
 in the document https://arxiv.org/abs/1707.03429 by Andrew
 W. Cross, Lev S. Bishop, John A. Smolin, Jay M. Gambetta
*/
  OpenQASMv2,
#endif

#ifdef LIBKET_WITH_OPENQL
  /**
 @brief OpenQL Framework

 OpenQL is a framework for high-level quantum programming in
 C++/Python. The framework provides a compiler for compiling
 and optimizing quantum code. The compiler produces the
 intermediate quantum assembly language and the compiled
 micro-code for various target platforms. While the microcode
 is platform-specific, the quantum assembly code (qasm) is
 hardware-agnostic and can be simulated on the QX simulator.

 The OpenQL Framework is developed at the Quantum Engineering
 Lab at TU Delft. The project can be found online at
 https://github.com/QE-Lab/OpenQL
*/
  OpenQL,
#endif

#ifdef LIBKET_WITH_QASM
  /**
 @brief QASM for the quantum circuit viewer qasm2circ

 QASM is a simple text-format language for describing acyclic
 quantum circuits composed from single qubit and multiply
 controlled single-qubit gates.

 qasm2circ is a package which converts a QASM file into a
 graphical depiction of the quantum circuit, using standard
 quantum gate symbols (and other user-defined symbols).  This
 is done using latex (specifically, xypic), to produce
 high-quality output in epsf, pdf, or png formats.

 Figures of quantum circuits in the book "Quantum Computation
 and Quantum Information," by Nielsen and Chuang, were produced
 using an earlier version of this package.

 The qasm2circ package is developed by I. Chuang
 <ichuang@mit.edu> and can be obtained from
 https://www.media.mit.edu/quanta/qasm2circ
*/
  QASM,
#endif

#ifdef LIBKET_WITH_QUIL
  /**
 @brief Rigetti's Quantum Instruction Language

 Rigetti's Quantum Instruction Language (Quil) is defined in
 the document https://arxiv.org/abs/1608.03355 by Robert
 S. Smith, Michael J. Curtis, William J. Zeng
*/
  Quil,
#endif

#ifdef LIBKET_WITH_QX
  /**
 @brief QX-simulator

 QX is a quantum simulator that is developed at the Quantum
 Engineering Lab at TU Delft. The project can be found online
 at https://github.com/QE-Lab/qx-simulator
*/
  QX,
#endif
};

/**
   @brief LibKet quantum base class

   The LibKet quantum base class is the base class of all LibKet classes.
*/
class QBase
{};

/**
   @brief LibKet standard logging messages

   QInfo is ment to be the standard output stream, like for the
   output of the executables. In general, the library should not
   write to QInfo.
*/
#define QInfo std::cout

/**
   @brief LibKet warning logging messages

   QWarn is for warnings, eg, for missing functionality or problem
   in the input.

   Note that QWarn cannot be given as a parameter to another
   function.
*/
#define QWarn std::cout << "Warning: "

/**
   @brief LibKet debugging logging messages

   QDebug and QDebugVar(.) are for debugging messages and are
   enabled in debug mode only.

   Note that QDebug cannot be given as a parameter to another
   function.
*/
#ifndef NDEBUG

#define QDebug std::cout << "LIBKET_DEBUG: "

#define QDebugVar(variable)                                                    \
  QDebug << (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)   \
         << ":" << __LINE__ << ", " #variable ": \n"                           \
         << (variable) << "\n"
#define QDebugIf(cond, variable)                                               \
  if (cond)                                                                    \
  QDebug << "[ " #cond " ] -- "                                                \
         << (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)   \
         << ":" << __LINE__ << ", " #variable ": \n"                           \
         << (variable) << "\n"
#else
#define QDebug                                                                 \
  if (0)                                                                       \
  std::cout
#define QDebugVar(variable)
#define QDebugIf(cond, variable)
#endif

/**
   @brief LibKet C++ feature enabling macros

   The C++ feature enabling macros __CXX11__, __CXX14__, __CXX17__,
   and __CXX20__ enable the given statement if the C++ version is
   equal to or better than C++11, 14, 17, or 20.

   The C++ feature enabling macros __CXX11_ONLY__, __CXX14_ONLY__,
   __CXX17_ONLY__, and __CXX20_ONLY__ enable the given statement if
   the C++ version exactly matches C++11, 14, 17, or 20.

   The following code snippet illustrates how to disable the
   `constexpr` keyword for C++ version below C++14 and and to enable
   the return type deduction for C++11 only.

   \code
   template<typename T>
   auto __CXX14__(constexpr) function(T& t) __CXX11_ONLY__(-> T)
   { ...}
   \endcode
*/
#if __cplusplus >= 201806L // C++20

#define __CXX11__(statement) statement
#define __CXX14__(statement) statement
#define __CXX17__(statement) statement
#define __CXX20__(statement) statement

#define __CXX11_ONLY__(statement)
#define __CXX14_ONLY__(statement)
#define __CXX17_ONLY__(statement)
#define __CXX20_ONLY__(statement) statement

#elif __cplusplus >= 201703L // C++17

#define __CXX11__(statement) statement
#define __CXX14__(statement) statement
#define __CXX17__(statement) statement
#define __CXX20__(statement)

#elif __cplusplus >= 201402L // C++14

#define __CXX11__(statement) statement
#define __CXX14__(statement) statement
#define __CXX17__(statement)
#define __CXX20__(statement)

#elif __cplusplus >= 201103L // C++11

#define __CXX11__(statement) statement
#define __CXX14__(statement)
#define __CXX17__(statement)
#define __CXX20__(statement)

#endif

/**
  @brief LibKet default quantum streams

  The default quantum streams are used whenever quantum jobs are not sceduled to
  a dedicated quantum stream object. The default quantum streams might be
  compared with the default streams in NVIDIA's CUDA SDK.
  */
static QStream<QJob_enum::Python> _qstream_python;
static QStream<QJob_enum::CXX> _qstream_cxx;

} // namespace LibKet

#endif // QBASE_HPP
