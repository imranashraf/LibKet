/** @file libket/QUtils.hpp

    @brief LibKet utility classes

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QUTILS_HPP
#define QUTILS_HPP

#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <type_traits>

namespace LibKet {

/**
   @namespace LibKet::utils

   @brief
   The LibKet::utils namespace, containing utility functionality of the LibKet
   project

   The LibKet::utils namespace contains internal utility
   functionality of the LibKet project that is not exposed to the
   end-user. Functionality in this namespace can change without
   notice.
 */

namespace utils {

// Make static_for available
using namespace zed;

// Make JSON available
using json = nlohmann::json;

// This is the type which holds sequences
template<std::size_t... Ns>
struct sequence
{
  constexpr operator std::initializer_list<std::size_t>() const noexcept
  {
    return { Ns... };
  }
};

// First define the template signature
template<std::size_t... Ns>
struct seq_gen;

// Recursion case
template<std::size_t I0, std::size_t I, std::size_t... Ns>
struct seq_gen<I0, I, Ns...>
{
  // Take front most number of sequence,
  // decrement it, and prepend it twice.
  // First I - 1 goes into the counter,
  // Second I - 1 goes into the sequence.
  using type = typename seq_gen<I0, I - 1, I - 1, Ns...>::type;
};

// Recursion abort
template<std::size_t I0, std::size_t... Ns>
struct seq_gen<I0, I0, Ns...>
{
  using type = sequence<Ns...>;
};

template<std::size_t N, std::size_t M = 0>
using sequence_t = typename seq_gen<M >= N ? N : 0, M >= N ? M + 1 : N>::type;

template<std::size_t N, std::size_t _N, std::size_t... Ns>
std::ostream&
operator<<(std::ostream& os, const sequence<N, _N, Ns...>&) noexcept
{
  os << N << ", " << sequence<_N, Ns...>();
  return os;
}

template<std::size_t N>
std::ostream&
operator<<(std::ostream& os, const sequence<N>&) noexcept
{
  os << N << "\n";
  return os;
}

/**
   @brief Compile-time variant of std::forward_as_tuple

   The std::forward_as_tuple function cannot be used within constant
   expressions. This variant can be used within constant expressions.
  */
template<typename... Ts>
constexpr std::tuple<Ts...>
forward_as_tuple(Ts&&... ts) noexcept
{
  return std::tuple<Ts...>{ std::forward<Ts>(ts)... };
}

/**
@namespace LibKet::utils::detail

@brief
The LibKet::utils::detail namespace, containing internal
implementation details of the utility functionality of the
LibKet project

The LibKet::utils::detail namespace contains internal
implementation details of the utility functionality of the
LibKet project that are not exposed to the end-user.
Functionality in this namespace can change without notice.
*/
namespace detail {

/**
@brief
Converts strings into types

@note
This is a simplified version of the 'typestring' header file
developed by George Makrydakis <george@irrequietus.eu>.

The header of the original 'typestring' header file reads as follows:

@copyright Copyright (C) 2015, 2016 George Makrydakis
<george@irrequietus.eu>

The 'typestring' header is a single header C++ library for
creating types to use as type parameters in template
instantiations, repository available at
https://github.com/irrequietus/typestring. Conceptually
stemming from own implementation of the same thing (but in a
more complicated manner to be revised) in 'clause':
https://github.com/irrequietus/clause.

File subject to the terms and conditions of the Mozilla
Public License v 2.0. If a copy of the MPLv2 license text
was not distributed with this file, you can obtain it at:
http://mozilla.org/MPL/2.0/.
*/
template<int N, int M>
constexpr char
tygrab(char const (&c)[M]) noexcept
{
  return c[N < M ? N : M - 1];
}

} // namespace detail

/**
   @brief Python interpreter wrapper

*/

/**
@brief
Compile-time constant string type

The compile-time constant string type makes it possible to
encode objects of type std::string into types so that the
content of the string can be extracted from the type and does
not need an instantiated object.

The string_t type is be used to store, e.g., parameter values
of quantum gates in the LibKet::QGate classes
*/
template<char... Cs>
struct string_t final
{
private:
  /// Capture leading character
  template<char C_, char... Cs_>
  struct lchar_t
  {
    static constexpr char value = C_;
  };

public:
  /// Default constructor
  constexpr string_t() = default;

  /// Returns value as string
  inline static std::string str()
  {
    std::string val;
    int unpack[]{ 0, (val += Cs, 0)... };
    static_cast<void>(unpack);

    // Remove trailing '\x00' symbols
    val.erase(std::remove(val.begin(), val.end(), '\x00'), val.end());

    return val;
  }

  /// Returns value as real-valued number
  inline static real_t value()
  {
    return (std::is_same<real_t, float>::value
              ? std::stof(str())
              : (std::is_same<real_t, double>::value ? std::stod(str())
                                                     : std::stold(str())));
  }
};

/// Operator+()
template<char C, char... Cs>
inline static auto
operator+(const string_t<C, Cs...>&)
{
  return string_t<C, Cs...>{};
}

/// Operator-()
template<char C, char... Cs>
inline static auto
operator-(const string_t<C, Cs...>&)
{
  return string_t<'-', C, Cs...>{};
}

/// Operator-()
template<char... Cs>
inline static auto
operator-(const string_t<'-', Cs...>&)
{
  return string_t<Cs...>{};
}

/// Operator==()
template<char... Cs, char... Ds>
inline static auto
operator==(const string_t<Cs...>&, const string_t<Ds...>&)
{
  return (string_t<Cs...>::value() == string_t<Ds...>::value());
}

/// Operator!=()
template<char... Cs, char... Ds>
inline static auto
operator!=(const string_t<Cs...>&, const string_t<Ds...>&)
{
  return (string_t<Cs...>::value() != string_t<Ds...>::value());
}

/// Operator<=()
template<char... Cs, char... Ds>
inline static auto
operator<=(const string_t<Cs...>&, const string_t<Ds...>&)
{
  return (string_t<Cs...>::value() <= string_t<Ds...>::value());
}

/// Operator>=()
template<char... Cs, char... Ds>
inline static auto
operator>=(const string_t<Cs...>&, const string_t<Ds...>&)
{
  return (string_t<Cs...>::value() >= string_t<Ds...>::value());
}

/// Operator<()
template<char... Cs, char... Ds>
inline static auto
operator<(const string_t<Cs...>&, const string_t<Ds...>&)
{
  return (string_t<Cs...>::value() < string_t<Ds...>::value());
}

/// Operator>()
template<char... Cs, char... Ds>
inline static auto
operator>(const string_t<Cs...>&, const string_t<Ds...>&)
{
  return (string_t<Cs...>::value() > string_t<Ds...>::value());
}

/// Tolerance function: true of abs(first argument) > abs(second argument)
template<char... Cs, char... Ds>
inline static auto
tolerance(const string_t<Cs...>&, const string_t<Ds...>&)
{
  return (abs(string_t<Cs...>::value()) > abs(string_t<Ds...>::value()));
}

/// Tolerance function: true of abs(value) > abs(second argument)
template<char... Ds>
inline static auto
tolerance(const real_t& value, const string_t<Ds...>&)
{
  return (abs(value) > abs(string_t<Ds...>::value()));
}

template<char... Cs>
std::ostream&
operator<<(std::ostream& os, const string_t<Cs...>& s)
{
  os << s.str();
  return os;
}

template<typename T>
std::string
to_string(const T& obj)
{
  return std::to_string(obj);
}

template<>
std::string
to_string(const float& obj)
{
  std::stringstream ss;
  ss << std::fixed << std::setprecision(std::numeric_limits<float>::digits10)
     << obj;
  return ss.str();
}

template<>
std::string
to_string(const double& obj)
{
  std::stringstream ss;
  ss << std::fixed << std::setprecision(std::numeric_limits<double>::digits10)
     << obj;
  return ss.str();
}

template<>
std::string
to_string(const long double& obj)
{
  std::stringstream ss;
  ss << std::fixed
     << std::setprecision(std::numeric_limits<long double>::digits10) << obj;
  return ss.str();
}

} // namespace utils

#define TYPESTRING1(n, x) LibKet::utils::detail::tygrab<0x##n##0>(x)

#define TYPESTRING2(n, x)                                                      \
  LibKet::utils::detail::tygrab<0x##n##0>(x),                                  \
    LibKet::utils::detail::tygrab<0x##n##1>(x)

#define TYPESTRING4(n, x)                                                      \
  LibKet::utils::detail::tygrab<0x##n##0>(x),                                  \
    LibKet::utils::detail::tygrab<0x##n##1>(x),                                \
    LibKet::utils::detail::tygrab<0x##n##2>(x),                                \
    LibKet::utils::detail::tygrab<0x##n##3>(x)

#define TYPESTRING8(n, x)                                                      \
  LibKet::utils::detail::tygrab<0x##n##0>(x),                                  \
    LibKet::utils::detail::tygrab<0x##n##1>(x),                                \
    LibKet::utils::detail::tygrab<0x##n##2>(x),                                \
    LibKet::utils::detail::tygrab<0x##n##3>(x),                                \
    LibKet::utils::detail::tygrab<0x##n##4>(x),                                \
    LibKet::utils::detail::tygrab<0x##n##5>(x),                                \
    LibKet::utils::detail::tygrab<0x##n##6>(x),                                \
    LibKet::utils::detail::tygrab<0x##n##7>(x)

#define TYPESTRING16(n, x)                                                     \
  LibKet::utils::detail::tygrab<0x##n##0>(x),                                  \
    LibKet::utils::detail::tygrab<0x##n##1>(x),                                \
    LibKet::utils::detail::tygrab<0x##n##2>(x),                                \
    LibKet::utils::detail::tygrab<0x##n##3>(x),                                \
    LibKet::utils::detail::tygrab<0x##n##4>(x),                                \
    LibKet::utils::detail::tygrab<0x##n##5>(x),                                \
    LibKet::utils::detail::tygrab<0x##n##6>(x),                                \
    LibKet::utils::detail::tygrab<0x##n##7>(x),                                \
    LibKet::utils::detail::tygrab<0x##n##8>(x),                                \
    LibKet::utils::detail::tygrab<0x##n##9>(x),                                \
    LibKet::utils::detail::tygrab<0x##n##A>(x),                                \
    LibKet::utils::detail::tygrab<0x##n##B>(x),                                \
    LibKet::utils::detail::tygrab<0x##n##C>(x),                                \
    LibKet::utils::detail::tygrab<0x##n##D>(x),                                \
    LibKet::utils::detail::tygrab<0x##n##E>(x),                                \
    LibKet::utils::detail::tygrab<0x##n##F>(x)

#define QConst1_t (value) LibKet::utils::string_t<TYPESTRING1(0, #value)>
#define QConst2_t (value) LibKet::utils::string_t<TYPESTRING2(0, #value)>
#define QConst4_t (value) LibKet::utils::string_t<TYPESTRING4(0, #value)>
#define QConst8_t (value) LibKet::utils::string_t<TYPESTRING8(0, #value)>
#define QConst16_t(value) LibKet::utils::string_t<TYPESTRING16(0, #value)>
#define QConst32_t(value)                                                      \
  LibKet::utils::string_t<TYPESTRING16(0, #value), TYPESTRING16(1, #value)>
#define QConst64_t(value)                                                      \
  LibKet::utils::string_t<TYPESTRING16(0, #value),                             \
                          TYPESTRING16(1, #value),                             \
                          TYPESTRING16(2, #value),                             \
                          TYPESTRING16(3, #value)>
#define QConst128_t(value)                                                     \
  LibKet::utils::string_t<TYPESTRING16(0, #value),                             \
                          TYPESTRING16(1, #value),                             \
                          TYPESTRING16(2, #value),                             \
                          TYPESTRING16(3, #value),                             \
                          TYPESTRING16(4, #value),                             \
                          TYPESTRING16(5, #value),                             \
                          TYPESTRING16(6, #value),                             \
                          TYPESTRING16(7, #value)>

#define QConst1(value)                                                         \
  QConst1_t(value) {}
#define QConst2(value)                                                         \
  QConst2_t(value) {}
#define QConst4(value)                                                         \
  QConst4_t(value) {}
#define QConst8(value)                                                         \
  QConst8_t(value) {}
#define QConst16(value)                                                        \
  QConst16_t(value) {}
#define QConst32(value)                                                        \
  QConst32_t(value) {}
#define QConst64(value)                                                        \
  QConst64_t(value) {}
#define QConst128(value) QConst128_t(value)()

#if (real_t == float)
#define QConst_t(value) QConst16_t(value)
#elif (real_t == double)
#define QConst_t(value) QConst32_t(value)
#else
#define QConst_t(value) QConst64_t(value)
#endif

#define QConst(value)                                                          \
  QConst_t(value) {}

} // namespace LibKet

#endif // QUTILS_HPP
