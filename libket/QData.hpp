/** @file libket/QData.hpp

    @brief LibKet quantum data classes

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QDATA_HPP
#define QDATA_HPP

#include <cstdio>
#include <exception>
#include <fstream>
#include <functional>
#include <iostream>
#include <regex>
#include <sstream>
#include <string>

#ifdef LIBKET_WITH_PYTHON
#include <Python.h>
#endif

#include <QBase.hpp>
#include <QUtils.hpp>

namespace LibKet {

/**
   @brief LibKet quantum data class

   The LibKet quantum data class holds the quantum data
*/
template<std::size_t _qubits, QBackend _qbackend>
class QData;

#ifdef LIBKET_WITH_AQASM
/**
   @brief LibKet quantum data class specialization for AQASM backend

   The LibKet quantum data class implements the quantum data class
   for the AQASM backend.
*/
template<std::size_t _qubits>
class QData<_qubits, QBackend::AQASM> : public QBase
{
public:
  /// QBit type
  using qubit_t = std::size_t;

  /// QData type
  using data_t = complex_t;

  /// Default constructor
  QData() = default;

  /// Constructor: Copy from raw data
  QData(data_t) {}

  /// Constructor: Copy from raw data
  QData(data_t[_qubits]) {}

  /// Constructor: Copy from constant reference
  QData(const QData&) = default;

  /// Constructor: More from universal reference
  QData(QData&&) = default;

  /// Copy assignment operator
  QData& operator=(const QData&) = default;

  /// Move assignment operator
  QData& operator=(QData&&) = default;

  /// Access operator[]
  qubit_t operator[](std::size_t i) const noexcept { return (qubit_t)i; }

  /// Operator+: Concatenate two QData objects
  template<std::size_t __qubits>
  QData<_qubits + __qubits, QBackend::AQASM> inline constexpr operator+(
    QData<__qubits, QBackend::AQASM>) const noexcept
  {
    return QData<_qubits + __qubits, QBackend::AQASM>();
  }

  /// Dump QData object to string
  const std::string to_string() const noexcept { return _kernel + "END\n"; }

  /// Append quantum gate expression to kernel
  QData& append_kernel(const std::string& gate)
  {
    _kernel.append(gate);
    return *this;
  }

  /// Get constant reference to kernel
  const std::string& kernel() const { return _kernel; }

  /// Get reference to kernel
  std::string& kernel() { return _kernel; }

  /// Clear kernel
  void clear() { _kernel.clear(); }

  /// Execute quantum circuit remotely on Atos QLM  asynchronously
  QJob<QJob_enum::Python>* execute_async(
    QStream<QJob_enum::Python>* stream = NULL,
    std::size_t shots = 1024)
  {
#ifdef LIBKET_WITH_PYTHON
    std::stringstream ss;

    ss << "def run():\n"

       << "\ttry:\n"
       << "\t\timport json\n"
       << "\texcept:\n"
       << "\t\treturn '\"Unable to import json module\"'\n"

       << "\ttry:\n"
       << "\t\timport bxi.base.log\n"
       << "\texcept:\n"
       << "\t\treturn '\"Unable to import bxi.base.log module\"'\n"

       << "\ttry:\n"
       << "\t\timport import qat.core.qpu\n"
       << "\texcept:\n"
       << "\t\treturn '\"Unable to import import qat.core.qpu module\"'\n"

       << "\ttry:\n"
       << "\t\timport import qat.core.qpu.agent\n"
       << "\texcept:\n"
       << "\t\treturn '\"Unable to import import qat.core.qpu.agent module\"'\n"

       << "\ttry:\n"
       << "\t\timport import qat.core.qpu.stateanalyzer\n"
       << "\texcept:\n"
       << "\t\treturn '\"Unable to import import qat.core.qpu.stateanalyzer module\"'\n"
      
       << "\ttry:\n"
       << "\t\timport import qat.core.task\n"
       << "\texcept:\n"
       << "\t\treturn '\"Unable to import import qat.core.task module\"'\n"

       << "\ttry:\n"
       << "\t\timport qat.lang.parser.aqasm_parser\n"
       << "\texcept:\n"
       << "\t\treturn '\"Unable to import qat.lang.parser.aqasm_parser module\"'\n"
      
       << "\ttry:\n"
       << "\t\timport qat.mps\n"
       << "\texcept:\n"
       << "\t\treturn '\"Unable to import qat.mps module\"'\n"

       << "\taqasm = '''\n"
       << to_string() << "'''\n"
      
       << "\t_LOGGER = bxi.base.log.getLogger(bxi.base.log.LIB_PREFIX + \"qat.lang.aqasm2circ\")\n"
       << "\tparser = qat.lang.parser.aqasm_parser.AqasmParser()\n"
       << "\tparser.build(errorlog=_LOGGER)\n"
       << "\tif parser.parse(aqasm) == 1:\n"
       << "\t\tcircuit = parser.compiler.gen_circuit()\n"
       << "\telse:\n"
       << "\t\treturn '\"An error occured while parsing the AQASM program\"'\n"

       << "\tsimulator = qat.mps.MPS(lnnize=False, no_merge=False, n_trunc=None, threshold=None)\n"
      
       << "\thandlers = {'agent': qat.core.qpu.agent.GenericAgent, 'state_analyzer': qat.core.qpu.stateanalyzer.StateAnalyzer}\n"

       << "\tqpu = qat.core.qpu.Server(simulator, handlers=handlers)\n"
       << "\ttask = qat.core.task.Task(circuit)\n"
       << "\ttask.attach(qpu)\n"
       << "\trangeset = range(len(qbits.qbits))\n"
       << "\tresult = task.execute(list(rangeset))\n"

       << "\tdata = {}\n"
       << "\tdata['amplitude'] = result.amplitude\n"
       << "\tdata['cbits'] = result.cbits\n"
       << "\tdata['err'] = result.err\n"
       << "\tdata['history'] = result.history\n"
       << "\tdata['probability'] = result.probability\n"

       << "\tdata_report = {}\n"
       << "\tdata_report['elapsed'] = result.report.elapsed\n"
       << "\tdata_report['max_rss'] = result.report.max_rss\n"
       << "\tdata_report['system']  = result.report.system\n"
       << "\tdata_report['user']    = result.report.user\n"
       << "\tdata['report'] = data_report\n"

       << "\tdata_state = {}\n"
       << "\tdata_state['bin'] = result.state.bin\n"
       << "\tfor i in range(result.state.len):\n"
       << "\t\tdata_state[str(count)] = result.state[i]\n"
       << "\tdata['state'] = data_state\n"

       << "\treturn json.dumps(data)\n";
      
    QDebug << ss.str();

    try {
      if (stream != NULL)
        return stream->run(ss.str(), "run", "", "");
      else
        return _qstream_python.run(ss.str(), "run", "", "");
    } catch (std::exception& e) {
      QInfo << e.what() << std::endl;
    }
#else
    throw "This feature requires -DLIBKET_WITH_PYTHON";
#endif
  }

  /// Execute quantum circuit remotely on Atos QLM  synchronously
  utils::json execute(QStream<QJob_enum::Python>* stream = NULL,
                      std::size_t shots = 1024)
  {
    return execute_async(stream, shots)->get();
  }

private:
  /// Kernel source code
  std::string _kernel;
};
#endif

#ifdef LIBKET_WITH_CQASM
/**
   @brief LibKet quantum data class specialization for cQASM v1.0 backend

   The LibKet quantum data class implements the quantum data class
   for the cQASM v1.0 backend.
*/
template<std::size_t _qubits>
class QData<_qubits, QBackend::cQASMv1> : public QBase
{
public:
  /// QBit type
  using qubit_t = std::size_t;

  /// QData type
  using data_t = complex_t;

  /// Default constructor
  QData() = default;

  /// Constructor: Copy from raw data
  QData(data_t) {}

  /// Constructor: Copy from raw data
  QData(data_t[_qubits]) {}

  /// Constructor: Copy from constant reference
  QData(const QData&) = default;

  /// Constructor: More from universal reference
  QData(QData&&) = default;

  /// Copy assignment operator
  QData& operator=(const QData&) = default;

  /// Move assignment operator
  QData& operator=(QData&&) = default;

  /// Access operator[]
  qubit_t operator[](std::size_t i) const noexcept { return (qubit_t)i; }

  /// Operator+: Concatenate two QData objects
  template<std::size_t __qubits>
  QData<_qubits + __qubits, QBackend::cQASMv1> inline constexpr operator+(
    QData<__qubits, QBackend::cQASMv1>) const noexcept
  {
    return QData<_qubits + __qubits, QBackend::cQASMv1>();
  }

  /// Dump QData object to string
  const std::string& to_string() const noexcept { return _kernel; }

  /// Append quantum gate expression to kernel
  QData& append_kernel(const std::string& gate)
  {
    _kernel.append(gate);
    return *this;
  }

  /// Get constant reference to kernel
  const std::string& kernel() const { return _kernel; }

  /// Get reference to kernel
  std::string& kernel() { return _kernel; }

  /// Clear kernel
  void clear() { _kernel.clear(); }

  /// Execute quantum circuit remotely on Quantum Inspire asynchronously
  QJob<QJob_enum::Python>* execute_async(
    QStream<QJob_enum::Python>* stream = NULL,
    const std::string& username = std::getenv("QI_USERNAME"),
    const std::string& password = std::getenv("QI_PASSWORD"),
    const std::string& url = "https://api.quantum-inspire.com",
    const std::string& backend = "QX single-node simulator",
    std::size_t shots = 1024)
  {
#ifdef LIBKET_WITH_PYTHON
    std::stringstream ss;

    ss << "def run():\n"
      
       << "\ttry:\n"
       << "\t\timport json\n"
       << "\texcept:\n"
       << "\t\treturn '\"Unable to import json module\"'\n"
      
       << "\ttry:\n"
       << "\t\tfrom getpass import getpass\n"
       << "\texcept:\n"
       << "\t\treturn '\"Unable to import getpass module\"'\n"
      
       << "\ttry:\n"
       << "\t\tfrom coreapi.auth import BasicAuthentication\n"
       << "\texcept:\n"
       << "\t\treturn '\"Unable to import coreapi.auth module\"'\n"
      
       << "\ttry:\n"
       << "\t\tfrom quantuminspire.api import QuantumInspireAPI\n"
       << "\texcept:\n"
       << "\t\treturn '\"Unable to import quantuminspire.api module\"'\n"

       << "\tusername = '" << username << "'\n"
       << "\tpassword = '" << password << "'\n"
       << "\tserver_url = r'" << url << "'\n"
       << "\tauth = BasicAuthentication(username, password)\n"
       << "\tqi = QuantumInspireAPI(server_url, auth)\n"
      
       << "\tqasm = '''\n"
       << to_string() << "'''\n"
       << "\tbackend_type = qi.get_backend_type_by_name('" << backend << "')\n"
       << "\tresult = qi.execute_qasm(qasm, backend_type=backend_type, "
          "number_of_shots="
       << utils::to_string(shots) << ")\n"
      
       << "\treturn json.dumps(result)\n";

    QDebug << ss.str();

    try {
      if (stream != NULL)
        return stream->run(ss.str(), "run", "", "");
      else
        return _qstream_python.run(ss.str(), "run", "", "");
    } catch (std::exception& e) {
      QInfo << e.what() << std::endl;
    }
#else
    throw "This feature requires -DLIBKET_WITH_PYTHON";
#endif
  }

  /// Execute quantum circuit remotely on Quantum Inspire synchronously
  utils::json execute(
    QStream<QJob_enum::Python>* stream = NULL,
    const std::string& username = std::getenv("QI_USERNAME"),
    const std::string& password = std::getenv("QI_PASSWORD"),
    const std::string& url = "https://api.quantum-inspire.com",
    const std::string& backend = "QX single-node simulator",
    std::size_t shots = 1024)
  {
    return execute_async(stream, username, password, url, backend, shots)
      ->get();
  }

private:
  /// Kernel source code
  std::string _kernel;
};
#endif

#ifdef LIBKET_WITH_OPENQASM
/**
   @brief LibKet quantum data class specialization for OpenQASM v2.0 backend

   The LibKet quantum data class implements the quantum data class
   for the OpenQASM v2.0 backend.
*/
template<std::size_t _qubits>
class QData<_qubits, QBackend::OpenQASMv2> : public QBase
{
public:
  /// QBit type
  using qubit_t = std::size_t;

  /// QData type
  using data_t = complex_t;

  /// Default constructor
  QData() = default;

  /// Constructor: Copy from raw data
  QData(data_t) {}

  /// Constructor: Copy from raw data
  QData(data_t[_qubits]) {}

  /// Constructor: Copy from constant reference
  QData(const QData&) = default;

  /// Constructor: More from universal reference
  QData(QData&&) = default;

  /// Copy assignment operator
  QData& operator=(const QData&) = default;

  /// Move assignment operator
  QData& operator=(QData&&) = default;

  /// Access operator[]
  qubit_t operator[](std::size_t i) const noexcept { return (qubit_t)i; }

  /// Operator+: Concatenate two QData objects
  template<std::size_t __qubits>
  QData<_qubits + __qubits, QBackend::OpenQASMv2> inline constexpr operator+(
    QData<__qubits, QBackend::OpenQASMv2>) const noexcept
  {
    return QData<_qubits + __qubits, QBackend::OpenQASMv2>();
  }

  /// Dump QData object to string
  const std::string& to_string() const noexcept { return _kernel; }

  /// Append quantum gate expression to kernel
  QData& append_kernel(const std::string& gate)
  {
    _kernel.append(gate);
    return *this;
  }

  /// Get constant reference to kernel
  const std::string& kernel() const { return _kernel; }

  /// Get reference to kernel
  std::string& kernel() { return _kernel; }

  /// Clear kernel
  void clear() { _kernel.clear(); }

  /// Execute quantum circuit in simulation mode using QisKit SDK asynchronously
  QJob<QJob_enum::Python>* execute_async(
    QStream<QJob_enum::Python>* stream = NULL,
    const std::string& API_token = std::getenv("IBMQ_API_TOKEN"),
    const std::string& backend = "qasm_simulator",
    std::size_t shots = 1024)
  {
#ifdef LIBKET_WITH_PYTHON
    std::stringstream ss;
    
    ss << "def run():\n"

       << "\ttry:\n"
       << "\t\timport json\n"
       << "\texcept:\n"
       << "\t\treturn '\"Unable to import json module\"'\n"
      
       << "\ttry:\n"
       << "\timport qiskit\n"
       << "\texcept:\n"
       << "\t\treturn '\"Unable to import qiskit module\"'\n";

    if (API_token.empty())
      ss << "\tqiskit.IBMQ.load_accounts()\n";
    else
      ss << "\tqiskit.IBMQ.enable_account('" << API_token << "')\n";

    ss << "\tqr = qiskit.QuantumRegister(" << utils::to_string(_qubits) << ")\n"
       << "\tcr = qiskit.ClassicalRegister(" << utils::to_string(_qubits)
       << ")\n"
       << "\tqc = qiskit.QuantumCircuit(qr, cr)\n"
      
       << "\tqasm = '''\n"
       << to_string() << "'''\n"
       << "\tqc = qc.from_qasm_str(qasm)\n"
      
       << "\tbackend_type = qiskit.BasicAer.get_backend('" << backend << "')\n"
      
       << "\tqexe = qiskit.execute(qc, backend=backend_type, shots="
       << utils::to_string(shots) << ")\n"
      
       << "\tresult = qexe.result()\n"

       << "\treturn json.dumps(result.to_dict())\n";

    QDebug << ss.str();

    try {
      if (stream != NULL)
        return stream->run(ss.str(), "run", "", "");
      else
        return _qstream_python.run(ss.str(), "run", "", "");
    } catch (std::exception& e) {
      QInfo << e.what() << std::endl;
    }
#else
    throw "This feature requires -DLIBKET_WITH_PYTHON";
#endif
  }

  /// Execute quantum circuit in simulation mode using QisKit SDK synchronously
  utils::json execute(
    QStream<QJob_enum::Python>* stream = NULL,
    const std::string& API_token = std::getenv("IBMQ_API_TOKEN"),
    const std::string& backend = "qasm_simulator",
    std::size_t shots = 1024)
  {
    std::cout << "#1\n";
    auto qjob = execute_async(stream, API_token, backend, shots);
    std::cout << "#2\n";
    qjob->wait();
    std::cout << "#3\n";
    //return execute_async(stream, API_token, backend, shots)->get();
    utils::json result = qjob->get();
    std::cout << "#4\n";
    return result;
  }

private:
  /// Kernel source code
  std::string _kernel;
};
#endif

#ifdef LIBKET_WITH_OPENQL
/**
   @brief LibKet quantum data class specialization for OpenQL backend

   The LibKet quantum data class implements the quantum data class
   for the OpenQL backend.
*/
template<std::size_t _qubits>
class QData<_qubits, QBackend::OpenQL> : public QBase
{
public:
  /// QBit type
  using qubit_t = std::size_t;

  /// QData type
  using data_t = complex_t;

  /// Default constructor
  QData(const std::string& platform = "platform",
        const std::string& configfile = "test_config_default.json")
    : _platform(platform, configfile)
    , _kernel("libket", _platform, _qubits, 0)
  {
#ifndef NDEBUG
    _platform.print_info();
#endif
    ql::set_platform(_platform);
    ql::options::set("output_dir", ".");
  }

  /// Constructor: Copy from raw data
  QData(data_t)
    : QData()
  {}

  /// Constructor: Copy from raw data
  QData(data_t[_qubits])
    : QData()
  {}

  /// Constructor: Copy from constant reference
  QData(const QData&)
    : QData()
  {}

  /// Constructor: More from universal reference
  QData(QData&&)
    : QData()
  {}

  /// Copy assignment operator
  QData& operator=(const QData&) = default;

  /// Move assignment operator
  QData& operator=(QData&&) = default;

  /// Access operator[]
  qubit_t operator[](std::size_t i) const noexcept { return (qubit_t)i; }

  /// Operator+: Concatenate two QData objects
  template<std::size_t __qubits>
  QData<_qubits + __qubits, QBackend::OpenQL> inline constexpr operator+(
    QData<__qubits, QBackend::OpenQL>) const noexcept
  {
    return QData<_qubits + __qubits, QBackend::OpenQL>();
  }

  /// Dump QData object to string
  std::string to_string() const noexcept
  {
    return const_cast<ql::quantum_kernel&>(_kernel).qasm();
  }

  /// Append quantum gate expression to kernel
  QData& append_kernel(std::function<void()> append_gate)
  {
    append_gate();
    return *this;
  }

  /// Get constant reference to kernel
  const ql::quantum_kernel& kernel() const { return _kernel; }

  /// Get reference to kernel
  ql::quantum_kernel& kernel() { return _kernel; }

  /// Clear kernel
  void clear() { _kernel.get_circuit().clear(); }

  /// Get constant reference to platform
  const ql::quantum_platform& platform() const { return _platform; }

  /// Get reference to platform
  ql::quantum_platform& platform() { return _platform; }

  /// Compile kernel
  std::string compile(bool scheduled = false)
  {
    std::string filename = std::tmpnam(nullptr);
    ql::options::set("output_dir", "/");
    std::string s = compile(filename, scheduled);
    ql::options::set("output_dir", ".");
    return s;
  }

  /// Compile kernel
  std::string compile(const std::string& filename, bool scheduled = false)
  {

    ql::quantum_program _program(filename, _platform, _qubits, 0);
    _program.add(_kernel);
    try {
      _program.compile();
    } catch (std::exception& e) {
      QInfo << e.what() << std::endl;
    }

    std::ifstream file;
    if (scheduled)
      file.open(filename + "_scheduled.qasm");
    else
      file.open(filename + ".qasm");

    if (file.is_open()) {
      std::stringstream ss;
      ss << file.rdbuf();
      return ss.str();
    } else
      return "Unable to open file" + filename;
  }

private:
  ql::quantum_platform _platform;
  ql::quantum_kernel _kernel;
};
#endif

#ifdef LIBKET_WITH_QASM
/**
   @brief LibKet quantum data class specialization for QASM backend

   The LibKet quantum data class implements the quantum data class
   for the QASM backend.
*/
template<std::size_t _qubits>
class QData<_qubits, QBackend::QASM> : public QBase
{
public:
  /// QBit type
  using qubit_t = std::size_t;

  /// QData type
  using data_t = complex_t;

  /// Default constructor
  QData() = default;

  /// Constructor: Copy from raw data
  QData(data_t) {}

  /// Constructor: Copy from raw data
  QData(data_t[_qubits]) {}

  /// Constructor: Copy from constant reference
  QData(const QData&) = default;

  /// Constructor: More from universal reference
  QData(QData&&) = default;

  /// Copy assignment operator
  QData& operator=(const QData&) = default;

  /// Move assignment operator
  QData& operator=(QData&&) = default;

  /// Access operator[]
  qubit_t operator[](std::size_t i) const noexcept { return (qubit_t)i; }

  /// Operator+: Concatenate two QData objects
  template<std::size_t __qubits>
  QData<_qubits + __qubits, QBackend::QASM> inline constexpr operator+(
    QData<__qubits, QBackend::QASM> other) const noexcept
  {
    return QData<_qubits + __qubits, QBackend::QASM>();
    ;
  }

  /// Dump QData object to string
  const std::string& to_string() const noexcept { return _kernel; }

  /// Append quantum gate expression to kernel
  QData& append_kernel(const std::string& gate)
  {
    _kernel.append(gate);
    return *this;
  }

  /// Get constant reference to kernel
  const std::string& kernel() const { return _kernel; }

  /// Get reference to kernel
  std::string& kernel() { return _kernel; }

  /// Clear kernel
  void clear() { _kernel.clear(); }

  /// Compile kernel
  std::string compile()
  {
    std::string filename = std::tmpnam(nullptr);
    return compile(filename);
  }

  /// Compile kernel
  std::string compile(const std::string& filename,
                      QStream<QJob_enum::Python>* stream = NULL)
  {
    std::ofstream file;
    file.open(filename + ".qasm");
    file << *this;
    file.close();

    std::stringstream ss;
    utils::json result;

    ss << "def run():\n"

       << "\ttry:\n"
       << "\t\timport json\n"
       << "\texcept:\n"
       << "\t\treturn '\"Unable to import json module\"'\n"

       << "\ttry:\n"
       << "\t\timport subprocess\n"
       << "\texcept:\n"
       << "\t\treturn '\"Unable to import subprocess module\"'\n"
      
       << "\tresult=subprocess.run(['python', "
          "'"
       << QASM2TEX_PY << "', '" << filename << ".qasm'], capture_output=True)\n"
       << "\treturn json.dumps(result.stdout.decode('UTF-8'))\n";

    QDebug << ss.str();

    try {
      if (stream != NULL)
        result = stream->run(ss.str(), "run", "", "")->get();
      else
        result = _qstream_python.run(ss.str(), "run", "", "")->get();

      file.open(filename + ".tex");
      file << result.get<std::string>();
      file.close();
    } catch (std::exception& e) {
      QInfo << e.what() << std::endl;
    }
    return result.get<std::string>();
  }

private:
  /// Kernel source code
  std::string _kernel;
};
#endif

#ifdef LIBKET_WITH_QUIL
/**
   @brief LibKet quantum data class specialization for Quil backend

   The LibKet quantum data class implements the quantum data class
   for the Quil backend.
*/
template<std::size_t _qubits>
class QData<_qubits, QBackend::Quil> : public QBase
{
public:
  /// QBit type
  using qubit_t = std::size_t;

  /// QData type
  using data_t = complex_t;

  /// Default constructor
  QData() = default;

  /// Constructor: Copy from raw data
  QData(data_t) {}

  /// Constructor: Copy from raw data
  QData(data_t[_qubits]) {}

  /// Constructor: Copy from constant reference
  QData(const QData&) = default;

  /// Constructor: More from universal reference
  QData(QData&&) = default;

  /// Copy assignment operator
  QData& operator=(const QData&) = default;

  /// Move assignment operator
  QData& operator=(QData&&) = default;

  /// Access operator[]
  qubit_t operator[](std::size_t i) const noexcept { return (qubit_t)i; }

  /// Operator+: Concatenate two QData objects
  template<std::size_t __qubits>
  QData<_qubits + __qubits, QBackend::Quil> inline constexpr operator+(
    QData<__qubits, QBackend::Quil>) const noexcept
  {
    return QData<_qubits + __qubits, QBackend::Quil>();
  }

  /// Dump QData object to string
  const std::string& to_string() const noexcept { return _kernel; }

  /// Append quantum gate expression to kernel
  QData& append_kernel(const std::string& gate)
  {
    _kernel.append(gate);
    return *this;
  }

  /// Get constant reference to kernel
  const std::string& kernel() const { return _kernel; }

  /// Get reference to kernel
  std::string& kernel() { return _kernel; }

  /// Clear kernel
  void clear() { _kernel.clear(); }

  /// Execute quantum circuit remotely on Rigetti QVM asynchronously
  QJob<QJob_enum::Python>* execute_async(
    QStream<QJob_enum::Python>* stream = NULL,
    const std::string& backend = "9q-square-qvm",
    std::size_t shots = 1024)
  {
#ifdef LIBKET_WITH_PYTHON
    std::stringstream ss;

    ss << "def run():\n"

       << "\ttry:\n"
       << "\t\timport json\n"
       << "\texcept:\n"
       << "\t\treturn '\"Unable to import json module\"'\n"

       << "\ttry:\n"
       << "\t\timport pyquil\n"
       << "\texcept:\n"
       << "\t\treturn '\"Unable to import pyquil module\"'\n"

       << "\tqasm = '''\n"
       << to_string() << "'''\n"
      
       << "\tp = pyquil.Program(qasm)\n"
       << "\tqc = pyquil.get_qc('" << backend << "')\n"
       << "\tresult = qc.run_and_measure(p, trials=" << utils::to_string(shots)
       << ")\n"
      
       << "\tj=json.loads('''{\"0\":'''+str(result.get(0).tolist())+'''}''')\n"
       << "\tfor i in result.keys():\n"
       << "\t\tj.update(json.loads('''{\"'''+str(i)+'''\":'''+str(result.get(i)"
          ".tolist())+'''}'''))\n";

    ss << "\treturn json.dumps(j)\n";

    QDebug << ss.str();

    try {
      if (stream != NULL)
        return stream->run(ss.str(), "run", "", "");
      else
        return _qstream_python.run(ss.str(), "run", "", "");
    } catch (std::exception& e) {
      QInfo << e.what() << std::endl;
    }
#else
    throw "This feature requires -DLIBKET_WITH_PYTHON";
#endif
  }

  /// Execute quantum circuit remotely on Rigetti QVM synchronously
  utils::json execute(QStream<QJob_enum::Python>* stream = NULL,
                      const std::string& backend = "9q-square-qvm",
                      std::size_t shots = 1024)
  {
    return execute_async(stream, backend, shots)->get();
  }

private:
  /// Kernel source code
  std::string _kernel;
};
#endif

#ifdef LIBKET_WITH_QX
/**
   @brief LibKet quantum data class specialization for QX backend

   The LibKet quantum data class implements the quantum data class
   for the QX simulator backend.
*/
template<std::size_t _qubits>
class QData<_qubits, QBackend::QX> : public QBase
{
public:
  /// QBit type
  using qubit_t = std::size_t;

  /// QData type
  using data_t = complex_t;

  /// Default constructor
  QData()
    : _circuit(_qubits)
    , _reg(_qubits)
  {
    xpu::init();
  }

  /// Constructor: Copy from raw data
  QData(data_t)
    : QData()
  {}

  /// Constructor: Copy from raw data
  QData(data_t[_qubits])
    : QData()
  {}

  /// Constructor: Copy from constant reference
  QData(const QData&)
    : QData()
  {}

  /// Constructor: More from universal reference
  QData(QData&&)
    : QData()
  {}

  /// Destructor
  ~QData() { xpu::clean(); }

  /// Copy assignment operator
  QData& operator=(const QData&) = default;

  /// Move assignment operator
  QData& operator=(QData&&) = default;

  /// Access operator[]
  qubit_t operator[](std::size_t i) const noexcept { return (qubit_t)i; }

  /// Operator+: Concatenate two QData objects
  template<std::size_t __qubits>
  QData<_qubits + __qubits, QBackend::QX> inline constexpr operator+(
    QData<__qubits, QBackend::QX>) const noexcept
  {
    return QData<_qubits + __qubits, QBackend::QX>();
  }

  /// Dump QData object to string
  std::string to_string() const noexcept
  {
    // QX, unfortunately, does not define output routines as const,
    // which makes the use of const_cast necessary. Moreover, the
    // dump() method prints its output to std::cout so that the
    // output stream needs to be captured temporarily
    std::stringstream buffer;
    std::streambuf* sbuf = std::cout.rdbuf();
    std::cout.rdbuf(buffer.rdbuf());
    const_cast<qx::circuit&>(_circuit).dump();
    std::cout.rdbuf(sbuf);

    return buffer.str();
  }

  /// Append quantum gate expression to kernel
  QData& append_kernel(qx::gate* gate)
  {
    _circuit.add(gate);
    return *this;
  }

  /// Get constant reference to kernel
  const qx::circuit& kernel() const { return _circuit; }

  /// Get reference to kernel
  qx::circuit& kernel() { return _circuit; }

  /// Clear kernel
  void clear() { _circuit.clear(); }

  /// Get constant reference to circuit
  const qx::circuit& circuit() const { return _circuit; }

  /// Get reference to circuit
  qx::circuit& circuit() { return _circuit; }

  /// Get constant reference to quantum register
  const qx::qu_register& reg() const { return _reg; }

  /// Get reference to circuit
  qx::qu_register& reg() { return _reg; }

  /// Reset quantum register
  void reset() { _reg.reset(); }

  /// Execute kernel using perfect qubits asynchronously
  QJob<QJob_enum::CXX>* execute_async(QStream<QJob_enum::CXX>* stream = NULL,
                                      bool verbose = false,
                                      bool silent = false,
                                      bool only_binary = false,
                                      std::size_t shots = 1024)
  {
    auto runner = [&, verbose, silent, only_binary, shots]() -> void {
      _circuit.set_iterations(shots);
      _circuit.execute(_reg, verbose, silent, only_binary);
    };

    if (stream != NULL)
      return stream->run(runner);
    else
      return _qstream_cxx.run(runner);
  }

  /// Execute kernel using perfect qubits synchronously
  qx::qu_register& execute(QStream<QJob_enum::CXX>* stream = NULL,
                           bool verbose = false,
                           bool silent = false,
                           bool only_binary = false,
                           std::size_t shots = 1024)
  {
    execute_async(stream, verbose, silent, only_binary, shots)->wait();
    return _reg;
  }

  /// Execute kernel using imperfect qubits asynchronously
  QJob<QJob_enum::CXX>* execute_async(double error_probability,
                                      QStream<QJob_enum::CXX>* stream = NULL,
                                      bool verbose = false,
                                      bool silent = false,
                                      bool only_binary = false,
                                      std::size_t shots = 1024)
  {
    auto runner =
      [&, error_probability, verbose, silent, only_binary, shots]() -> void {
      qx::depolarizing_channel dep_ch(&_circuit, _qubits, error_probability);
      qx::circuit* _c = dep_ch.inject(verbose);
      _c->set_iterations(shots);
      _c->execute(_reg, verbose, silent, only_binary);
    };

    if (stream != NULL)
      return stream->run(runner);
    else
      return _qstream_cxx.run(runner);
  }

  /// Execute kernel using imperfect qubits synchronously
  qx::qu_register& execute(double error_probability,
                           QStream<QJob_enum::CXX>* stream = NULL,
                           bool verbose = false,
                           bool silent = false,
                           bool only_binary = false,
                           std::size_t shots = 1024)
  {
    execute_async(
      error_probability, stream, verbose, silent, only_binary, shots)
      ->wait();
    return _reg;
  }

private:
  qx::circuit _circuit;
  qx::qu_register _reg;
};
#endif

/// Serialize operator
template<std::size_t _qubits, QBackend _qbackend>
std::ostream&
operator<<(std::ostream& os, const QData<_qubits, _qbackend>& data)
{
  os << data.to_string();
  return os;
}

/**
   @brief LibKet show gate type - specialization for QData objects
*/
template<std::size_t level = 1, std::size_t _qubits, QBackend _qbackend>
inline static auto
show(const QData<_qubits, _qbackend>& data,
     std::ostream& os = std::cout,
     const std::string& prefix = "")
{
  os << "QData\n";
  if (level > 0) {
    os << prefix << "|   qubits = " << utils::to_string(_qubits) << std::endl;
    os << prefix << "| backend = " << utils::to_string((int)_qbackend)
       << std::endl;
  }

  return data;
}

} // namespace LibKet

#endif // QDATA_HPP
