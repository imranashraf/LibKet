/** @file libket/QJob.hpp

    @brief LibKet quantum job execution class

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
*/

#pragma once
#ifndef QJOB_HPP
#define QJOB_HPP

#include <chrono>
#include <functional>
#include <future>

#ifdef LIBKET_WITH_PYTHON
#include <Python.h>
#endif

namespace LibKet {

namespace detail {
#ifdef LIBKET_WITH_PYTHON

template<typename T>
T
py_cast(PyObject* obj);

template<>
short
py_cast<short>(PyObject* obj)
{
  return PyLong_AsLong(obj);
}

template<>
unsigned short
py_cast<unsigned short>(PyObject* obj)
{
  return PyLong_AsUnsignedLong(obj);
}

template<>
int
py_cast<int>(PyObject* obj)
{
  return PyLong_AsLong(obj);
}

template<>
unsigned int
py_cast<unsigned int>(PyObject* obj)
{
  return PyLong_AsUnsignedLong(obj);
}

template<>
long
py_cast<long>(PyObject* obj)
{
  return PyLong_AsLong(obj);
}

template<>
unsigned long
py_cast<unsigned long>(PyObject* obj)
{
  return PyLong_AsUnsignedLong(obj);
}

template<>
long long
py_cast<long long>(PyObject* obj)
{
  return PyLong_AsLongLong(obj);
}

template<>
unsigned long long
py_cast<unsigned long long>(PyObject* obj)
{
  return PyLong_AsUnsignedLongLong(obj);
}

template<>
float
py_cast<float>(PyObject* obj)
{
  return PyFloat_AsDouble(obj);
}

template<>
double
py_cast<double>(PyObject* obj)
{
  return PyFloat_AsDouble(obj);
}

template<>
long double
py_cast<long double>(PyObject* obj)
{
  return PyFloat_AsDouble(obj);
}

template<>
std::string
py_cast<std::string>(PyObject* obj)
{
  return PyBytes_AsString(PyUnicode_AsUTF8String(obj));
}

template<>
nlohmann::json
py_cast<nlohmann::json>(PyObject* obj)
{
  return nlohmann::json::parse(PyBytes_AsString(PyUnicode_AsUTF8String(obj)));
}

#else

template<typename T>
T
py_cast(void*)
{
  throw "This feature requires -DLIBKET_WITH_PYTHON";
}

#endif

} // namespace detail

/**
     @brief LibKet quantum job executor enumerator

     The LibKet quantum job executor enumerator defines the supported job
   execution units
  */
enum class QJob_enum
{
  Python,
  CXX
};

/**
   @brief LibKet quantum job class

   The LibKet quantum job class implements a quantum job
*/
template<enum QJob_enum>
class QJob;

/**
    @brief LibKet quantum job class specialization for Python executor

    The LibKet quantum job class implements a quantum job that can be executed
   within the LibKet quantum stream class for the Python executor
  */
template<>
class QJob<QJob_enum::Python>
{
private:
  // Timing information
  std::chrono::time_point<std::chrono::high_resolution_clock> _timing_start;
  std::chrono::time_point<std::chrono::high_resolution_clock> _timing_stop;

#ifdef LIBKET_WITH_PYTHON

private:
  // Python function and value objects
  PyObject* _value = NULL;

  // Python module and global/local dictionary objects
  PyObject* _module = NULL;
  PyObject* _global = NULL;
  PyObject* _local = NULL;

  // Handle to asynchronous operation
  std::future<void> _handle;

  // Python script
  const std::string _script;

  // Names of run, wait, and query methods
  const std::string _method_run;
  const std::string _method_wait;
  const std::string _method_query;

  // Launches the quantum job
  void launch(const std::string& method, const QJob* depend)
  {
    // Wait for dependent quantum job (if any)
    if (depend != NULL)
      depend->_handle.wait();

    // Ensure that the current thread is ready to call the Python C API
    // regardless of the current state of Python, or of the global interpreter
    // lock.
    PyGILState_STATE gilState;
    gilState = PyGILState_Ensure();

    // Define function in the newly created module
    _value = PyRun_String(_script.c_str(), Py_file_input, _global, _local);

    // _value would be null if the Python syntax is wrong
    if (_value == NULL) {
      if (PyErr_Occurred()) {
        PyErr_Print();
      }
      throw "An error occured: Python syntax is wrong!";
    }

    // _value is the result of the executing code,
    // chuck it away because we've only declared a function
    Py_DECREF(_value);

    // Get a pointer to the function
    PyObject* _func = PyObject_GetAttrString(_module, method.c_str());

    // Double check we have actually found it and it is callable
    if (!_func || !PyCallable_Check(_func)) {
      if (PyErr_Occurred()) {
        PyErr_Print();
      }
      throw "An error occured: Cannot find find function \"" + method + "\"!";
    }

    // Call the function, passing to it the arguments
    _timing_start = std::chrono::high_resolution_clock::now();
    _value = PyObject_CallObject(_func, NULL);
    _timing_stop = std::chrono::high_resolution_clock::now();

    PyErr_Print();

    // Decrease reference counter
    Py_DECREF(_func);

    // Release any resources previously acquired
    PyGILState_Release(gilState);
  }

public:
  /// Default constructor deleted
  QJob() = delete;

  /// Explicit quantum job constructor
  explicit QJob(const std::string& script,
                const std::string& method_run,
                const std::string& method_wait,
                const std::string& method_query,
                PyObject* module,
                PyObject* global,
                PyObject* local,
                const QJob* depend = NULL)
    : _script(script)
    , _method_run(method_run)
    , _method_wait(method_wait)
    , _method_query(method_query)
    , _module(module)
    , _global(global)
    , _local(local)
  {
    // Start asynchronous execution
    _handle = run(depend);
  }

  /// Destructor
  ~QJob()
  {
    this->wait();

    // Ensure that the current thread is ready to call the Python C API
    // regardless of the current state of Python, or of the global interpreter
    // lock.
    PyGILState_STATE gilState;
    gilState = PyGILState_Ensure();

    // Decrease reference counter
    Py_DECREF(_value);

    // Release any resources previously acquired
    PyGILState_Release(gilState);
  }

  /// (Re-)runs the quantum job
  std::future<void> run(const QJob* depend = NULL)
  {
    return std::async(std::launch::async, &QJob::launch, this, _method_run, depend);
  }

  /// Waits for quantum job to complete and returns the result as JSON object
  nlohmann::json get() noexcept
  {
    this->wait();
    return detail::py_cast<nlohmann::json>(_value);
  }

  /// Waits for quantum job to complete
  void wait() noexcept
  {
    if (_method_wait.empty()) {
      _handle.wait();

    } else {
      std::future<void> __handle;
      __handle =
        std::async(std::launch::async, &QJob::launch, this, _method_run, this);
      __handle.wait();
    }
  }

  /// Returns true if quantum job has completed, or false if not
  bool query() noexcept
  {

    if (_method_query.empty()) {
      return _handle.valid();

    } else {
      std::future<void> __handle;
      __handle = std::async(
        std::launch::async, &QJob::launch, this, _method_query, this);
      __handle.wait();
      return detail::py_cast<bool>(_value);
    }
  }

#else

  /// Default constructor
  QJob(const std::string& py_script,
       const std::string& py_method,
       PyObject* py_module,
       PyObject* py_global,
       PyObject* py_local,
       QJob* py_depend = NULL)
  {
    throw "This feature requires -DLIBKET_WITH_PYTHON";
  }

  /// Waits for quantum job to complete and returns the result as JSON object
  const nlohmann::json& get() const noexcept
  {
    throw "This feature requires -DLIBKET_WITH_PYTHON";
  }

  /// Waits for quantum job to complete
  void wait() const noexcept
  {
    throw "This feature requires -DLIBKET_WITH_PYTHON";
  }

  /// Returns true if quantum job has completed, or false if not
  bool query() const noexcept
  {
    throw "This feature requires -DLIBKET_WITH_PYTHON";

    return false;
  }

#endif

  /// Returns duration of the quantum job
  template<class Rep = double, class Period = std::ratio<1>>
  const std::chrono::duration<Rep, Period> duration() const noexcept
  {
    return std::chrono::duration_cast<std::chrono::duration<Rep, Period>>(
      _timing_stop - _timing_start);
  }
};

/**
  @brief LibKet quantum job class specialization for C++ executor

  The LibKet quantum job class implements a quantum job that can be executed
  within the LibKet quantum stream class for the C++ executor
*/
template<>
class QJob<QJob_enum::CXX>
{
private:
  // Timing information
  std::chrono::time_point<std::chrono::high_resolution_clock> _timing_start;
  std::chrono::time_point<std::chrono::high_resolution_clock> _timing_stop;

  // Handle to asynchronous operation
  std::future<void> _handle;

  // Callback method
  const std::function<void(void)>& _method;

  // Launches the quantum job
  void launch(const std::function<void(void)>& method, const QJob* depend)
  {
    // Wait for dependent quantum job (if any)
    if (depend != NULL)
      depend->_handle.wait();

    // Run method
    _timing_start = std::chrono::high_resolution_clock::now();
    method();
    _timing_stop = std::chrono::high_resolution_clock::now();
  }

public:
  /// Default constructor deleted
  QJob() = delete;

  /// Explicit quantum job constructur
  explicit QJob(const std::function<void(void)>& method,
                const QJob* depend = NULL)
    : _method(method)
  {
    // Start asynchronous execution
    _handle = run(depend);
  }

  /// Destructor
  ~QJob() { this->wait(); }

  /// (Re-)runs the quantum job
  std::future<void> run(const QJob* depend = NULL)
  {
    return std::async(std::launch::async, &QJob::launch, this, _method, depend);
  }

  /// Waits for quantum job to complete
  void wait() const noexcept { _handle.wait(); }

  /// Returns true if quantum job has completed, or false if not
  bool query() const noexcept { return _handle.valid(); }

  /// Returns duration of the quantum job
  template<class Rep = double, class Period = std::ratio<1>>
  const std::chrono::duration<Rep, Period> duration() const noexcept
  {
    return std::chrono::duration_cast<std::chrono::duration<Rep, Period>>(
      _timing_stop - _timing_start);
  }
};

} // namespace LibKet

#endif // QJOB_HPP
